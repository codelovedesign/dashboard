<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Import_mgr_testdrive extends import  {

   public  $Coluums = array('ลำดับ','รหัสตัวแทน','ชื่อตัวแทน','คะแนนรวม','อัพเดท');
   
   
   public function __construct($dbname)
   {
        parent::__construct($dbname);
   }
   
   public function get_datatable(){

       	$this->CI->load->model('mgr_testdrive_model','data_model');
        $result = $this->CI->data_model->get_datatables($this->database_new_name);
        $data = array();
        $no = $_POST['start'];
        foreach ($result as $customers) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $customers->dealer_code;
            $row[] = $customers->dealer_name;
            $row[] = $customers->score;
            $row[] = date('d-m-Y' , strtotime($customers->created_at));
 
            $data[] = $row;
        }
 
        $json= array("draw" => $_POST['draw'] , 
					"recordsTotal" => $this->CI->data_model->count_all($this->database_new_name) ,
					'recordsFiltered' => $this->CI->data_model->count_filtered($this->database_new_name),
					"data"=> $data );

        return $json;
 
   }

   public function read_data_excel($FileName){
        $inputFileName = "upload/" . $FileName ;  
		$result = $this->get_data_excel($inputFileName);

		$first_arr=$result[0];	
		if(isset($first_arr['Dlr Code']) && isset($first_arr['STTL']) ){
			
            $i = 0;
            foreach ($result as $data) {
				$i++;
			    $day = date("Y-m-d H:i:s");
			
				$data_array  = array(
					'dealer_code'=> $data['Dlr Code'] ,
					'dealer_name'=> $data['Dealer'] ,
					'jan'=> $data['Jan'] ,
					'fab'=> $data['Feb'] ,
					'mar'=> $data['Mar'] ,
					'apr'=> $data['Apr'] ,
					'may'=> $data['May'] ,
					'jun'=> $data['Jun'] ,
					'jul'=> $data['Jul'] ,
					'aug'=> $data['Aug'] ,
					'sep'=> $data['Sep'] ,
					'oct'=> $data['Oct'] ,
					'nov'=> $data['Nov'] ,
					'dec'=> $data['Dec'] ,
					'score'=> $data['STTL'] ,
					'created_at'=> $day ,
				);

				$query = $this->otherdb
					->where('dealer_code', $data['Dlr Code'])
					->get('tb_mgr_testdrive');

				if ($query->num_rows() > 0) {
					$this->otherdb
					->where('dealer_code', $data['Dlr Code'])
					->update('tb_mgr_testdrive', $data_array);
				}
				else
				{
					$this->otherdb->insert('tb_mgr_testdrive', $data_array);
					$id = $this->otherdb->insert_id();
				}
		    }

            return true;
        }
        else{

            return false;
        }
   }



}