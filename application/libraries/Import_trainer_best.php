<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Import_trainer_best extends import  {
   public  $Coluums =  array('ลำดับ','ปี','รหัสผู้ฝึกสอน', 'รหัสตัวแทน' ,'หมวดหมู่','อนุมัติ','อนุมัติโดย', 'อัพเดท');
   
   
   public function __construct($dbname)
   {
        parent::__construct($dbname);
       	$this->CI->load->model('Best_practice_model','data_model');
   }
   
   public function get_datatable(){

        $result = $this->CI->data_model->get_datatables($this->database_new_name);
        $data = array();
        $no = $_POST['start'];
        foreach ($result as $customers) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $customers->year;
            $row[] = $customers->trainer_code;
            $row[] = $customers->dealer_code	;
            $row[] = $customers->category;
            $row[] = $customers->best_practice;
            $row[] = $customers->approval	;
            $row[] = date('d-m-Y' , strtotime($customers->created_at));
 
            $data[] = $row;
        }
        $json= array("draw" => $_POST['draw'] , 
					"recordsTotal" => $this->CI->data_model->count_all($this->database_new_name) ,
					'recordsFiltered' => $this->CI->data_model->count_filtered($this->database_new_name),
					"data"=> $data );

        return $json;
 
   }

   public function read_data_excel($FileName){
        $inputFileName = "upload/" . $FileName ;  
		$result = $this->get_data_excel($inputFileName);

		$first_arr=$result[0];	
		if(isset($first_arr['Index']) && isset($first_arr['T_Code']) ){
			
            $i = 0;
            foreach ($result as $data) {
				$i++;
			    $day = date("Y-m-d H:i:s");
				$year = '';
				$quarter ='';
				if(isset($data['rYear'])){
					$year = $data['rYear']-543;
				}


				$data_array  = array(
					'year'=> $year ,
					'no'=> $data['rNo'] ,
					'trainer_code'=> $data['T_Code'],
					'dealer_code'=> $data['D_Code'],
					'br_register'=> $data['Br_Register'],
					'category'=> $data['rCategory'],
					'receive_date'=> $this->dateExcel2date($data['Receive_Date']),
					'update_date'=> $this->dateExcel2date($data['Update_Date']),
					'best_practice'=> $data['Best_Practice'],
					'approval'=> $data['Approval'],
					'Index'=> $data['Index'],
					'created_at'=> $day ,
				);

				$query = $this->otherdb
					->where('Index',$data['Index'])
					->get('tb_best_practice');

				if ($query->num_rows() > 0) {
					$this->otherdb
					->where('Index',$data['Index'])
					->update('tb_best_practice', $data_array);
				}
				else
				{
					$this->otherdb->insert('tb_best_practice', $data_array);
					$id = $this->otherdb->insert_id();
				}
			}

            return true;
        }
        else{

            return false;
        }
   }



}