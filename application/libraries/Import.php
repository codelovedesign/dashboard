<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Import {
   protected $CI;
   protected $database_new_name='';
   protected $otherdb;


   public function __construct($dbname)
   {
        // parent::__construct();
        $this->CI =& get_instance();
        $this->database_new_name = $dbname['database_name'] ;
        $otherdb =$this->CI->load->database("otherdb", TRUE);
		$otherdb->db_select($this->database_new_name);
		$this->otherdb =  $otherdb;
   }
    
    public function get_data_excel($inputFileName){

		// $inputFileName = "media/Active SM.xlsx";  
		$inputFileType = PHPExcel_IOFactory::identify($inputFileName);  
		$objReader = PHPExcel_IOFactory::createReader($inputFileType);  
		$objReader->setReadDataOnly(true);  
		$objPHPExcel = $objReader->load($inputFileName);  

		$objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
		$highestRow = $objWorksheet->getHighestRow();
		$highestColumn = $objWorksheet->getHighestColumn();

		$headingsArray = $objWorksheet->rangeToArray('A1:'.$highestColumn.'1',null, true, true, true);
		$headingsArray = $headingsArray[1];

		$r = -1;
		$namedDataArray = array();
		for ($row = 2; $row <= $highestRow; ++$row) {
			$dataRow = $objWorksheet->rangeToArray('A'.$row.':'.$highestColumn.$row,null, true, true, true);
		    //  var_dump($dataRow);

			if ((isset($dataRow[$row]['A'])) && ($dataRow[$row]['A'] > '')) {
				++$r;
				foreach($headingsArray as $columnKey => $columnHeading) {
					$namedDataArray[$r][$columnHeading] = $dataRow[$row][$columnKey];
				}
			}
		}

		return  $namedDataArray ;

	}

    protected function dateExcel2date($data){
		$result_data = '';
		// var_dump($data['work_startdate']);
		if($data){
			$result_data =  date("Y-m-d", PHPExcel_Shared_Date::ExcelToPHP($data)); 
		}

		return $result_data;
	}
}