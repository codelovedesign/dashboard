<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Import_mgr extends import  {

   public  $Coluums = array('ลำดับ','รหัสตัวแทน','ชื่อตัวแทน','รหัสสาขา','รหัสผู้จัดการ','ชื่อ','นามสกุล','อัพเดท');
   
   
   public function __construct($dbname)
   {
        parent::__construct($dbname);
   }
   
   public function get_datatable(){

       	$this->CI->load->model('mgr_model','data_model');
        $result = $this->CI->data_model->get_datatables($this->database_new_name);
        $data = array();
        $no = $_POST['start'];
        foreach ($result as $customers) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $customers->dealer_code;
            $row[] = $customers->dealer_name;
            $row[] = $customers->branch_code;
            $row[] = $customers->mgr_code;
            $row[] = $customers->mgr_name	;
            $row[] = $customers->mgr_surname	;
            $row[] = date('d-m-Y' , strtotime($customers->created_at));
 
            $data[] = $row;
        }
 
        $json= array("draw" => $_POST['draw'] , 
					"recordsTotal" => $this->CI->data_model->count_all($this->database_new_name) ,
					'recordsFiltered' => $this->CI->data_model->count_filtered($this->database_new_name),
					"data"=> $data );

        return $json;
 
   }

   public function read_data_excel($FileName){
        $inputFileName = "upload/" . $FileName ;  
		$result = $this->get_data_excel($inputFileName);

		$first_arr=$result[0];	
		if(isset($first_arr['Dlr_Code']) && isset($first_arr['Group']) ){
			
            $i = 0;
            foreach ($result as $data) {
				$i++;
			    $day = date("Y-m-d H:i:s");


			   if(!$data['Mgr_Code']){
                  continue;
			   }	

				$data_array  = array(
					'group_a'=> $data['Group_A'] ,
					'group'=> $data['Group'] ,
					'dealer_code'=> $data['Dlr_Code'] ,
					'dealer_name'=> $data['Dealer Name'] ,
					'branch_code'=> $data['Br_Code'] ,
					'location'=> $data['Location'] ,
					'mgr_code'=> $data['Mgr_Code'] ,
					'mgr_name'=> $data['Mgr_Name'] ,
					'mgr_surname'=> $data['Mgr_Surname'] ,
					'crm_q2'=> $data['CRM_Q2'] ,
					'crm_q3'=> $data['CRM_Q3'] ,
					'crm_q4'=> $data['CRM_Q4'] ,
					'act_q1'=> $data['Act_Q1'] ,
					'act_q2'=> $data['Act_Q2'] ,
					'act_q3'=> $data['Act_Q3'] ,
					'act_q4'=> $data['Act_Q4'] ,
					'present_cer'=> $data['Present_Cer'] ,
					'created_at'=> $day ,
				);

				$query = $this->otherdb
					->where('mgr_code',$data['Mgr_Code'])
					->get('tb_mgr');

				if ($query->num_rows() > 0) {
					$this->otherdb
					->where('mgr_code',$data['Mgr_Code'])
					->update('tb_mgr', $data_array);
				}
				else
				{
					$this->otherdb->insert('tb_mgr', $data_array);
					$id = $this->otherdb->insert_id();
				}
		    }

            return true;
        }
        else{

            return false;
        }
   }



}