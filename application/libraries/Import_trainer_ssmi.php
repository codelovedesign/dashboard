<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Import_trainer_ssmi extends import  {
   public  $Coluums = array('ลำดับ','รหัสตัวแทน','รหัสสาขา','ssmi','คะแนน','เดือน','ปี','อัพเดท');
   
   
   public function __construct($dbname)
   {
        parent::__construct($dbname);
       	$this->CI->load->model('trainer_ssmi_model','data_model');
   }
   
   public function get_datatable(){

        $result = $this->CI->data_model->get_datatables($this->database_new_name);
        $data = array();
        $no = $_POST['start'];
        foreach ($result as $item) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $item->dealer_code;
            $row[] = $item->branch_code;
            $row[] = $item->ssmi;
            $row[] = $item->score;
            $row[] = $item->month;
            $row[] = $item->year;
            $row[] = date('d-m-Y' , strtotime($item->created_at));
            $data[] = $row;
        }
        $json= array("draw" => $_POST['draw'] , 
					"recordsTotal" => $this->CI->data_model->count_all($this->database_new_name) ,
					'recordsFiltered' => $this->CI->data_model->count_filtered($this->database_new_name),
					"data"=> $data );

        return $json;
 
   }

   public function read_data_excel($FileName){
        $inputFileName = "upload/" . $FileName ;  
		$result = $this->get_data_excel($inputFileName);

		$first_arr=$result[0];	
		if(isset($first_arr['Month']) && isset($first_arr['Dlr_Code']) && isset($first_arr['BR']) ){
			
            $i = 0;
            foreach ($result as $data) {
				$i++;
				
			    $day = date("Y-m-d H:i:s");
			    $codeIndex ="";
			    $year ="";
			    if(isset($data['Month'])){
					$code_arr= explode("-",$data['Month']);
					$codeIndex = (int)$code_arr[0];
					$year = "20" .$code_arr[1]-43;
			    }
				
				$data_array  = array(
					'month'=> $codeIndex ,
					'year'=> $year ,
					'dealer_code'=> $data['Dlr_Code'],
					'branch_code'=> $data['BR'],
					'ssmi'=> $data['SSMI'],
					'remerk'=> $data['Remark'],
					// 'score'=> $data['SSMI_Score'],
					'created_at'=> $day ,
				);

				$query = $this->otherdb
					->where('dealer_code',$data['Dlr_Code'])
					->where('branch_code',$data['BR'])
					->where('month',$codeIndex )
					->where('year',$year )
					->get('tb_trainer_ssmi');

				if ($query->num_rows() > 0) {
					$this->otherdb
					->where('dealer_code',$data['Dlr_Code'])
					->where('branch_code',$data['BR'])
					->where('month',$codeIndex )
					->where('year',$year )
					->update('tb_trainer_ssmi', $data_array);
				}
				else
				{
					$this->otherdb->insert('tb_trainer_ssmi', $data_array);
					$id = $this->otherdb->insert_id();
				}
		    }

            return true;
        }
        else{

            return false;
        }
   }



}