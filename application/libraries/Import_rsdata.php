<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Import_rsdata extends import  {
   public  $Coluums = array('ลำดับ','ซีรี่ย์','เลขตัวถัง','รหัสตัวแทน','รหัสพนักงาน','ประเภท','วันจอง','วันส่งมอบ');
   
   
   public function __construct($dbname)
   {
        parent::__construct($dbname);
   }
   
   public function get_datatable(){

       	$this->CI->load->model('rs_model','data_model');
        $result = $this->CI->data_model->get_datatables($this->database_new_name);
        $data = array();
        $no = $_POST['start'];
        foreach ($result as $customers) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $customers->series;
            $row[] = $customers->vin_no;
            $row[] = $customers->dealer_code;
            $row[] = $customers->salesman_code;
            $row[] = $customers->sorce;
            $row[] = date('d-m-Y' , strtotime($customers->rs_month));
            $row[] = date('d-m-Y' , strtotime($customers->dl_month));
 
            $data[] = $row;
        }
        $json= array("draw" => $_POST['draw'] , 
					"recordsTotal" => $this->CI->data_model->count_all($this->database_new_name) ,
					'recordsFiltered' => $this->CI->data_model->count_filtered($this->database_new_name),
					"data"=> $data );

        return $json;
 
   }

   public function read_data_excel($FileName){
        $inputFileName = "upload/" . $FileName ;  
		$result = $this->get_data_excel($inputFileName);

		$first_arr=$result[0];	
		if(isset($first_arr['Dealer Code']) && isset($first_arr['Series']) && isset($first_arr['VIN No']) ){
			
            $i = 0;
            foreach ($result as $data) {
				$i++;
				
			    $day = date("Y-m-d H:i:s");
				
				$data_array  = array(
					'dealer_code'=> $data['Dealer Code'],
					'series'=> $data['Series'],
					'vin_no'=> $data['VIN No'] ,
					'rs_month'=> $this->dateExcel2date($data['RSMonth']) ,
					'dl_month'=> $this->dateExcel2date($data['DLMonth']) ,
					'salesman_code'=> $data['Salesman Code'],
					'fleet_id'=> $data['Fleet ID'],
					'sorce'=> $data['Sorce'],
					'original_series'=> $data['OriginalSeries'],
					'region_id'=> $data['Region'],
					'created_at'=> $day ,
				);

				$query = $this->otherdb
					->where('vin_no',$data['VIN No'])
					->get('tb_sales_summary');

				if ($query->num_rows() > 0) {
					$this->otherdb->where('vin_no', $data['VIN No']);
					$this->otherdb->update('tb_sales_summary', $data_array);
				}
				else
				{
					$this->otherdb->insert('tb_sales_summary', $data_array);
					$id = $this->otherdb->insert_id();
				}

				
		    }

            return true;
        }
        else{

            return false;
        }
   }



}