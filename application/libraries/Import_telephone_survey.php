<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class import_telephone_survey  extends import  {
   public  $Coluums = array('ลำดับ','ไตรมาส','รหัสตัวแทน','รหัสสาขา','คะแนน','ปี','อัพเดท');
   
   public function __construct($dbname)
   {
        parent::__construct($dbname);
       	$this->CI->load->model('telephone_model','data_model');
   }
   
   public function get_datatable(){

        $result = $this->CI->data_model->get_datatables($this->database_new_name);
        $data = array();
        $no = $_POST['start'];
        foreach ($result as $item) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $item->quarter;
            $row[] = $item->dealer_code;
            $row[] = $item->branch_code;
            $row[] = $item->rating_answer;
            $row[] = $item->year;
            $row[] = date('d-m-Y' , strtotime($item->created_at));
            $data[] = $row;
        }
        $json= array("draw" => $_POST['draw'] , 
					"recordsTotal" => $this->CI->data_model->count_all($this->database_new_name) ,
					'recordsFiltered' => $this->CI->data_model->count_filtered($this->database_new_name),
					"data"=> $data );

        return $json;
 
   }

   public function read_data_excel($FileName){
        $inputFileName = "upload/" . $FileName ;  
		$result = $this->get_data_excel($inputFileName);

		$first_arr=$result[0];	
		if(isset($first_arr['Dlr_Code']) && isset($first_arr['Br_Code']) && isset($first_arr['Rating_Answer']) ){
			
            $i = 0;
           	foreach ($result as $data) {
				$i++;

				if(empty($data['Br_Code'])){
					$data['Br_Code'] = '';
				}

				if(empty($data['Rating_Answer'])){
					$data['Rating_Answer'] = '';
				}
				
			    $day = date("Y-m-d H:i:s");
			    $codeIndex ="";
			    $year ="";
			    if(isset($data['Quarter'])){
					$code_arr= explode("-",$data['Quarter']);
					$codeIndex = (int)$code_arr[0];
					$year = "20" .$code_arr[1];
			    }
				
				$data_array  = array(
					'quarter'=> $codeIndex ,
					'year'=> $year ,
					'dealer_code'=> $data['Dlr_Code'],
					'branch_code'=> $data['Br_Code'],
					'rating_answer'=> $data['Rating_Answer'],
					'created_at'=> $day ,
				);

				$query = $this->otherdb
					->where('dealer_code',$data['Dlr_Code'])
					->where('branch_code',$data['Br_Code'])
					->where('quarter',$codeIndex )
					->where('year',$year )
					->get('tb_telephone_survey');

				if ($query->num_rows() > 0) {
					$this->otherdb
					->where('dealer_code',$data['Dlr_Code'])
					->where('branch_code',$data['Br_Code'])
					->where('quarter',$codeIndex )
					->where('year',$year )
					->update('tb_telephone_survey', $data_array);
				}
				else
				{
					$this->otherdb->insert('tb_telephone_survey', $data_array);
					$id = $this->otherdb->insert_id();
				}
			}

            return true;
        }
        else{

            return false;
        }
   }



}