<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Import_training_report extends import  {
   
   public  $Coluums = array('ลำดับ','รหัสผู้ฝึกสอน','ไตรมาส','ปี','ประเภท ก.','ประเภท ข.','ประเภท ค.','ประเภท ง.','วันที่บันทึก','อัพเดท');
   
   public function __construct($dbname)
   {
        parent::__construct($dbname);
   }
   
   public function get_datatable(){

       	$this->CI->load->model('tr_model','data_model');
        $result = $this->CI->data_model->get_datatables($this->database_new_name);
        $data = array();
        $no = $_POST['start'];
        foreach ($result as $customers) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $customers->training_code;
            $row[] = $customers->quarter;
            $row[] = $customers->year;
            $row[] = $customers->type_a;
            $row[] = $customers->type_b;
            $row[] = $customers->type_c;
            $row[] = $customers->type_d;
            $row[] = $customers->record_date;
            $row[] = date('d-m-Y' , strtotime($customers->created_at));
 
            $data[] = $row;
        }
        $json= array("draw" => $_POST['draw'] , 
					"recordsTotal" => $this->CI->data_model->count_all($this->database_new_name) ,
					'recordsFiltered' => $this->CI->data_model->count_filtered($this->database_new_name),
					"data"=> $data );

        return $json;
 
   }

   public function read_data_excel($FileName){
        $inputFileName = "upload/" . $FileName ;  
		$result = $this->get_data_excel($inputFileName);

		$first_arr=$result[0];	
		if(isset($first_arr['Quarter']) && isset($first_arr['T_Code']) ){

            $i = 0;
			foreach ($result as $data) {
				$i++;
				
			    $day = date("Y-m-d H:i:s");
			   
			   if(!isset($data['T_Code'])) {
				   $data['T_Code']= '';
			   }
			  
			   $year = '';
			   $quarter ='';
				if(isset($data['Quarter'])){
					$arr= explode("-",$data['Quarter']);
					$quarter = (int)$arr[0];
					$year = "20" .($arr[1]-43);
			    }
				
				$data_array  = array(
					'training_code'=> $data['T_Code'],
					'quarter'=> $quarter,
					'year'=> $year ,
					'type_a'=> $data['Type_A'],
					'type_b'=> $data['Type_B'],
					'type_c'=> $data['Type_C'],
					'type_d'=> $data['Type_D'],
					'release_date'=> $this->dateExcel2date($data['Release_Date']) ,
					'record_date'=> $this->dateExcel2date($data['Record_date']) ,
					'report_index'=> $data['Report_Index'],
					'running_no'=> $data['Running_No'],
					'created_at'=> $day ,
				);

				$query = $this->otherdb
					->where('report_index',$data['Report_Index'])
					->get('tb_training_report');

				if ($query->num_rows() > 0) {
					$this->otherdb
					->where('report_index',$data['Report_Index'])
					->update('tb_training_report', $data_array);
				}
				else
				{
					$this->otherdb->insert('tb_training_report', $data_array);
					$id = $this->otherdb->insert_id();
				}
			}
            return true;
        }
        else{

            return false;
        }
   }



}