<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Import_exam extends import  {
   public  $Coluums = array('ลำดับ','รหัสพนักงาน','ชื่อแบบทดสอบ','คะแนน','ปี','อัพเดท');
   
   public function __construct($dbname)
   {
        parent::__construct($dbname);
   }
   
   public function get_datatable(){

       	$this->CI->load->model('exam_model','data_model');
        $result = $this->CI->data_model->get_datatables($this->database_new_name);
        $data = array();
        $no = $_POST['start'];
        foreach ($result as $item) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $item->salesman_code;
            $row[] = $item->exam_name;
            $row[] = $item->exam_score;
            $row[] = $item->exam_year;
            $row[] = date('d-m-Y' , strtotime($item->created_at));
            $data[] = $row;
        }
        $json= array("draw" => $_POST['draw'] , 
					"recordsTotal" => $this->CI->data_model->count_all($this->database_new_name) ,
					'recordsFiltered' => $this->CI->data_model->count_filtered($this->database_new_name),
					"data"=> $data );

        return $json;
 
   }

   public function read_data_excel($FileName){
        $inputFileName = "upload/" . $FileName ;  
		$result = $this->get_data_excel($inputFileName);

		$first_arr=$result[0];	
		if(isset($first_arr['Exam_Code']) && isset($first_arr['SM_Code']) && isset($first_arr['Score']) ){
			
            $i = 0;
           	foreach ($result as $data) {
				$i++;
				
			    $day = date("Y-m-d H:i:s");
			    $codeIndex ="";
			    $year ="";
			    if(isset($data['Exam_Code'])){
					$code_arr= explode("-",$data['Exam_Code']);
					$codeIndex = (int)$code_arr[0];
					$year = "20" .$code_arr[1];
			    }
				
				$data_array  = array(
					'salesman_code'=> $data['SM_Code'],
					'exam_name'=> $data['E_Name'],
					'score'=> $data['Score'],
					'exam_code'=> $codeIndex ,
					'exam_year'=> $year ,
					'exam_score'=> $data['Score'],
					'created_at'=> $day ,
				);

				$query = $this->otherdb
					->where('salesman_code',$data['SM_Code'])
					->where('exam_code',$codeIndex )
					->where('exam_year',$year )
					->get('tb_exam');

				if ($query->num_rows() > 0) {
					$this->otherdb->where('salesman_code', $data['SM_Code']);
					$this->otherdb->where('exam_code',$codeIndex ) ;
					$this->otherdb->where('exam_year', $year ) ;
					$this->otherdb->update('tb_exam', $data_array);
				}
				else
				{
					$this->otherdb->insert('tb_exam', $data_array);
					$id = $this->otherdb->insert_id();
				}

				
			}

            return true;
        }
        else{

            return false;
        }
   }



}