<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adjust_model extends CI_Model {
    
    public function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
    }

   public function get_year($where= array()){
       $query = $this->db->get_where('tb_year', $where );
       return $query->result();
   }

   public function set_year($arr)
    {
        $this->db->insert('tb_year', $arr);
        return $this->db->insert_id();
    }


    public function get_year_by_id($id)
    {
        if($id != FALSE) {
            $query = $this->db->get_where('tb_year', array('id' => $id));
            return $query->row_array();
        }
        else {
            return FALSE;
        }
    }

   public function del_year($id){
       $result = $this->db->delete('tb_year', array('id' => $id)); 
       return $result;
   }
   
   


    

}
?>