<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Global_model extends CI_Model {
        var $table = "";
        public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
        }

        public function get_data_by_id($table,$id)
        {
                $query = $this->db->where("id", $id )
                         ->get($table);
                return $query->row();
        }

        public function insert_data($table,$post_data)
        {
                $this->db->insert($table, $post_data);
                $insert_id = $this->db->insert_id();

                return  $insert_id;
        }

        public function get_data($table,$where= array() ,$orderby= array(), $groupby = array() )
        {
                $this->table = $table;
                
                foreach($where as $key => $value) {
                    if(isset($value)) {  
                      $this->db->WHERE($key, $value);
                    }
                    else{
                       echo "Error : value of  {$key}  is null";     
                    }
                }

                if(isset($groupby)){
                    foreach($groupby as $value){
                        $this->db->group_by($value); 
                    }    
                }

                if(isset($orderby)){
                    foreach($orderby as $key => $value){
                           $this->db->order_by($key,$value); 
                    }    
                }

                $query = $this->db->get($this->table);
                return $query->result();
        }

        public function delete_data($table,$where=array())
        {

                foreach($where as $key => $value) {
                    if(isset($value)) {  
                      $this->db->WHERE($key, $value);
                    }
                }
                $result=$this->db->delete($table);
                return  $result;
        }

        public function update_data($table,$post_data = array() ,$where=array())
        {

                foreach($where as $key => $value) {
                    if(isset($value)) {  
                      $this->db->WHERE($key, $value);
                    }
                }
                $result=$this->db->update($table,$post_data);
                return  $result;
        }

        

        public function count_filtered()
        {
            $query = $this->db->get();
            return $query->num_rows();
        }
    
        public function count_all()
        {
            $this->db->from($this->table);
            return $this->db->count_all_results();
        }
 
   

}
?>