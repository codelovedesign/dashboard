<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Award_model extends CI_Model {

    public $title;
    public $content;
    public $date;
    public $db2;
    public $dbname_default = "saleaward_";
    public function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();

        $this->db2 = $this->load->database("otherdb", TRUE);
        
    }

    public function get_award_type_by_id($id)
    {
        if($id != FALSE) {
            $query = $this->db->get_where('tb_award_type', array('id' => $id));
            return $query->row_array();
        }
        else {
            return FALSE;
        }
    }

    public function get_award_type($where=array(),$group_by=null)
    {
        if($group_by){
            $this->db->group_by($group_by); 
        }
        $query = $this->db->get_where('tb_award_type', $where );

        return $query->result();
    }

    public function get_award($where=array(), $year)
    {
        $database_new_name = $this->dbname_default.$year ;
        $this->db2->db_select($database_new_name);
        
        $this->db2->order_by('total_score', 'desc'); 
        $query = $this->db2->get_where('tb_award', $where );

        // echo $this->db2->last_query();
        return $query->result();
    }

    public function get_award_year($where=array())
    {
        $query = $this->db->get_where('tb_year', $where );
        return $query->result();
    }


    public function get_group_by_id($id)
    {
        if($id != FALSE) {
            $query = $this->db->get_where('tb_group', array('id' => $id));
            return $query->row_array();
        }
        else {
            return FALSE;
        }
    }

    public function get_award_list($where=array() , $limit = NULL , $year){

        $database_new_name = $this->dbname_default. $year ;
        $this->db2->db_select($database_new_name);

        $this->db2->order_by('total_score', 'desc'); 
        if($limit){
            $this->db2->limit($limit);
        }
        $query = $this->db2->get_where('tb_award', $where );
        $result = array();
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
        }

        return $result ;
   }

    

}
?>