<!-- begin row -->
<div class="row">
    <div class="col-md-12">
        <!-- begin panel -->
        <div class="panel panel-inverse" data-sortable-id="form-stuff-1">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                </div>
                <h4 class="panel-title">เงื่อนไขการรับรางวัล</h4>
            </div>
            <div class="panel-body">
                <form class="form-horizontal">
                    <!--<div class="form-group">
                        <label class="col-md-3 control-label">Default Input</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" placeholder="Default input" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Disabled Input</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" placeholder="Disabled input" disabled />
                        </div>
                    </div>-->
                    <div class="form-group">
                        <label class="col-md-3 control-label">ประเภทรางวัล</label>
                        <div class="col-md-9">
                            <select class="form-control">
                                <option>รางวัลพนักงานยอดเยี่ยมด้านขายประจํา ปี 2559 ระดับพนักงานขาย</option>
                                <option>รางวัลพนักงานยอดเยี่ยมด้านขายประจํา ปี 2559 ระดับผู้ฝึกสอน</option>
                                <option>รางวัลพนักงานยอดเยี่ยมด้านขายประจํา ปี 2559 ระดับผู้จัดการขาย</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label">ชื่อรางวัล</label>
                        <div class="col-md-9">
                            <select class="form-control">
                                <option>รางวัลพนักงานขายยอดเยี่ยม (Excellent Salesman)</option>
                                <option>รางวัลพนักงานขายยอดเยี่ยม 3 ปีซ้อน (Hall of Fame)</option>
                                <option>รางวัลผู้ฝึกสอนยอดเยี่ยม (Excellent Sales Trainer)</option>
                                <option>รางวัลผู้ฝึกสอนยอดเยี่ยม 3 ปีซ้อน (Hall of Fame)</option>
                                <option>รางวัลผู้จัดการขายยอดเยี่ยม (Excellent Sales Manager)</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label">ระดับรางวัล</label>
                        <div class="col-md-9">
                            <select class="form-control">
                                <option>รางวัลพนักงานขายยอดเยี่ยม ใบอนุญาตขายระดับ TSE-G</option>
                                <option>รางวัลพนักงานขายยอดเยี่ยม ใบอนุญาตขายระดับ TSE</option>
                                <option>รางวัลพนักงานขายยอดเยี่ยม ใบอนุญาตขายระดับ TSC</option>
                            </select>
                        </div>
                    </div>



                    <div class="form-group">
                        <label class="col-md-3 control-label">จำนวนรางวัล</label>
                        <div class="col-md-2">
                            <input class="form-control input-lg" type="text" value="75" placeholder="ลำดับ" />
                        </div>
                        <label class="col-md-5 control-label text-left">รางวัล</label>
                        
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">คะแนนสะสมรวมลำดับที่</label>
                        <div class="col-md-2">
                            <input class="form-control input-lg" type="text" value="1" placeholder="ลำดับ" />
                        </div>
                        <label class="col-md-1 control-label text-center">ถึง</label>
                        <div class="col-md-2">
                            <input class="form-control input-lg" type="text" placeholder="ลำดับ" />
                        </div>
                    </div>

                    
                </form>
            </div>
        </div>
        <!-- end panel -->
    </div>
    <div class="col-md-12">
        <!-- begin panel -->
        <div class="panel panel-inverse" data-sortable-id="form-stuff-1">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                </div>
                <h4 class="panel-title">รายละเอียดการประเมินผล</h4>
            </div>
            <div class="panel-body">
                <form class="form-horizontal" parsley-validate  data-form="Form-sales">

                    <h4 class="m-t-10">หัวข้อการให้คะแนน</h4>
                     <div class="form-group">
                        <label class="col-md-offset-0 col-md-8 control-label text-left">1. ประสิทธิภาพการขาย</label>
                        <div class="col-md-2">
                            <input class="form-control input-lg" type="text" value="7200" placeholder="คะแนน" />
                        </div>
                        <label class="col-md-2 control-label text-left">คะแแน</label>
                    </div>
                     <div class="form-group">
                        <label class="col-md-offset-1 col-md-7 control-label text-left">1.1 ตัวเลขยอดขายรายเดือน <br/> คิดคะแนนเป็นรายเดือน ตั้งแต่เดือนมกราคมถึงธันวาคม  (สูงสุดเดือนละ)</label>
                        <div class="col-md-2">
                            <input class="form-control input-lg" type="number" data-month-limit=".limitscore" value="5400" placeholder="คะแนน" />
                        </div>
                        <label class="col-md-2 control-label text-left">คะแแน</label>
                    </div>


                     <div class="form-group">
                        <label class="col-md-offset-5 col-md-1 control-label text-left">จำนวนคัน</label>
                        <div class="col-md-1">
                            <input class="form-control input-sm" data-input="start" type="number" min='1' value="1" placeholder="ตัวเลข" />
                        </div>
                        <label class="col-md-1 control-label text-center">ถึง</label>
                        <div class="col-md-1">
                            <input class="form-control input-sm"  data-input="end" type="number" max='15'min='1' value="10" placeholder="ตัวเลข" />
                        </div>
                         <div class="col-md-1 col-sm-1">
                            <a class="btn btn-primary btn-sm m-r-5" data-click="tablescore" >
                                <i class="fa fa-repeat" aria-hidden="true"></i>
                                อัพเดท
                            </a>
                        </div>
                    </div>


                    <div class="form-group">
                           <label class="col-md-offset-0 col-md-6 control-label text-left">- คิดคะแนนเป็นรายเดือน ตั้งแต่เดือนมกราคมถึงธันวาคม  (สูงสุดเดือนละ)</label>

                            <div class="col-md-offset-0 col-md-12">
                                <span data-append="tablescore"></span>
                                <!--<table class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th width="120">กลุ่ม</th>
                                        <th class="text-center" >10 คัน</th>
                                        <th class="text-center" >9 คัน</th>
                                        <th class="text-center" >8 คัน</th>
                                        <th class="text-center" >7 คัน</th>
                                        <th class="text-center" >6 คัน</th>
                                        <th class="text-center" >5 คัน</th>
                                        <th class="text-center" >4 คัน</th>
                                        <th class="text-center" >3 คัน</th>
                                        <th class="text-center" >2 คัน</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>กลุ่มเจ้าพระยา 1</td>
                                        <td><input class="form-control input-sm" type="text" value="0"  /></td>
                                        <td><input class="form-control input-sm" type="text" value="0"  /></td>
                                        <td><input class="form-control input-sm" type="text" value="0"  /></td>
                                        <td><input class="form-control input-sm" type="text" value="0"  /></td>
                                        <td><input class="form-control input-sm" type="text" value="0"  /></td>
                                        <td><input class="form-control input-sm" type="text" value="0"  /></td>
                                        <td><input class="form-control input-sm" type="text" value="0"  /></td>
                                        <td><input class="form-control input-sm" type="text" value="0"  /></td>
                                        <td><input class="form-control input-sm" type="text" value="0"  /></td>
                                        
                                    </tr>
                                    </tbody>
                                </table>-->
                            </div>
                        </div>


                     <div class="form-group">
                        <label class="col-md-offset-1 col-md-7 control-label text-left">1.2 ความสม่ำเสมอในการขายรถทุก series</label>
                        <div class="col-md-2">
                            <input class="form-control input-lg" type="text" value="1800" placeholder="ลำดับ" />
                        </div>
                        <label class="col-md-2 control-label text-left">คะแแน</label>
                    </div>
                     <div class="form-group">
                        <label class="col-md-offset-1 col-md-7 control-label text-left">- มียอดขายแต่ละซีรีส์ไม่น้อยกว่า 1 คันในแต่ละเดือน ไม่รวม Fleet Sales <br/> (ยกเว้นโครงการพิเศษ ที่มิใช่การขายให้หน่วยงานราชการและนิติบุคคล) </label>
                        <div class="col-md-2">
                            <input class="form-control input-lg" type="text" value="15" placeholder="ลำดับ" />
                        </div>
                        <label class="col-md-2 control-label text-left">คะแนน</label>
                    </div>

                     <div class="form-group">
                        <label class="col-md-offset-1 col-md-7 control-label text-left">- ในแต่ละซีรี่ส์ถ้ามียอดขายครบทุกเดือน (ม.ค.-ธ.ค.) โดยรวม ≥ 12 คัน จะได้เพิ่มอีก </label>
                        <div class="col-md-2">
                            <input class="form-control input-lg" type="text" value="45" placeholder="ลำดับ" />
                        </div>
                        <label class="col-md-2 control-label text-left">คะแนนในซีรี่ส์นั้นๆ</label>
                    </div>

                     <div class="form-group">
                        <label class="col-md-offset-1 col-md-6 control-label text-left">- ยกเว้นในรุ่น Innova หรือ Avanza จะมีการพิจารณาตามตาราง ดังต่อไปนี้</label>
                    </div>

                      <div class="form-group">

                            <div class="col-md-offset-2 col-md-8">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th width="120">กลุ่ม</th>
                                        <th>ทุก N เดือน</th>
                                        <th>คะแนนต่อ N เดือน</th>
                                        <th>ม.ค.-ธ.ค. มียอดขายทุก N เดือน</th>
                                        
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>กลุ่มเจ้าพระยา 1</td>
                                        <td><input class="form-control input-sm" type="text" value="1"  /></td>
                                        <td><input class="form-control input-sm" type="text" value="15"  /></td>
                                        <td><input class="form-control input-sm" type="text" value="45"  /></td>
                                        
                                        
                                    </tr>
                                    <tr>
                                        <td>กลุ่มเจ้าพระยา 2</td>
                                        <td><input class="form-control input-sm" type="text" value="2"  /></td>
                                        <td><input class="form-control input-sm" type="text" value="30"  /></td>
                                        <td><input class="form-control input-sm" type="text" value="45"  /></td>
                                        
                                    </tr>
                                    <tr>
                                        <td>ปิง</td>
                                        <td><input class="form-control input-sm" type="text" value="3"  /></td>
                                        <td><input class="form-control input-sm" type="text" value="45"  /></td>
                                        <td><input class="form-control input-sm" type="text" value="45"  /></td>
                                        
                                    </tr>
                                    <tr>
                                        <td>วัง</td>
                                        <td><input class="form-control input-sm" type="text" value="3"  /></td>
                                        <td><input class="form-control input-sm" type="text" value="45"  /></td>
                                        <td><input class="form-control input-sm" type="text" value="45"  /></td>
                                        
                                    </tr>
                                    <tr>
                                        <td>ยม</td>
                                        <td><input class="form-control input-sm" type="text" value="3"  /></td>
                                        <td><input class="form-control input-sm" type="text" value="45"  /></td>
                                        <td><input class="form-control input-sm" type="text" value="45"  /></td>
                                        
                                    </tr>
                                    <tr>
                                        <td>น่าน</td>
                                        <td><input class="form-control input-sm" type="text" value="3"  /></td>
                                        <td><input class="form-control input-sm" type="text" value="45"  /></td>
                                        <td><input class="form-control input-sm" type="text" value="45"  /></td>
                                        
                                    </tr>
                                    
                                    </tbody>
                                </table>
                            </div>
                        </div>


                    

                     <div class="form-group">
                        <label class="col-md-offset-0 col-md-8 control-label text-left">2. ความรู้ด้านผลิตภัณฑ์เพื่อการขาย</label>
                        <div class="col-md-2">
                            <input class="form-control input-lg" type="text" value="2200" placeholder="คะแนน" />
                        </div>
                        <label class="col-md-2 control-label text-left">คะแแน</label>
                    </div>
                     <div class="form-group">
                        <label class="col-md-offset-1 col-md-7 control-label text-left">2.1 การทดสอบความรู้ทางระบบ E-Exam ผ่าน T-web</label>
                        <div class="col-md-2">
                            <input class="form-control input-lg" type="text" value="1000" placeholder="คะแนน" />
                        </div>
                        <label class="col-md-2 control-label text-left">คะแแน</label>
                    </div>
                     <div class="form-group">
                        <label class="col-md-offset-1 col-md-7 control-label text-left">2.1 การสำรวจทางโทรศัพท์ คะแนนสูงสุด</label>
                        <div class="col-md-2">
                            <input class="form-control input-lg" type="text" value="1200" placeholder="คะแนน" />
                        </div>
                        <label class="col-md-2 control-label text-left">คะแแน</label>
                    </div>

                     <div class="form-group">
                          <label class="col-md-offset-1 col-md-7 control-label text-left">มีหลักการประเมินดังนี้</label>

                            <div class="col-md-offset-1 col-md-9">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th width="200"></th>
                                        <th class="text-center">ม.ค.-มี.ค.</th>
                                        <th class="text-center">เม.ย.-มิ.ย.</th>
                                        <th class="text-center">ก.ค.-ก.ย.</th>
                                        <th class="text-center">ต.ค.-ธ.ค.</th>      
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>พนักงานตอบคำถามได้ระดับ  A</td>
                                            <td><input class="form-control input-sm" type="text" value="300"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="300"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="300"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="300"  /></td>
                                        </tr>
                                        <tr>
                                            <td>พนักงานตอบคำถามได้ระดับ  B</td>
                                            <td><input class="form-control input-sm" type="text" value="300"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="300"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="300"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="300"  /></td>
                                        </tr>
                                        <tr>
                                            <td>พนักงานตอบคำถามได้ระดับ  C</td>
                                            <td><input class="form-control input-sm" type="text" value="300"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="300"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="300"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="300"  /></td>
                                        </tr>
                                        <tr>
                                            <td>พบว่าพนักงานตอบคำถามไม่ได้</td>
                                            <td><input class="form-control input-sm" type="text" value="0"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="0"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="0"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="0"  /></td>
                                        </tr>
                                   
                                    
                                    </tbody>
                                </table>
                            </div>
                        </div>


                     <div class="form-group">
                        <label class="col-md-offset-0 col-md-8 control-label text-left">3. ผลการสำรวจค่าความพึงพอใจของลูกค้าทางด้านการขายของผู้แทนจำหน่ายฯ [SSMI]  </label>
                        <div class="col-md-2">
                            <input class="form-control input-lg" type="text" value="600" placeholder="ลำดับ" />
                        </div>
                        <label class="col-md-2 control-label text-left">คะแแน</label>
                    </div>

                    <div class="form-group">
                          <label class="col-md-offset-0 col-md-7 control-label text-left">มีหลักการประเมินดังนี้</label>

                            <div class="col-md-offset-0 col-md-12">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th class="text-center" width="100"></th>
                                        <th class="text-center" width="200">คะแนน</th>
                                        <th class="text-center">ม.ค.</th>
                                        <th class="text-center">ก.พ.</th>
                                        <th class="text-center">มี.ค.</th>
                                        <th class="text-center">เม.ย.</th>      
                                        <th class="text-center">พ.ค.</th>      
                                        <th class="text-center">มิ.ย.</th>      
                                        <th class="text-center">ก.ค.</th>      
                                        <th class="text-center">ส.ค.</th>      
                                        <th class="text-center">ก.ย.</th>      
                                        <th class="text-center">ต.ค.</th>      
                                        <th class="text-center">พ.ย.</th>      
                                        <th class="text-center">ธ.ค.</th>      
                                    </tr>
                                    </thead>
                                    <tbody>
                                        
                                        <tr>
                                            <td>
                                                <select class="form-control">
                                                    <option>></option>
                                                    <option>>=</option>
                                                    <option>=</option>
                                                    <option><</option>
                                                    <option><=</option>
                                                    <option>ระหว่าง</option>
                                                </select>
                                            </td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input class="form-control input-sm" type="text" value="300"  />
                                                    </div>    
                                                    <div class="col-md-6 hide">
                                                        <input class="form-control input-sm" type="text" value="300"  />
                                                    </div>    
                                                </div>
                                            </td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <select class="form-control">
                                                    <option>></option>
                                                    <option>>=</option>
                                                    <option>=</option>
                                                    <option><</option>
                                                    <option><=</option>
                                                    <option>ระหว่าง</option>
                                                </select>
                                            </td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input class="form-control input-sm" type="text" value="300"  />
                                                    </div>    
                                                   
                                                </div>
                                            </td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <select class="form-control">
                                                    <option>></option>
                                                    <option>>=</option>
                                                    <option>=</option>
                                                    <option><</option>
                                                    <option><=</option>
                                                    <option>ระหว่าง</option>
                                                </select>
                                            </td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input class="form-control input-sm" type="text" value="300"  />
                                                    </div>    
                                                    <label class="col-md-12 control-label text-center">ถึง</label>
                                                    <div class="col-md-12 text-right">
                                                        <input class="form-control input-sm" type="text" value="300"  />
                                                    </div>    
                                                </div>
                                            </td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                        </tr>
                                        
                                   
                                    
                                    </tbody>
                                </table>
                            </div>
                        </div>


                </form>
            </div>
        </div>
        <!-- end panel -->
    </div>

    <div class="col-md-12">
        <!-- begin panel -->
        <div class="panel panel-inverse" data-sortable-id="form-stuff-1">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                </div>
                <h4 class="panel-title">รายละเอียดการประเมินผล พนักงานขายยอดเยี่ยมระดับผู้ฝึกสอน</h4>
            </div>
            <div class="panel-body">
                <form class="form-horizontal" parsley-validate  data-form="Form-sales">

                    <h4 class="m-t-10">หัวข้อการให้คะแนน</h4>
                     <div class="form-group">
                        <label class="col-md-offset-0 col-md-8 control-label text-left">1. ความรู้และทักษะพนักงานขายภายใต้ความรับผิดชอบของผู้ฝึกสอน คะแนนรวมสูงสุด</label>
                        <div class="col-md-2">
                            <input class="form-control input-lg" type="text" value="4200" placeholder="คะแนน" />
                        </div>
                        <label class="col-md-2 control-label text-left">คะแแน</label>
                    </div>
                     <div class="form-group">
                        <label class="col-md-offset-1 col-md-7 control-label text-left">1.1 การสำรวจความรู้ของพนักงานทางโทรศัพท์</label>
                        <div class="col-md-2">
                            <input class="form-control input-lg" type="number"  value="1500" placeholder="คะแนน" />
                        </div>
                        <label class="col-md-2 control-label text-left">คะแแน</label>
                    </div>

                     <div class="form-group">
                          <label class="col-md-offset-1 col-md-7 control-label text-left">มีหลักการประเมินดังนี้</label>

                            <div class="col-md-offset-1 col-md-9">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th width="200"></th>
                                        <th class="text-center">ม.ค.-มี.ค.</th>
                                        <th class="text-center">เม.ย.-มิ.ย.</th>
                                        <th class="text-center">ก.ค.-ก.ย.</th>
                                        <th class="text-center">ต.ค.-ธ.ค.</th>      
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>พนักงานตอบคำถามได้ระดับ  A</td>
                                            <td><input class="form-control input-sm" type="text" value="300"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="300"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="300"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="300"  /></td>
                                        </tr>
                                        <tr>
                                            <td>พนักงานตอบคำถามได้ระดับ  B</td>
                                            <td><input class="form-control input-sm" type="text" value="300"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="300"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="300"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="300"  /></td>
                                        </tr>
                                        <tr>
                                            <td>พนักงานตอบคำถามได้ระดับ  C</td>
                                            <td><input class="form-control input-sm" type="text" value="300"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="300"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="300"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="300"  /></td>
                                        </tr>
                                        <tr>
                                            <td>พบว่าพนักงานตอบคำถามไม่ได้</td>
                                            <td><input class="form-control input-sm" type="text" value="0"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="0"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="0"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="0"  /></td>
                                        </tr>
                                   
                                    
                                    </tbody>
                                </table>
                            </div>
                        </div>
         

                     <div class="form-group">
                        <label class="col-md-offset-1 col-md-7 control-label text-left">1.2 การทดสอบความรู้ทางระบบ E-Exam ผ่าน T-web</label>
                        <div class="col-md-2">
                            <input class="form-control input-lg" type="text" value="500" placeholder="คะแนน" />
                        </div>
                        <label class="col-md-2 control-label text-left">คะแแน</label>
                    </div>

                     <div class="form-group">
                          <label class="col-md-offset-1 col-md-7 control-label text-left">มีหลักการประเมินดังนี้</label>

                            <div class="col-md-offset-1 col-md-9">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th class="text-center" colspan="4" width="100">คะแนนที่จะได้</th>
                                       
                                    </tr>
                                    <tr>
                                        <th class="text-center" width="100"></th>
                                        <th class="text-center" width="200">อัตราการเข้าสอบ</th>
                                        <th class="text-center">อัตราการสอบผ่าน >= 90 %</th>
                                        <th class="text-center">อัตราการสอบผ่าน >= 80 %</th>   
                                    </tr>
                                    </thead>
                                    <tbody>
                                        
                                        <tr>
                                            <td>
                                                <select class="form-control">
                                                    <option>></option>
                                                    <option>>=</option>
                                                    <option>=</option>
                                                    <option><</option>
                                                    <option><=</option>
                                                    <option>ระหว่าง</option>
                                                </select>
                                            </td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input class="form-control input-sm" type="text" value="100"  />
                                                    </div>    
                                                    <div class="col-md-6 hide">
                                                        <input class="form-control input-sm" type="text" value="100"  />
                                                    </div>    
                                                </div>
                                            </td>
                                            <td><input class="form-control input-sm" type="text" value="125"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="125"  /></td>
                                            
                                        </tr>
                                        <tr>
                                            <td>
                                                <select class="form-control">
                                                    <option>></option>
                                                    <option>>=</option>
                                                    <option>=</option>
                                                    <option><</option>
                                                    <option><=</option>
                                                    <option>ระหว่าง</option>
                                                </select>
                                            </td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input class="form-control input-sm" type="text" value="100"  />
                                                    </div>    
                                                    <div class="col-md-6 hide">
                                                        <input class="form-control input-sm" type="text" value="100"  />
                                                    </div>    
                                                </div>
                                            </td>
                                            <td><input class="form-control input-sm" type="text" value="125"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="125"  /></td>
                                            
                                        </tr>
                                        <tr>
                                            <td>
                                                <select class="form-control">
                                                    <option>></option>
                                                    <option>>=</option>
                                                    <option>=</option>
                                                    <option><</option>
                                                    <option><=</option>
                                                    <option>ระหว่าง</option>
                                                </select>
                                            </td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input class="form-control input-sm" type="text" value="100"  />
                                                    </div>    
                                                    <div class="col-md-6 hide">
                                                        <input class="form-control input-sm" type="text" value="100"  />
                                                    </div>    
                                                </div>
                                            </td>
                                            <td><input class="form-control input-sm" type="text" value="125"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="125"  /></td>
                                            
                                        </tr>
                                       
                                   
                                    
                                    </tbody>
                                </table>
                                <span>*เกณฑ์การสอบผ่าน >= 80 %</span>
                            </div>
                        </div>


                     <div class="form-group">
                        <label class="col-md-offset-1 col-md-7 control-label text-left">1.3 ประสิทธิภาพการสร้างพนักงานขายและรักษาจำนวนพนักงานขายที่ผ่าน TSC </label>
                        <div class="col-md-2">
                            <input class="form-control input-lg" type="text" value="" placeholder="คะแนนสูงสุด" />
                        </div>
                        <label class="col-md-2 control-label text-left">คะแนน</label>
                    </div>

                     <div class="form-group">
                          <label class="col-md-offset-1 col-md-7 control-label text-left">มีหลักการประเมินดังนี้</label>

                            <div class="col-md-offset-1 col-md-9">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                   
                                    <tr>
                                        <th class="text-center" width="100"></th>
                                        <th class="text-center">ประสิทธิพนักงานขายในการสอบผ่านใบอนุญาติระดับ TSC</th>
                                        <th class="text-center">คะแนน</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        
                                        <tr>
                                            <td>
                                                <select class="form-control">
                                                    <option>></option>
                                                    <option>>=</option>
                                                    <option>=</option>
                                                    <option><</option>
                                                    <option><=</option>
                                                    <option>ระหว่าง</option>
                                                </select>
                                            </td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input class="form-control input-sm" type="text" value="90"  />
                                                    </div>    
                                                    <div class="col-md-6 hide">
                                                        <input class="form-control input-sm" type="text" value=""  />
                                                    </div>    
                                                </div>
                                            </td>
                                            <td><input class="form-control input-sm" type="text" value=""  /></td>
                                            
                                        </tr>
                                        <tr>
                                            <td>
                                                <select class="form-control">
                                                    <option>></option>
                                                    <option>>=</option>
                                                    <option>=</option>
                                                    <option><</option>
                                                    <option><=</option>
                                                    <option>ระหว่าง</option>
                                                </select>
                                            </td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input class="form-control input-sm" type="text" value=""  />
                                                    </div>    
                                                    <div class="col-md-6 hide">
                                                        <input class="form-control input-sm" type="text" value=""  />
                                                    </div>    
                                                </div>
                                            </td>
                                            <td><input class="form-control input-sm" type="text" value=""  /></td>
                                            
                                        </tr>
                                        <tr>
                                            <td>
                                                <select class="form-control">
                                                    <option>></option>
                                                    <option>>=</option>
                                                    <option>=</option>
                                                    <option><</option>
                                                    <option><=</option>
                                                    <option>ระหว่าง</option>
                                                </select>
                                            </td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input class="form-control input-sm" type="text" value=""  />
                                                    </div>    
                                                    <div class="col-md-6 hide">
                                                        <input class="form-control input-sm" type="text" value=""  />
                                                    </div>    
                                                </div>
                                            </td>
                                            <td><input class="form-control input-sm" type="text" value=""  /></td>
                                            
                                        </tr>
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>






                     <div class="form-group">
                        <label class="col-md-offset-1 col-md-7 control-label text-left">1.4 ผลการสำรวจ JD Power (SSMI)</label>
                        <div class="col-md-2">
                            <input class="form-control input-lg" type="text" value="1200" placeholder="คะแนน" />
                        </div>
                        <label class="col-md-2 control-label text-left">คะแนน</label>
                    </div>

                    <div class="form-group">
                          <label class="col-md-offset-0 col-md-7 control-label text-left">มีหลักการประเมินดังนี้</label>

                            <div class="col-md-offset-0 col-md-12">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th class="text-center" width="100"></th>
                                        <th class="text-center" width="200">คะแนน</th>
                                        <th class="text-center">ม.ค.</th>
                                        <th class="text-center">ก.พ.</th>
                                        <th class="text-center">มี.ค.</th>
                                        <th class="text-center">เม.ย.</th>      
                                        <th class="text-center">พ.ค.</th>      
                                        <th class="text-center">มิ.ย.</th>      
                                        <th class="text-center">ก.ค.</th>      
                                        <th class="text-center">ส.ค.</th>      
                                        <th class="text-center">ก.ย.</th>      
                                        <th class="text-center">ต.ค.</th>      
                                        <th class="text-center">พ.ย.</th>      
                                        <th class="text-center">ธ.ค.</th>      
                                    </tr>
                                    </thead>
                                    <tbody>
                                        
                                        <tr>
                                            <td>
                                                <select class="form-control">
                                                    <option>></option>
                                                    <option>>=</option>
                                                    <option>=</option>
                                                    <option><</option>
                                                    <option><=</option>
                                                    <option>ระหว่าง</option>
                                                </select>
                                            </td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input class="form-control input-sm" type="text" value="300"  />
                                                    </div>    
                                                    <div class="col-md-6 hide">
                                                        <input class="form-control input-sm" type="text" value="300"  />
                                                    </div>    
                                                </div>
                                            </td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <select class="form-control">
                                                    <option>></option>
                                                    <option>>=</option>
                                                    <option>=</option>
                                                    <option><</option>
                                                    <option><=</option>
                                                    <option>ระหว่าง</option>
                                                </select>
                                            </td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input class="form-control input-sm" type="text" value="300"  />
                                                    </div>    
                                                   
                                                </div>
                                            </td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <select class="form-control">
                                                    <option>></option>
                                                    <option>>=</option>
                                                    <option>=</option>
                                                    <option><</option>
                                                    <option><=</option>
                                                    <option>ระหว่าง</option>
                                                </select>
                                            </td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input class="form-control input-sm" type="text" value="300"  />
                                                    </div>    
                                                    <label class="col-md-12 control-label text-center">ถึง</label>
                                                    <div class="col-md-12 text-right">
                                                        <input class="form-control input-sm" type="text" value="300"  />
                                                    </div>    
                                                </div>
                                            </td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                        </tr>
                                        
                                   
                                    
                                    </tbody>
                                </table>
                            </div>
                        </div>

                     <div class="form-group">
                        <label class="col-md-offset-0 col-md-6 control-label text-left">2. การจัดอบรมหรือการจัดให้มีกระบวนการถ่ายทอดความรู้</label>
                    </div>

                     <div class="form-group">
                        <label class="col-md-offset-1 col-md-7 control-label text-left">2.1 การจัดอบรมภายในและการจัดส่งรายงานไตรมาส</label>
                        <div class="col-md-2">
                            <input class="form-control input-lg" type="text" value="" placeholder="คะแนน" />
                        </div>
                        <label class="col-md-2 control-label text-left">คะแนน</label>
                    </div>

                     <div class="form-group">
                        <label class="col-md-offset-0 col-md-8 control-label text-left">2. ความรู้ด้านผลิตภัณฑ์เพื่อการขาย</label>
                        <div class="col-md-2">
                            <input class="form-control input-lg" type="text" value="2200" placeholder="คะแนน" />
                        </div>
                        <label class="col-md-2 control-label text-left">คะแแน</label>
                    </div>
                     <div class="form-group">
                        <label class="col-md-offset-1 col-md-7 control-label text-left">2.1 การทดสอบความรู้ทางระบบ E-Exam ผ่าน T-web</label>
                        <div class="col-md-2">
                            <input class="form-control input-lg" type="text" value="1000" placeholder="คะแนน" />
                        </div>
                        <label class="col-md-2 control-label text-left">คะแแน</label>
                    </div>
                     <div class="form-group">
                        <label class="col-md-offset-1 col-md-7 control-label text-left">2.1 การสำรวจทางโทรศัพท์ คะแนนสูงสุด</label>
                        <div class="col-md-2">
                            <input class="form-control input-lg" type="text" value="1200" placeholder="คะแนน" />
                        </div>
                        <label class="col-md-2 control-label text-left">คะแแน</label>
                    </div>

                     <div class="form-group">
                          <label class="col-md-offset-1 col-md-7 control-label text-left">มีหลักการประเมินดังนี้</label>

                            <div class="col-md-offset-1 col-md-9">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th width="400">ประเภทเนื้อหา *</th>
                                        <th class="text-center">ม.ค.-มี.ค.</th>
                                        <th class="text-center">เม.ย.-มิ.ย.</th>
                                        <th class="text-center">ก.ค.-ก.ย.</th>
                                        <th class="text-center">ต.ค.-ธ.ค.</th>      
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>ประเภท ก.ผลิตภัณฑ์ (Product Knowledge) >= 1 ครั้ง</td>
                                            <td><input class="form-control input-sm" type="text" value="300"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="300"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="300"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="300"  /></td>
                                        </tr>
                                        <tr>
                                            <td>ประเภท ข. ตอบถามเพื่อการขาย (Sale Talk) >= 1 ครั้ง</td>
                                            <td><input class="form-control input-sm" type="text" value="300"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="300"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="300"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="300"  /></td>
                                        </tr>
                                        <tr>
                                            <td>ประเภท ค. มาตรฐานในการปฏิบัติงาน (SOP) >= 1 ครั้ง</td>
                                            <td><input class="form-control input-sm" type="text" value="300"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="300"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="300"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="300"  /></td>
                                        </tr>
                                        <tr>
                                            <td>ประเภท ง.ฝึกทักษะ (Sales Skill) >= 1  ครั้ง</td>
                                            <td><input class="form-control input-sm" type="text" value="300"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="300"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="300"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="300"  /></td>
                                        </tr>
                                        <tr>
                                            <td>รวม</td>
                                            <td><input class="form-control input-sm" type="text" value="300"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="300"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="300"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="300"  /></td>
                                        </tr>
                                        
                                   
                                    
                                    </tbody>
                                </table>
                            </div>
                        </div>

                     <div class="form-group">
                        <label class="col-md-offset-1 col-md-7 control-label text-left">2.2 รายงานสถานะพนักงานขายรายไตรมาส</label>
                        <div class="col-md-2">
                            <input class="form-control input-lg" type="text" value="600" placeholder="คะแนน" />
                        </div>
                        <label class="col-md-2 control-label text-left">คะแแน</label>
                     </div>

                     <div class="form-group">
                         <label class="col-md-offset-1 col-md-7 control-label text-left">มีหลักการประเมินดังนี้</label>

                            <div class="col-md-offset-1 col-md-9">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th class="text-center">ไตรมาส 1</th>
                                        <th class="text-center">ไตรมาส 2</th>
                                        <th class="text-center">ไตรมาส 3</th>
                                        <th class="text-center">ไตรมาส 4</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><input class="form-control input-sm" type="text" value="150"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="150"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="150"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="150"  /></td>
                                           
                                        </tr>
                                        
                                    
                                    </tbody>
                                </table>
                            </div>
                     </div>


                     <div class="form-group">
                        <label class="col-md-offset-0 col-md-8 control-label text-left">3. การเข้าร่วมอบรม </label>
                        <div class="col-md-2">
                            <input class="form-control input-lg" type="text" value="200" placeholder="ลำดับ" />
                        </div>
                        <label class="col-md-2 control-label text-left">คะแแน</label>
                    </div>

                    <div class="form-group">
                          <label class="col-md-offset-1 col-md-7 control-label text-left">มีหลักการประเมินดังนี้</label>

                            <div class="col-md-offset-1 col-md-9">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th class="text-center" width="200">เข้าอบรมโดยมีเวลาเรียน</th>
                                        <th class="text-center" width="100">เปอร์เซ็น</th>
                                        <th class="text-center"> คะแนน >= 90%</th>
                                        <th class="text-center"> คะแนน >= 80%</th>
                                        <th class="text-center"> คะแนน >= 70%</th>
                                        <th class="text-center"> คะแนน < 70%</th>
                                           
                                    </tr>
                                    </thead>
                                    <tbody>
                                        
                                        <tr>
                                            <td>
                                                <select class="form-control">
                                                    <option>></option>
                                                    <option>>=</option>
                                                    <option>=</option>
                                                    <option><</option>
                                                    <option><=</option>
                                                    <option>ระหว่าง</option>
                                                </select>
                                            </td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input class="form-control input-sm" type="text" value="80"  />
                                                    </div>    
                                                    <div class="col-md-6 hide">
                                                        <input class="form-control input-sm" type="text" value="80"  />
                                                    </div>    
                                                </div>
                                            </td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            
                                        </tr>
                           
                                    </tbody>
                                </table>
                            </div>
                        </div>

                
                        <div class="form-group">
                            <label class="col-md-offset-0 col-md-8 control-label text-left">4. การกำกับดูแลพนักงานขายให้มีความรู้ความสามารถในการปฏิบัติหน้าที่อย่างมืออาชีพ </label>
                            <div class="col-md-2">
                                <input class="form-control input-lg" type="text" value="500" placeholder="คะแแน" />
                            </div>
                            <label class="col-md-2 control-label text-left">คะแแน</label>
                        </div>
                        <div class="form-group">
                            <label class="col-md-offset-0 col-md-8 control-label text-left">5. การรายงานสถานะการณ์การขาย ณ พื้นที่/Trainer Redar </label>
                            <div class="col-md-2">
                                <input class="form-control input-lg" type="text" value="1500" placeholder="คะแแน" />
                            </div>
                            <label class="col-md-2 control-label text-left">คะแแน</label>
                        </div>
                        <div class="form-group">
                            <label class="col-md-offset-1 col-md-7 control-label text-left">5.1 รายงานความเคลื่อนไหวของคู่แข่งและพฤติกรรมลูกค้าที่กระทบต่อการขาย </label>
                            <div class="col-md-2">
                                <input class="form-control input-lg" type="text" value="500" placeholder="คะแแน" />
                            </div>
                            <label class="col-md-2 control-label text-left">คะแแน</label>
                        </div>

                         <div class="form-group">
                          <label class="col-md-offset-1 col-md-7 control-label text-left">มีหลักการประเมินดังนี้</label>

                            <div class="col-md-offset-1 col-md-9">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th class="text-center" width="200"></th>
                                        <th class="text-center">ม.ค.-มี.ค.</th>
                                        <th class="text-center">เม.ย.-มิ.ย.</th>
                                        <th class="text-center">ก.ค.-ก.ย.</th>
                                        <th class="text-center">ต.ค.-ธ.ค.</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        
                                        <tr>
                                            <td>
                                                จำนวนที่ส่งอย่างน้อย 1 เรื่อง / 3 เดือน
                                            </td>
                                            <td><input class="form-control input-sm" type="text" value="125"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="125"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="125"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="125"  /></td>
                                        </tr>
                                        <tr>
                                            <td>
                                               ไม่ส่งรายงาน
                                            </td>
                                            <td><input class="form-control input-sm" type="text" value="0"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="0"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="0"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="0"  /></td>
                                        </tr>
                           
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-offset-1 col-md-7 control-label text-left">5.2 ส่งตัวอย่างที่ดีในการปฏิบัติงาน </label>
                            <div class="col-md-2">
                                <input class="form-control input-lg" type="text" value="500" placeholder="คะแแน" />
                            </div>
                            <label class="col-md-2 control-label text-left">คะแแน</label>
                        </div>

                        <div class="form-group">
                          <label class="col-md-offset-1 col-md-7 control-label text-left">มีหลักการประเมินดังนี้</label>

                            <div class="col-md-offset-1 col-md-9">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th class="text-center" width="200">คุณภาพของ Best Practice</th>
                                        <th class="text-center">ฉบับที่ 1</th>
                                        <th class="text-center">ฉบับที่ 2</th>
                                        <th class="text-center">ฉบับที่ 3</th>
                                        <th class="text-center">ฉบับที่ 4</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        
                                        <tr>
                                            <td>
                                                ได้รับการนำไปขยายผลต่อ*
                                            </td>
                                            <td><input class="form-control input-sm" type="text" value="200"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="200"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="200"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="200"  /></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                ได้รับอนุมัติ
                                            </td>
                                            <td><input class="form-control input-sm" type="text" value="100"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="100"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="100"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="100"  /></td>
                                        </tr>
                           
                                    </tbody>
                                </table>
                            </div>
                        </div>
              
              
              
              
              
              
              
              
                </form>
            </div>
        </div>
        <!-- end panel -->
    </div> 


      <div class="col-md-12">
        <!-- begin panel -->
        <div class="panel panel-inverse" data-sortable-id="form-stuff-1">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                </div>
                <h4 class="panel-title">รายละเอียดการประเมินผล พนักงานขายยอดเยี่ยมระดับผู้จัดการขาย</h4>
            </div>
            <div class="panel-body">
                <form class="form-horizontal" parsley-validate  data-form="Form-sales">

                    <h4 class="m-t-10">หัวข้อการให้คะแนน</h4>
                     <div class="form-group">
                        <label class="col-md-offset-0 col-md-8 control-label text-left">1. ประสิทธภาพการบริหารการขาย คะแนนรวมสูงสุด</label>
                        <div class="col-md-2">
                            <input class="form-control input-lg" type="text" value="4200" placeholder="คะแนน" />
                        </div>
                        <label class="col-md-2 control-label text-left">คะแแน</label>
                    </div>
                     <div class="form-group">
                        <label class="col-md-offset-1 col-md-7 control-label text-left">1.1 ผลงานการขายของพนักงานขายในสาขา</label>
                        <div class="col-md-2">
                            <input class="form-control input-lg" type="number"  value="2400" placeholder="คะแนน" />
                        </div>
                        <label class="col-md-2 control-label text-left">คะแแน</label>
                    </div>

                     <div class="form-group">
                          <label class="col-md-offset-1 col-md-7 control-label text-left">ตารางเทียบคะแนน</label>

                            <div class="col-md-offset-1 col-md-9">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th class="text-center"></th>
                                            <th class="text-center">ประสิทธิภาพรายเดือน (%)</th>
                                            <th class="text-center">คะแนนที่ได้/เดือน</th>      
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                             <td>
                                                <select class="form-control">
                                                    <option>></option>
                                                    <option>>=</option>
                                                    <option>=</option>
                                                    <option><</option>
                                                    <option><=</option>
                                                    <option>ระหว่าง</option>
                                                </select>
                                            </td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input class="form-control input-sm" type="text" value="100"  />
                                                    </div>    
                                                    <div class="col-md-6 hide">
                                                        <input class="form-control input-sm" type="text" value="100"  />
                                                    </div>    
                                                </div>
                                            </td>
                                            <td><input class="form-control input-sm" type="text" value="200"  /></td>
                                        </tr>
                                        <tr>
                                             <td>
                                                <select class="form-control">
                                                    <option>></option>
                                                    <option>>=</option>
                                                    <option>=</option>
                                                    <option><</option>
                                                    <option><=</option>
                                                    <option>ระหว่าง</option>
                                                </select>
                                            </td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input class="form-control input-sm" type="text" value="100"  />
                                                    </div>    
                                                    <div class="col-md-6 hide">
                                                        <input class="form-control input-sm" type="text" value="100"  />
                                                    </div>    
                                                </div>
                                            </td>
                                            <td><input class="form-control input-sm" type="text" value="200"  /></td>
                                        </tr>
                                        <tr>
                                             <td>
                                                <select class="form-control">
                                                    <option>></option>
                                                    <option>>=</option>
                                                    <option>=</option>
                                                    <option><</option>
                                                    <option><=</option>
                                                    <option>ระหว่าง</option>
                                                </select>
                                            </td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input class="form-control input-sm" type="text" value="100"  />
                                                    </div>    
                                                    <div class="col-md-6 hide">
                                                        <input class="form-control input-sm" type="text" value="100"  />
                                                    </div>    
                                                </div>
                                            </td>
                                            <td><input class="form-control input-sm" type="text" value="200"  /></td>
                                        </tr>
                                        <tr>
                                             <td>
                                                <select class="form-control">
                                                    <option>></option>
                                                    <option>>=</option>
                                                    <option>=</option>
                                                    <option><</option>
                                                    <option><=</option>
                                                    <option>ระหว่าง</option>
                                                </select>
                                            </td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input class="form-control input-sm" type="text" value="100"  />
                                                    </div>    
                                                    <div class="col-md-6 hide">
                                                        <input class="form-control input-sm" type="text" value="100"  />
                                                    </div>    
                                                </div>
                                            </td>
                                            <td><input class="form-control input-sm" type="text" value="200"  /></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
         

                     <div class="form-group">
                        <label class="col-md-offset-1 col-md-7 control-label text-left">1.2 การจัดกิจกรรมการ (CRM)</label>
                        <div class="col-md-2">
                            <input class="form-control input-lg" type="text" value="1800" placeholder="คะแนน" />
                        </div>
                        <label class="col-md-2 control-label text-left">คะแแน</label>
                    </div>


                     <div class="form-group">
                          <label class="col-md-offset-1 col-md-7 control-label text-left">ตารางคะแนน</label>

                            <div class="col-md-offset-1 col-md-9">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                  
                                    <tr>
                                        <th class="text-center" width="100"></th>
                                        <th class="text-center">ไตรมาส 2</th>   
                                        <th class="text-center">ไตรมาส 3</th>   
                                        <th class="text-center">ไตรมาส 4</th>   
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><input class="form-control input-sm" type="text" value="600"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="600"  /></td> 
                                            <td><input class="form-control input-sm" type="text" value="600"  /></td> 
                                            <td><input class="form-control input-sm" type="text" value="600"  /></td> 
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>


                     <div class="form-group">
                        <label class="col-md-offset-0 col-md-8 control-label text-left">2. การบริหารกิจกรรมเพื่อสงเสริมการขาย </label>
                        <div class="col-md-2">
                            <input class="form-control input-lg" type="text" value="2200" placeholder="คะแนนสูงสุด" />
                        </div>
                        <label class="col-md-2 control-label text-left">คะแนน</label>
                    </div>
                     <div class="form-group">
                        <label class="col-md-offset-1 col-md-7 control-label text-left">2.1 กิจกรรมทางการตลาด </label>
                        <div class="col-md-2">
                            <input class="form-control input-lg" type="text" value="2200" placeholder="คะแนนสูงสุด" />
                        </div>
                        <label class="col-md-2 control-label text-left">คะแนน</label>
                    </div>

                     <div class="form-group">
                          <label class="col-md-offset-1 col-md-7 control-label text-left">มีหลักการประเมินดังนี้</label>

                            <div class="col-md-offset-1 col-md-9">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                   
                                    <tr>
                                        <th class="text-center" width="100"></th>
                                        <th class="text-center">จำนวนครั้งที่จัดแต่ละไตรมาส (ครั้ง)</th>
                                        <th class="text-center">คะแนน/ไตรมาส</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        
                                        <tr>
                                            <td>
                                                <select class="form-control">
                                                    <option>></option>
                                                    <option>>=</option>
                                                    <option>=</option>
                                                    <option><</option>
                                                    <option><=</option>
                                                    <option>ระหว่าง</option>
                                                </select>
                                            </td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input class="form-control input-sm" type="text" value="90"  />
                                                    </div>    
                                                    <div class="col-md-6 hide">
                                                        <input class="form-control input-sm" type="text" value=""  />
                                                    </div>    
                                                </div>
                                            </td>
                                            <td><input class="form-control input-sm" type="text" value=""  /></td>
                                            
                                        </tr>
                                        <tr>
                                            <td>
                                                <select class="form-control">
                                                    <option>></option>
                                                    <option>>=</option>
                                                    <option>=</option>
                                                    <option><</option>
                                                    <option><=</option>
                                                    <option>ระหว่าง</option>
                                                </select>
                                            </td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input class="form-control input-sm" type="text" value="90"  />
                                                    </div>    
                                                    <div class="col-md-6 hide">
                                                        <input class="form-control input-sm" type="text" value=""  />
                                                    </div>    
                                                </div>
                                            </td>
                                            <td><input class="form-control input-sm" type="text" value=""  /></td>
                                            
                                        </tr>
                                        <tr>
                                            <td>
                                                <select class="form-control">
                                                    <option>></option>
                                                    <option>>=</option>
                                                    <option>=</option>
                                                    <option><</option>
                                                    <option><=</option>
                                                    <option>ระหว่าง</option>
                                                </select>
                                            </td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input class="form-control input-sm" type="text" value="90"  />
                                                    </div>    
                                                    <div class="col-md-6 hide">
                                                        <input class="form-control input-sm" type="text" value=""  />
                                                    </div>    
                                                </div>
                                            </td>
                                            <td><input class="form-control input-sm" type="text" value=""  /></td>
                                            
                                        </tr>
                                        <tr>
                                            <td>
                                                <select class="form-control">
                                                    <option>></option>
                                                    <option>>=</option>
                                                    <option>=</option>
                                                    <option><</option>
                                                    <option><=</option>
                                                    <option>ระหว่าง</option>
                                                </select>
                                            </td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input class="form-control input-sm" type="text" value="90"  />
                                                    </div>    
                                                    <div class="col-md-6 hide">
                                                        <input class="form-control input-sm" type="text" value=""  />
                                                    </div>    
                                                </div>
                                            </td>
                                            <td><input class="form-control input-sm" type="text" value=""  /></td>
                                            
                                        </tr>
                                        <tr>
                                            <td>
                                                <select class="form-control">
                                                    <option>></option>
                                                    <option>>=</option>
                                                    <option>=</option>
                                                    <option><</option>
                                                    <option><=</option>
                                                    <option>ระหว่าง</option>
                                                </select>
                                            </td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input class="form-control input-sm" type="text" value="90"  />
                                                    </div>    
                                                    <div class="col-md-6 hide">
                                                        <input class="form-control input-sm" type="text" value=""  />
                                                    </div>    
                                                </div>
                                            </td>
                                            <td><input class="form-control input-sm" type="text" value=""  /></td>
                                            
                                        </tr>
                                       
                                    </tbody>
                                </table>
                            </div>
                        </div>


                     <div class="form-group">
                        <label class="col-md-offset-1 col-md-7 control-label text-left">2.2 การบริหารทดลองขับ</label>
                        <div class="col-md-2">
                            <input class="form-control input-lg" type="text" value="1200" placeholder="คะแนน" />
                        </div>
                        <label class="col-md-2 control-label text-left">คะแนน</label>
                    </div>

                    <div class="form-group">
                          <label class="col-md-offset-0 col-md-7 control-label text-left">มีหลักการประเมินดังนี้</label>

                            <div class="col-md-offset-0 col-md-12">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th class="text-center" width="100"></th>
                                        <th class="text-center" width="200"></th>
                                        <th class="text-center">ม.ค.</th>
                                        <th class="text-center">ก.พ.</th>
                                        <th class="text-center">มี.ค.</th>
                                        <th class="text-center">เม.ย.</th>      
                                        <th class="text-center">พ.ค.</th>      
                                        <th class="text-center">มิ.ย.</th>      
                                        <th class="text-center">ก.ค.</th>      
                                        <th class="text-center">ส.ค.</th>      
                                        <th class="text-center">ก.ย.</th>      
                                        <th class="text-center">ต.ค.</th>      
                                        <th class="text-center">พ.ย.</th>      
                                        <th class="text-center">ธ.ค.</th>      
                                    </tr>
                                    </thead>
                                    <tbody>
                                        
                                        <tr>
                                            <td rowspan="3">
                                               เวลาในการส่ง (600 คะแนน) 
                                            </td>
                                            <td>
                                                ส่งของภายในวันที่ 15 ของเดือน ถัดไป
                                            </td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                        </tr>
                                        <tr>
                                            
                                            <td>
                                                ส่งหลังจากวันที่ 15 แต่ไม่เกินสิ้นเดือน
                                            </td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                        </tr>
                                        <tr>
                                            
                                            <td>
                                                ส่งเกินเดือนที่กำหนด
                                            </td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                        </tr>
                                        <tr>
                                            <td rowspan="3">
                                               เวลาในการส่ง (600 คะแนน) 
                                            </td>
                                            <td>
                                                เอกสารสมบูรณ์
                                            </td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                        </tr>
                                        <tr>
                                           
                                            <td>
                                                เอกสารสมบูรณ์บางส่วน
                                            </td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                        </tr>
                                        <tr>
                                            
                                            <td>
                                                เอกสารไม่สมบูรณ์
                                            </td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                        </tr>
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    


                     <div class="form-group">
                        <label class="col-md-offset-0 col-md-8 control-label text-left">3. การบริหารสร้างความพอใจลูกค้า</label>
                        <div class="col-md-2">
                            <input class="form-control input-lg" type="text" value="1200" placeholder="คะแนน" />
                        </div>
                        <label class="col-md-2 control-label text-left">คะแแน</label>
                    </div>

                    <div class="form-group">
                          <label class="col-md-offset-0 col-md-7 control-label text-left">มีหลักการประเมินดังนี้</label>

                            <div class="col-md-offset-0 col-md-12">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th class="text-center" width="100"></th>
                                        <th class="text-center" width="200"></th>
                                        <th class="text-center">ม.ค.</th>
                                        <th class="text-center">ก.พ.</th>
                                        <th class="text-center">มี.ค.</th>
                                        <th class="text-center">เม.ย.</th>      
                                        <th class="text-center">พ.ค.</th>      
                                        <th class="text-center">มิ.ย.</th>      
                                        <th class="text-center">ก.ค.</th>      
                                        <th class="text-center">ส.ค.</th>      
                                        <th class="text-center">ก.ย.</th>      
                                        <th class="text-center">ต.ค.</th>      
                                        <th class="text-center">พ.ย.</th>      
                                        <th class="text-center">ธ.ค.</th>      
                                    </tr>
                                    </thead>
                                    <tbody>
                                        
                                        <tr>
                                            <td>
                                                <select class="form-control">
                                                    <option>></option>
                                                    <option>>=</option>
                                                    <option>=</option>
                                                    <option><</option>
                                                    <option><=</option>
                                                    <option>ระหว่าง</option>
                                                </select>
                                            </td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input class="form-control input-sm" type="text" value="900"  />
                                                    </div>    
                                                    <div class="col-md-6 hide">
                                                        <input class="form-control input-sm" type="text" value="959"  />
                                                    </div>    
                                                </div>
                                            </td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <select class="form-control">
                                                    <option>></option>
                                                    <option>>=</option>
                                                    <option>=</option>
                                                    <option><</option>
                                                    <option><=</option>
                                                    <option>ระหว่าง</option>
                                                </select>
                                            </td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input class="form-control input-sm" type="text" value="100"  />
                                                    </div>    
                                                    <div class="col-md-6 hide">
                                                        <input class="form-control input-sm" type="text" value="100"  />
                                                    </div>    
                                                </div>
                                            </td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <select class="form-control">
                                                    <option>></option>
                                                    <option>>=</option>
                                                    <option>=</option>
                                                    <option><</option>
                                                    <option><=</option>
                                                    <option>ระหว่าง</option>
                                                </select>
                                            </td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input class="form-control input-sm" type="text" value="900"  />
                                                    </div>    
                                                    <div class="col-md-6 hide">
                                                        <input class="form-control input-sm" type="text" value="100"  />
                                                    </div>    
                                                </div>
                                            </td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                        </tr>
                                       
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    
                     <div class="form-group">
                        <label class="col-md-offset-0 col-md-8 control-label text-left">4. จริยธรรมด้านการขาย : การสำรวจราคาขาย</label>
                        <div class="col-md-2">
                            <input class="form-control input-lg" type="text" value="600" placeholder="คะแนน" />
                        </div>
                        <label class="col-md-2 control-label text-left">คะแแน</label>
                    </div>

                    <div class="form-group">
                          <label class="col-md-offset-0 col-md-7 control-label text-left">มีหลักการประเมินดังนี้</label>

                            <div class="col-md-offset-0 col-md-12">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th class="text-center">ผลการตรวจสอบ</th>      
                                        <th class="text-center">คะแนนที่ได้</th>      
                                    </tr>
                                    </thead>
                                    <tbody>
                                        
                                        <tr>
                                            <td>
                                               ไม่มีประวัติการขายตัดราคา
                                            </td>
                                            <td><input class="form-control input-sm" type="text" value="600"  /></td>
                                        </tr>
                                        <tr>
                                            <td>
                                               มีประวัติการขายตัดราคาและได้รับโทษ 1  ครั้ง
                                            </td>
                                            <td><input class="form-control input-sm" type="text" value="300"  /></td>
                                        </tr>
                                        <tr>
                                            <td>
                                               มีประวัติการขายตัดราคาและได้รับโทษ > 1 ครั้ง
                                            </td>
                                            <td><input class="form-control input-sm" type="text" value="0"  /></td>
                                        </tr>
                                      
                                        
                                    </tbody>
                                </table>
                            </div>
                    </div>

                     <div class="form-group">
                        <label class="col-md-offset-0 col-md-8 control-label text-left">5. การบริหารบุคลากรด้านการขาย</label>
                        <div class="col-md-2">
                            <input class="form-control input-lg" type="text" value="1000" placeholder="คะแนน" />
                        </div>
                        <label class="col-md-2 control-label text-left">คะแนน</label>
                    </div>

                       <div class="form-group">
                          <label class="col-md-offset-0 col-md-7 control-label text-left">มีหลักการประเมินดังนี้</label>

                            <div class="col-md-offset-2 col-md-8">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th class="text-center" width="100"></th>
                                        <th class="text-center" width="200">อัตราการหมุนเวียนของพนักงานขาย</th>
                                        <th class="text-center">คะแนนที่ได้ต่อไตรมาส</th> 
                                    </tr>
                                    </thead>
                                    <tbody>
                                        
                                        <tr>
                                            <td>
                                                <select class="form-control">
                                                    <option>></option>
                                                    <option>>=</option>
                                                    <option>=</option>
                                                    <option><</option>
                                                    <option><=</option>
                                                    <option>ระหว่างระหว่าง</option>
                                                    <option>ระหว่าง</option>
                                                </select>
                                            </td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input class="form-control input-sm" type="text" value="900"  />
                                                    </div>    
                                                    <div class="col-md-6 hide">
                                                        <input class="form-control input-sm" type="text" value="959"  />
                                                    </div>    
                                                </div>
                                            </td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <select class="form-control">
                                                    <option>></option>
                                                    <option>>=</option>
                                                    <option>=</option>
                                                    <option><</option>
                                                    <option><=</option>
                                                    <option>ระหว่างระหว่าง</option>
                                                    <option>ระหว่าง</option>
                                                </select>
                                            </td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input class="form-control input-sm" type="text" value="900"  />
                                                    </div>    
                                                    <div class="col-md-6 hide">
                                                        <input class="form-control input-sm" type="text" value="959"  />
                                                    </div>    
                                                </div>
                                            </td>
                                            <td><input class="form-control input-sm" type="text" value="50"  /></td>
                                        </tr>
                                      
                                       
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>

                     <div class="form-group">
                        <label class="col-md-offset-0 col-md-8 control-label text-left">6. การบริหารโชว์รูม</label>
                        <div class="col-md-2">
                            <input class="form-control input-lg" type="text" value="800" placeholder="คะแนน" />
                        </div>
                        <label class="col-md-2 control-label text-left">คะแนน</label>
                    </div>
                    

                     <div class="form-group">
                          <label class="col-md-offset-1 col-md-7 control-label text-left">มีหลักการประเมินดังนี้</label>

                            <div class="col-md-offset-1 col-md-9">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th class="text-center">เกรดที่ได้จากการประเมินโชว์รูม</th>
                                        <th class="text-center">คะแนนที่ได้</th>
                            
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>เกรด A</td>
                                            <td><input class="form-control input-sm" type="text" value="800"  /></td>
                                        </tr>
                                        <tr>
                                            <td>เกรด B</td>
                                            <td><input class="form-control input-sm" type="text" value="500"  /></td>
                                        </tr>
                                        <tr>
                                            <td>เกรด C</td>
                                            <td><input class="form-control input-sm" type="text" value="200"  /></td>
                                        </tr>
                                        <tr>
                                            <td>ต่ำกว่าเกรด C</td>
                                            <td><input class="form-control input-sm" type="text" value="0"  /></td>
                                        </tr>
                                   
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    
              
              
              
              
              
              
              
                </form>
            </div>
        </div>
        <!-- end panel -->
    </div> 

    <div class="col-md-12">
        <!-- begin panel -->
        <div class="panel panel-inverse" data-sortable-id="form-stuff-1">
            <!--<div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                </div>
                <h4 class="panel-title">เงื่อนไขการรับรางวัล</h4>
            </div>-->
            <div class="panel-body">
                   
                     <div class="form-group">
                        <label class="control-label col-sm-0"></label>
                        <div class="col-md-6 col-sm-6">
                            <button type="submit" class="btn btn-primary btn-lg m-r-5">
                                <i class="fa fa-floppy-o" aria-hidden="true"></i>
                                บันทึก
                            </button>
                            <button type="submit" class="btn btn-danger btn-lg m-r-5">
                                <i class="fa fa-times" aria-hidden="true"></i>
                                ยกเลิก
                            </button>
                        </div>
                    </div>

            </div>
        </div>
        <!-- end panel -->
    </div>



<?php /**
    <!-- begin col-6 -->
    <div class="col-md-12">
        <!-- begin panel -->
        <div class="panel panel-inverse" data-sortable-id="form-stuff-2">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                </div>
                <h4 class="panel-title">Input Sizes, Input Group</h4>
            </div>
            <div class="panel-body">
                <h4 class="m-t-0">Input Sizing</h4>
                <input class="form-control input-lg" type="text" placeholder=".input-lg" />
                <p></p>
                <input class="form-control" type="text" placeholder="Default input" />
                <p></p>
                <input class="form-control input-sm" type="text" placeholder=".input-sm" />
                <p></p>
                <select class="form-control input-lg">
                    <option>.input-lg</option>
                </select>
                <p></p>
                <select class="form-control">
                    <option>default input</option>
                </select>
                <p></p>
                <select class="form-control input-sm">
                    <option>.input-sm</option>
                </select>
                <h4 class="m-t-20">Input Group</h4>
                <div class="input-group">
                    <span class="input-group-addon">@</span>
                    <input type="text" class="form-control" placeholder="Username" />
                </div>
                <p></p>
                <div class="input-group">
                    <input type="text" class="form-control" />
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                </div>
                <p></p>
                <div class="input-group">
                    <span class="input-group-addon">$</span>
                    <input type="text" class="form-control" />
                    <span class="input-group-addon">.00</span>
                </div>
                <p></p>
                <div class="input-group">
                    <span class="input-group-addon">
                        <input type="checkbox" />
                    </span>
                    <input type="text" class="form-control" placeholder="Checkbox add on" />
                </div>
                <p></p>
                <div class="input-group">
                    <span class="input-group-addon">
                        <input type="radio" />
                    </span>
                    <input type="text" class="form-control" placeholder="Radio button add on" />
                </div>
                <p></p>
                <div class="input-group">
                    <div class="input-group-btn">
                        <button type="button" class="btn btn-primary">Action</button>
                        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Another action</a></li>
                            <li><a href="#">Something else here</a></li>
                            <li class="divider"></li>
                            <li><a href="#">Separated link</a></li>
                        </ul>
                    </div>
                    <input type="text" class="form-control" />
                    <div class="input-group-btn">
                        <ul class="dropdown-menu pull-right">
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Another action</a></li>
                            <li><a href="#">Something else here</a></li>
                            <li class="divider"></li>
                            <li><a href="#">Separated link</a></li>
                        </ul>
                        <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown">
                            <span class="caret"></span>
                        </button>
                        <button type="button" class="btn btn-success">Action</button>
                    </div>
                </div>
                <h4 class="m-t-20">Input Group Sizing</h4>
                <div class="input-group input-group-lg">
                    <span class="input-group-addon">@</span>
                    <input type="text" class="form-control" placeholder="Username" />
                </div>
                <p></p>
                <div class="input-group">
                    <span class="input-group-addon">@</span>
                    <input type="text" class="form-control" placeholder="Username" />
                </div>
                <p></p>
                <div class="input-group input-group-sm">
                    <span class="input-group-addon">@</span>
                    <input type="text" class="form-control" placeholder="Username" />
                </div>
            </div>
        </div>
        <!-- end panel -->
    </div>
    <!-- end col-6 -->
 **/?>


</div>
<!-- end row -->