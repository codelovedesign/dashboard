	<!-- begin row -->
			<div class="row">
			   
			    <!-- begin col-12 -->
			    <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title"><?php echo $this->title?></h4>
                        </div>
                       
                        <div class="panel-body">

                         <?php if(validation_errors()) { ?>
                            <div class="alert alert-danger fade in m-b-15">
                                <strong>Warning!</strong>
                                <span class="close" data-dismiss="alert">×</span>
                                <?php echo validation_errors(); ?>
                            </div>
                            <?php } ?>

                            <form class="form-horizontal" action="<?php echo base_url();?>Processor/compile" method="POST" >
                                <div class="form-group">
                                    <label class="col-md-3 control-label">ปี</label>
                                    <div class="col-md-9">
                                        <select class="form-control" name="year">
                                             <option>--- เลือกปี ---</option>
                                            <?php
                                                if(isset($award_year)){
                                                    foreach($award_year as $key =>  $item ){
                                                        echo "<option value='{$item->year}'>{$item->year}</option>";
                                                    }
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">ระดับพนักงาน</label>
                                    <div class="col-md-9">
                                        <select class="form-control" name='employee_level'>
                                            <option>--- เลือกระดับพนักงาน ---</option>
                                            <?php
                                                if(isset($employee_level)){
                                                    foreach($employee_level as $key =>  $item ){
                                                        echo "<option value='{$item->id}'>{$item->name}</option>";
                                                    }
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">ประเภทรางวัล</label>
                                    <div class="col-md-9">
                                        <select class="form-control" name="award_type">
                                            <!--<option>รางวัลพนักงานยอดเยี่ยม </option>
                                            <option>รางวัลพนักงานยอดเยี่ยม</option>
                                            <option>รางวัลพนักงานยอดเยี่ยม</option>-->
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">ชุดกติกาการแข่งขัน</label>
                                    <div class="col-md-9">
                                        <select class="form-control" name="set_award">
                                            <!--<option>รางวัลพนักงานยอดเยี่ยม </option>-->
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label"></label>
                                    <div class="col-md-9">
                                       <button type="submit" class="btn btn-primary btn-lg m-r-5">
                                           <i class="fa fa-hourglass-start" aria-hidden="true"></i>
                                            ประมวลผล
                                        </button>
                                    </div>
                                </div>
                            </form>

                           
                        </div>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-10 -->
            </div>
            <!-- end row -->