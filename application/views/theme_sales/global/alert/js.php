<script src="<?php echo base_url();?>assets/js/apps.min.js"></script>
<script>
		$(document).ready(function() {
			App.init();
			// DashboardV2.init();
			$(".btn-close-error").click(function(){
				var callback = $(this).data("error-callback");
				console.log(callback);
				if(callback){
					window.location = callback;
				}
			});
		});
	</script>