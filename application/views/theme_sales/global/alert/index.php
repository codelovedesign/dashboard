<!-- begin row -->
<div class="row">
    <!-- begin col-12 -->
    <div class="col-md-12">
        <!-- begin panel -->
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <!--<div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                </div>-->
                <h4 class="panel-title"><?php if(isset($title)) echo $title;?></h4>
            </div>
            
            <div class="panel-body">

                <div class="row">
                    <div class="col-sm-12 text-center">
                        <h3 class="red"><?php if(isset($head)) echo $head;?></h3>
                        <br/>
                        <div class="col-sm-6 col-sm-offset-4 text-center">
                        <?php 
                        if(isset($data_error)) {
                            foreach ($data_error as $key => $value) {
                                echo "<p class='text-left'> " . ($key+1) . ". " . $value . "</p>";
                            }
                        }
                        ?>
                        </div>
                        <div class="col-sm-12 text-right">
                            <button type="button" class="btn btn-primary btn-close-error" data-error-callback="<?php if(isset($callback)) echo $callback;?>">
                                ปิดหน้านี้
                                <i class="fa fa-times" aria-hidden="true"></i>
                            </button>
                        </div>


                    </div>
                </div>

                
            </div>
        </div>
        <!-- end panel -->
    </div>
    <!-- end col-10 -->
</div>
<!-- end row -->