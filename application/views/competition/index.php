<!-- begin row -->
    <div class="row">
        
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title"><?php echo $this->title?></h4>
                </div>
                
                <div class="panel-body">

                    <form action="" class="form-horizontal" >
                        
                        <div class="form-group">
                            <label class="col-md-3 control-label">ปีการแข่งขัน : </label>
                            <div class="col-md-9">
                                <select class="form-control select-year">
                                  
                                  <?php
                                  if(isset($years)){
                                      foreach ($years as $key => $year) {
                                          if($year->year == $curent_year)
                                          {
                                            echo "<option selected value='{$year->year}'>{$year->year}</option>";
                                          }
                                          else{
                                            echo "<option value='{$year->year}'>{$year->year}</option>";
                                          }
                                      }
                                  }
                                  ?>
                                </select>
                            </div>
                        </div>

                    </form>

                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-10 -->
    </div>
<!-- end row -->


            <!-- begin nav-tabs -->
            <ul id="ioniconsTab" class="nav nav-tabs">
                <li class="active">
                    <a href="#default" data-toggle="tab">
                        <div class="text-center"><i class="material-icons">people</i></div>
                        <span class="hidden-xs m-l-3">พนักงานขาย</span>
                    </a>
                </li>
                <!--<li>
                    <a href="#ios" data-toggle="tab">
                        <div class="text-center"><i class="material-icons">people</i></div> 
                        <span class="hidden-xs m-l-3">พนักงานขายลูกค้าองค์กร</span>
                    </a>
                </li>-->
                <li>
                    <a href="#android" data-toggle="tab">
                        <div class="text-center"><i class="material-icons">people</i></div> 
                        <span class="hidden-xs m-l-3">ผู้ฝึกสอน</span>
                    </a>
                </li>
                <li>
                    <a href="#social" data-toggle="tab">
                        <div class="text-center"><i class="material-icons">people</i></div> 
                        <span class="hidden-xs m-l-3">ผู้ฝึกสอน</span>
                    </a>
                </li>
                <li>
                    <a href="#other" data-toggle="tab">
                        <div class="text-center"><i class="material-icons">people</i></div> 
                        <span class="hidden-xs m-l-3">อื่นๆ</span>
                    </a>
                </li>
            </ul>
            <!-- end nav-tabs -->
            <!-- begin tab-content -->
            <div id="ioniconsTabContent" class="tab-content">
                <!-- begin tab-pane -->
                <div class="tab-pane fade in active" id="default">
                    <div data-scrollbar="true" data-height="480px">

                        <div class="row">
                            <div class="col-md-3">
                                <select data-year="<?php echo $curent_year;?>" class="form-control select-option">
                                  <?php
                                    foreach($award_type as $key => $item){
                                        if($type == $item->id )
                                            echo "<option selected='selected' value='{$item->id}' >";
                                        else
                                            echo "<option value='{$item->id}' >";

                                        echo $item->name_en;
                                        echo "</option>";
                                    }
                                  ?>
                                </select>
                            </div>
                        </div>

                        <br/>

                        <div class="row row-space-10">

                              <table id="data-table" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>ลำดับ</th>
                                        <th>ชื่อ</th>
                                        <th>นามสกุล</th>
                                        <th>รหัสพนักงาน</th>
                                        <th>รหัสสาขา</th>
                                        <th>คะแนน</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    if(isset($get_award)) 
                                    {
                                        foreach($get_award as $key => $item) {
                                    ?>
                                    <tr>
                                        <td><?php echo ($key+1)?></td>
                                        <td><?php echo $item->name;?></td>
                                        <td><?php echo $item->lastname;?></td>
                                        <td><?php echo $item->sale_code;?></td>
                                        <td><?php echo $item->branch_code;?></td>
                                        <td><?php echo $item->total_score;?></td>
                                    </tr>
                                    <?php } 
                                    }
                                    ?>
                                </tbody>
                            </table>
                        
                        </div>
                    </div>
                </div>
                <!-- end tab-pane -->
                <!-- begin tab-pane -->
                <div class="tab-pane fade" id="ios">
                     <div data-scrollbar="true" data-height="480px">
                        <div class="row row-space-10">

                            <div class="row">
                                <div class="col-md-3">
                                    <select data-year="<?php echo $curent_year;?>" class="form-control select-option">
                                    <?php
                                        // foreach($award_type as $key => $item){
                                        //     if($type == $item->id )
                                        //         echo "<option selected='selected' value='{$item->id}' >";
                                        //     else
                                        //         echo "<option value='{$item->id}' >";

                                        //     echo $item->name_en;
                                        //     echo "</option>";
                                        // }
                                    ?>
                                    </select>
                                </div>
                            </div>

                            <br/>

                              <table id="data-table" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>ลำดับ</th>
                                        <th>ชื่อ</th>
                                        <th>นามสกุล</th>
                                        <th>รหัสพนักงาน</th>
                                        <th>รหัสสาขา</th>
                                        <th>คะแนน</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <!--<tr>
                                        <td>1</td>
                                        <td>chaichate</td>
                                        <td>tongsu</td>
                                        <td>11111</td>
                                        <td>21</td>
                                        <td>450</td>
                                    </tr>-->
                                </tbody>
                            </table>
                        
                        </div>
                    </div>
                </div>
                <!-- end tab-pane -->
                <!-- begin tab-pane -->
                <div class="tab-pane fade" id="android">
                     <div data-scrollbar="true" data-height="480px">

                          <div class="row">
                            <div class="col-md-3">
                                <select data-year="<?php echo $curent_year;?>" class="form-control select-option">
                                  <?php
                                    // foreach($award_type as $key => $item){
                                    //     if($type == $item->id )
                                    //         echo "<option selected='selected' value='{$item->id}' >";
                                    //     else
                                    //         echo "<option value='{$item->id}' >";

                                    //     echo $item->name_en;
                                    //     echo "</option>";
                                    // }
                                  ?>
                                </select>
                            </div>
                        </div>

                        <br/>
                        <div class="row row-space-10">

                              <table id="data-table" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>ลำดับ</th>
                                        <th>ชื่อ</th>
                                        <th>นามสกุล</th>
                                        <th>รหัสพนักงาน</th>
                                        <th>รหัสสาขา</th>
                                        <th>คะแนน</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <!--<tr>
                                        <td>1</td>
                                        <td>chaichate</td>
                                        <td>tongsu</td>
                                        <td>11111</td>
                                        <td>21</td>
                                        <td>450</td>
                                    </tr>-->
                                </tbody>
                            </table>
                        
                        </div>
                    </div>
                </div>
                <!-- end tab-pane -->
                <!-- begin tab-pane -->
                <div class="tab-pane fade" id="social">
                    <div data-scrollbar="true" data-height="480px">
                        <div class="row">
                            <div class="col-md-3">
                                 <select data-year="<?php echo $curent_year;?>" class="form-control select-option">
                                  <?php
                                    // foreach($award_type as $key => $item){
                                    //     if($type == $item->id )
                                    //         echo "<option selected='selected' value='{$item->id}' >";
                                    //     else
                                    //         echo "<option value='{$item->id}' >";

                                    //     echo $item->name_en;
                                    //     echo "</option>";
                                    // }
                                  ?>
                                </select>
                            </div>
                        </div>

                        <br/>
                        <div class="row row-space-10">

                              <table id="data-table" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>ลำดับ</th>
                                        <th>ชื่อ</th>
                                        <th>นามสกุล</th>
                                        <th>รหัสพนักงาน</th>
                                        <th>รหัสสาขา</th>
                                        <th>คะแนน</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <!--<tr>
                                        <td>1</td>
                                        <td>chaichate</td>
                                        <td>tongsu</td>
                                        <td>11111</td>
                                        <td>21</td>
                                        <td>450</td>
                                    </tr>-->
                                </tbody>
                            </table>
                        
                        </div>
                    </div>
                </div>
                <!-- end tab-pane -->
                <!-- begin tab-pane -->
                <div class="tab-pane fade" id="other">
                    <div data-scrollbar="true" data-height="480px">
                          <div class="row">
                            <div class="col-md-3">
                                 <select data-year="<?php echo $curent_year;?>" class="form-control select-option">
                                  <?php
                                    // foreach($award_type as $key => $item){
                                    //     if($type == $item->id )
                                    //         echo "<option selected='selected' value='{$item->id}' >";
                                    //     else
                                    //         echo "<option value='{$item->id}' >";

                                    //     echo $item->name_en;
                                    //     echo "</option>";
                                    // }
                                  ?>
                                </select>
                            </div>
                        </div>

                        <br/>
                        <div class="row row-space-10">

                              <table id="data-table" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>ลำดับ</th>
                                        <th>ชื่อ</th>
                                        <th>นามสกุล</th>
                                        <th>รหัสพนักงาน</th>
                                        <th>รหัสสาขา</th>
                                        <th>คะแนน</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <!--<tr>
                                        <td>1</td>
                                        <td>chaichate</td>
                                        <td>tongsu</td>
                                        <td>11111</td>
                                        <td>21</td>
                                        <td>450</td>
                                    </tr>-->
                                </tbody>
                            </table>
                        
                        </div>
                    </div>
                </div>
                <!-- end tab-pane -->
            </div>
            <!-- end tab-content -->
