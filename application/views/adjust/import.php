	<!-- #modal-message -->
    <div class="modal modal-message fade modal-loader" id="modal-upload"  >
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 class="modal-title">อัพโหลดข้อมูล</h3>
                </div>
                <div class="modal-body">
                    <!-- The global progress state -->
                    <!--<p>อัพโหลด</P>
                    <div class="progress" id="progress-upload">
                        <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                            0%
                        </div>
                    </div>-->
                    <!--<p>กำลังประมวลผล</P>-->
                    <div>
                        <div class="material-loader">
                            <svg class="circular" viewBox="25 25 50 50">
                                <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/>
                            </svg>
                            <div class="message">กำลังประมวลผล...</div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <a href="javascript:;" class="btn btn-sm btn-white" data-dismiss="modal">ปิดหน้านี้</a>
                    <!--<a href="javascript:;" class="btn btn-sm btn-primary">Save Changes</a>-->
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade modal-loader" id="modal-delete" >
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 class="modal-title">ยืนยันการลบข้อมูล</h3>
                </div>
                <div class="modal-body">
                    <div class="alert alert-warning fade in m-b-15">
                        <strong>คำเตือน!</strong>
                        เมื่อกดยืนยันการลบข้อมูล ข้อมูลจะถูกลบอย่างถาวร
                        <span class="close" data-dismiss="alert">×</span>
                    </div>

                    <div class="alert alert-danger hide">
                        <strong>แจ้งเตือน!</strong>
                        <span class="alert-span"></span>
                        <span class="close" data-dismiss="alert">×</span>
                    </div>

                     <form id="form-delete" class="form-horizontal" method="POST" data-parsley-validate="" >
                        <div class="form-group">
                            <label class="col-md-4 control-label">รหัสผ่าน : </label>
                            <div class="col-md-4">
                                <input type="password" required name="password" placeholder="">
                            </div>
                        </div>
                    </form>
                    <!--<h4><i class="material-icons">&#xE86C;</i> ตรวจสอบฐานข้อมูลซ้ำ</h4>
                    <h4><i class="material-icons">&#xE86C;</i> สร้างฐานข้อมูล</h4>-->
                   
                </div>
                <div class="modal-footer">
                    <a href="javascript:;" data-click="modal-delete" class="btn btn-sm btn-primary">ยืนยันการลบข้อมูล</a>
                    <a href="javascript:;" class="btn btn-sm btn-white" data-dismiss="modal">ยกเลิก</a>
                </div>
            </div>
        </div>
    </div>
    
<!-- begin row -->
    <div class="row">
        
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title"><?php echo $this->title?></h4>
                </div>
                
                <div class="panel-body">

                    <form action="<?php echo base_url();?>Adjust/do_upload"  method="post" enctype="multipart/form-data" class="form-horizontal" >
                        <div class="form-group">
                            <label class="col-md-3 control-label">ระดับ : </label>
                            <div class="col-md-9">
                                <select class="form-control select-level" name="type">
                                  <option <?php if($level==1) echo 'selected';?> value='1'>พนักงานขาย</option>
                                  <option <?php if($level==2) echo 'selected';?> value='2'>ผู้ฝึกสอน</option>
                                  <option <?php if($level==3) echo 'selected';?> value='3'>ผู้จัดการขาย</option>
                                </select>
                            </div>
                        </div> 
                        <div class="form-group">
                            <label class="col-md-3 control-label">ประเภทข้อมูล : </label>
                            <div class="col-md-9">
                                <select class="form-control select-data" name="type" data-selected="<?php echo $type;?>" >
                                  <!--<option <?php if($type==1) echo 'selected';?> value='1'>นำเข้าข้อมูลตัวแทนจำหน่าย (Dlr Group)</option>
                                  <option <?php if($type==2) echo 'selected';?>  value='2'>นำเข้าข้อมูลยอดขายพนักงาน(RS_Data) </option>
                                  <option <?php if($type==3) echo 'selected';?>  value='3'>นำเข้าข้อมูลการสำรวจทางโทรศัพท์ (Interview)</option>
                                  <option <?php if($type==4) echo 'selected';?>  value='4'>นำเข้าข้อมูลผลสำรวจความพึงพอใจฯ (SSMI) </option>
                                  <option <?php if($type==5) echo 'selected';?> value='5'>นำเข้าข้อมูลทดสอบความรู้ทางระบบ (E-Exam) </option>
                                  <option <?php if($type==6) echo 'selected';?>  value='6'>นำเข้าข้อมูลพนักงานขายยอดเยี่ยมย้อนหลัง(Award History)</option>
                                  <option <?php if($type==7) echo 'selected';?>  value='7'>นำเข้าข้อมูลการอบรมใบอนุญาต (License Training)</option>
                                  <option <?php if($type==8) echo 'selected';?>  value='8'>นำเข้าข้อมูลพนักงาน (Active SM)</option>
                                  
                                  <option <?php if($type==9) echo 'selected';?>  value='9'>นำเข้าข้อมูลผู้ฝึกสอน (Trainer)</option>
                                  <option <?php if($type==10) echo 'selected';?>  value='10'>นำเข้าข้อมูลการจัดอบรมภายในและการจัดส่งรายงานไตรมาส (Training Report)</option>
                                  <option <?php if($type==11) echo 'selected';?>  value='11'>นำเข้าข้อมูลรายงานสถานะพนักงานขายรายไตรมาส (Saleman Report)</option>
                                  <option <?php if($type==12) echo 'selected';?>  value='12'>นำเข้าข้อมูลรายงานความเคลื่อนไหวคู่แข่ง(Trainer Rader)</option>
                                  <option <?php if($type==13) echo 'selected';?>  value='13'>นำเข้าข้อมูลตัวอย่างที่ดีในการปฏิบัติงาน(Best Practice)</option>
                                  
                                  
                                  
                                  <option <?php if($type==14) echo 'selected';?>  value='14'>นำเข้ารายชื่อผู้จัดการ</option>
                                  <option <?php if($type==18) echo 'selected';?>  value=''>นำเข้าข้อมูลพนักงานขายรายเดือน มกราคม </option>
                                  <option <?php if($type==19) echo 'selected';?>  value=''>นำเข้าข้อมูลพนักงานขายรายเดือน มกราคม </option>
                                  <option <?php if($type==20) echo 'selected';?>  value=''>นำเข้าข้อมูลพนักงานขายรายเดือน มกราคม </option>

                                  <option <?php if($type==15) echo 'selected';?>  value='15'>นำเข้าข้อมูลการทดลองขับ</option>
                                  <option <?php if($type==16) echo 'selected';?>  value='16'>นำเข้าข้อมูลการบริหารโชว์รูม</option>
                                  <option <?php if($type==17) echo 'selected';?>  value='17'>นำเข้าข้อมูลการบริหารการสร้างความพึงพอใจลูกค้า (SSMI)</option>-->
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">นำเข้าข้อมูลประจำปี : </label>
                            <div class="col-md-9">
                                <select class="form-control select-year" name='year'>
                                    <?php
                                        foreach($years as $item){
                                            if($year == $item->year ) 
                                                echo "<option selected value='{$item->year}'>{$item->year}</option>";
                                            else
                                                echo "<option value='{$item->year}' >{$item->year}</option>";
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">เลือกไฟล์ : </label>
                            <div class="col-md-9">
                                 <span class="btn btn-success fileinput-button">
                                    <i class="fa fa-plus"></i>
                                    <span>Add files...</span>
                                    <input id="fileupload" type="file" name="files" data-url="<?php echo base_url();?>Adjust/do_upload" multiple>
                                </span>
                                <label class="filename"></label>
                            </div>
                             <input type="hidden" name="type" value="<?php echo $type;?>">
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label"></label>
                            <div class="col-md-9">
                                <button type="submit" class="btn btn-primary btn-submit btn-upload  m-r-5">
                                    <i class="fa fa-floppy-o" aria-hidden="true"></i> อัพโหลด
                                </button>
                                <button type="reset" class="btn btn-default  m-r-5">
                                    ยกเลิก
                                </button>
                            </div>
                        </div>

                    </form>

                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-10 -->
    </div>
<!-- end row -->

<!-- begin row -->
<div class="row">
    
    <!-- begin col-12 -->
    <div class="col-md-12">
        <!-- begin panel -->
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                </div>
                <h4 class="panel-title"><?php echo $this->titleTable?></h4>
            </div>
            
            <div class="panel-body">

                <table id="dataTable" data-url="<?php echo $url;?>" class="display" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <?php
                            foreach($columns as $column){
                                echo "<th>{$column}</th>";
                            }
                            ?>
                        </tr>
                    </thead>
                    <tfoot>
                        <?php
                            foreach($columns as $column){
                                echo "<th>{$column}</th>";
                            }
                        ?>
                    </tfoot>
                    </table>
            </div>
        </div>
        <!-- end panel -->
    </div>
    <!-- end col-10 -->
</div>
<!-- end row -->