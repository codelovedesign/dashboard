	<!-- #modal-message -->
    <div class="modal modal-message fade modal-loader" id="modal-message" >
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 class="modal-title">สร้างการแข่งขันประจำปี</h3>
                </div>
                <div class="modal-body">
                    <!--<h4><i class="material-icons">&#xE86C;</i> ตรวจสอบฐานข้อมูลซ้ำ</h4>
                    <h4><i class="material-icons">&#xE86C;</i> สร้างฐานข้อมูล</h4>-->
                    <!-- begin #page-loader -->
                    <div id="page-loader" >
                        <div class="material-loader">
                            <svg class="circular" viewBox="25 25 50 50">
                                <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/>
                            </svg>
                            <div class="message">Loading...</div>
                        </div>
                    </div>
                    <!-- end #page-loader -->
                </div>
                <div class="modal-footer">
                    <a href="javascript:;" class="btn btn-sm btn-white" data-dismiss="modal">ปิดหน้านี้</a>
                    <!--<a href="javascript:;" class="btn btn-sm btn-primary">Save Changes</a>-->
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade modal-loader" id="modal-delete" >
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 class="modal-title">ยืนยันการลบข้อมูล</h3>
                </div>
                <div class="modal-body">
                    <div class="alert alert-warning fade in m-b-15">
                        <strong>คำเตือน!</strong>
                        เมื่อกดยืนยันการลบข้อมูล ข้อมูลจะถูกลบอย่างถาวร
                        <span class="close" data-dismiss="alert">×</span>
                    </div>

                    <div class="alert alert-danger hide">
                        <strong>แจ้งเตือน!</strong>
                        <span class="alert-span"></span>
                        <span class="close" data-dismiss="alert">×</span>
                    </div>

                     <form id="form-delete" class="form-horizontal" method="POST" data-parsley-validate="" >
                        <div class="form-group">
                            <label class="col-md-4 control-label">รหัสผ่าน : </label>
                            <div class="col-md-4">
                                <input type="password" required name="password" placeholder="">
                            </div>
                        </div>
                    </form>
                    <!--<h4><i class="material-icons">&#xE86C;</i> ตรวจสอบฐานข้อมูลซ้ำ</h4>
                    <h4><i class="material-icons">&#xE86C;</i> สร้างฐานข้อมูล</h4>-->
                   
                </div>
                <div class="modal-footer">
                    <a href="javascript:;" data-click="modal-delete" class="btn btn-sm btn-primary">ยืนยันการลบข้อมูล</a>
                    <a href="javascript:;" class="btn btn-sm btn-white" data-dismiss="modal">ยกเลิก</a>
                </div>
            </div>
        </div>
    </div>
    
<!-- begin row -->
    <div class="row">
        
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title"><?php echo $this->title?></h4>
                </div>
                
                <div class="panel-body">

                    <form id="year" class="form-horizontal" action="<?php echo base_url();?>report/create" method="POST" data-parsley-validate="" >
                        
                        
                        <div class="form-group">
                            <label class="col-md-3 control-label">การแข่งขันประจำปี (ค.ศ.): </label>
                            <div class="col-md-9">
                                <input type="number" data-parsley-min="2016" data-parsley-max="2036" required data-parsley-maxlength="4" data-parsley-minlength="4"	name="year" class="form-control numberic" placeholder="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label"></label>
                            <div class="col-md-9">
                                <button type="submit" class="btn btn-primary btn-submit  m-r-5">
                                    <i class="fa fa-floppy-o" aria-hidden="true"></i> บันทึก
                                </button>
                                <button type="reset" class="btn btn-default  m-r-5">
                                    ยกเลิก
                                </button>
                            </div>
                        </div>
                    </form>

                    
                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-10 -->
    </div>
<!-- end row -->

<!-- begin row -->
<div class="row">
    
    <!-- begin col-12 -->
    <div class="col-md-12">
        <!-- begin panel -->
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                </div>
                <h4 class="panel-title">ปีการแข่งขัน</h4>
            </div>
            
            <div class="panel-body">

                <table id="data-table" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>ลำดับ</th>
                            <th>การแข่งขันประจำปี</th>
                            <th>ชื่อฐานข้อมูล</th>
                            <th>วันที่สร้าง</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php
                            if(isset($years)){
                                foreach($years as $key => $data){
                                 echo '<tr class="odd gradeX">
                                            <td>' . ($key + 1) .'</td>
                                            <td>' . $data->year  . '</td>
                                            <td>' . $data->db_name  . '</td>
                                            <td>' . $data->created_at  . '</td>
                                            <td class="text-center" width="200">
                                                <button type="button" data-id="' . $data->id .'" data-click="delete_database" class="btn btn-danger m-r-5 m-b-5">
                                                    <i class="fa fa-ban" aria-hidden="true"></i>
                                                    ลบ
                                                </button>
                                            </td>
                                        </tr>';
                                }
                            }
                        ?>
                        <!--<tr class="odd gradeX">
                            <td>1</td>
                            <td>2016 </td>
                            <td>saleaward_2010</td>
                            <td>25/12/2016</td>
                            <td class="text-center" width="200">
                               
                                <button type="button" class="btn btn-danger m-r-5 m-b-5">
                                    <i class="fa fa-ban" aria-hidden="true"></i>
                                    ลบ
                                </button>
                            </td>
                        </tr>-->
                     
                        
                    </tbody>
                </table>
            </div>
        </div>
        <!-- end panel -->
    </div>
    <!-- end col-10 -->
</div>
<!-- end row -->