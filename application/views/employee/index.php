	<!-- begin row -->
			<div class="row">
			   
			    <!-- begin col-12 -->
			    <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title"><?php echo $this->title?></h4>
                        </div>
                       
                        <div class="panel-body">
                            <table id="data-table" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>ลำดับ</th>
                                        <th>รหัสพนักงาน</th>
                                        <th>ชื่อ-สกุล</th>
                                        <th>ตำแหน่ง</th>
                                        <th>รหัสสาขา</th>
                                        <th>รหัสลีเดอร์</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="odd gradeX">
                                        <td>1</td>
                                        <td>0001</td>
                                        <td>นายไชยเชษฐ์ ทองสุ</td>
                                        <td>พนักงานขาย</td>
                                        <td>OH</td>
                                        <td>OH</td>
                                        <td class="text-center" width="200">
                                            <button type="button" class="btn btn-info m-r-5 m-b-5">
                                                <i class="fa fa-info-circle" aria-hidden="true"></i>
                                                รายละะอียด
                                            </button>
                                            <button type="button" class="btn btn-danger m-r-5 m-b-5">
                                                <i class="fa fa-ban" aria-hidden="true"></i>
                                                ลบ
                                            </button>
                                        </td>
                                    </tr>
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-10 -->
            </div>
            <!-- end row -->