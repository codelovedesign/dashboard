<?php
class MY_Controller extends CI_Controller
{
	   //set the class variable.
        public $template  = array();
        public $data      = array();
        public $title = 'Backoffice' ;
        public $js = array();
        public $page_level_js = array();
        public $page_level_css = array();
        public $no_login = true ;
        public $breadcrumb = array();
        public $other_db = "otherdb";

        public function __construct(){
            parent::__construct();

            date_default_timezone_set("Asia/Bangkok");
            $this->data['user']  = $this->session->userdata('logged_in');
            if (!$this->is_logged_in() && $this->no_login === true )
            {
                redirect('login', 'refresh');
            }

        }

        

        public function is_logged_in()
        {
            $user = $this->session->userdata('logged_in');
            return isset($user);
        }

        		
        /*Front Page Layout*/
        public function view($layout='') {

            // making template and send data to view.
            $this->template['header']   = $this->load->view('layout/inc/header', $this->data, true);
            $this->template['left']   = $this->load->view('layout/inc/navbar', $this->data, true);
            $this->template['middle'] = $this->load->view($this->middle, $this->data, true);
            $this->template['footer'] = $this->load->view('layout/inc/footer', $this->data, true);
            $this->template['title'] = $this->title;
            $this->template['js'] = $this->js;
            // $this->template['data'] = $this->data;
            $this->template['page_level_js'] = $this->page_level_js;
            $this->template['page_level_css'] = $this->page_level_css;

            if(empty($layout))
            {
                $this->load->view('layout/index', $this->template);
            }
            else
            {
                $this->load->view('layout/'. $layout , $this->template);
            }

        }
}

?>
