<?php
defined('BASEPATH') OR exit('No direct script access allowed');

     $arr['tb_award'] = array(
			"fields" =>array(
				'name' => array(
					'type' => 'VARCHAR',
					'constraint' => '250',
					'null' => TRUE
				),
				'lastname' => array(
					'type' => 'VARCHAR',
					'constraint' => '250',
					'null' => TRUE
				),
				'sale_code' => array(
					'type' => 'VARCHAR',
					'constraint' => '250',
					'null' => TRUE
				),
				'branch_code' => array(
					'type' => 'VARCHAR',
					'constraint' => '3',
					'null' => TRUE
				),
				'dealer_code' => array(
					'type' => 'VARCHAR',
					'constraint' => '6',
					'null' => TRUE
				),
				'dealer_name' => array(
					'type' => 'VARCHAR',
					'constraint' => '200',
					'null' => TRUE
				),
				'type' => array(
					'type' => 'INT',
					'constraint' => '1',
					'null' => TRUE
				),
				'year' => array(
					'type' => 'YEAR',
					'constraint' => '4',
					'null' => TRUE
				),
				'total_score' => array(
					'type' => 'INT',
					'constraint' => '6',
					'null' => TRUE
				),
				'monthly_score' => array(
					'type' => 'INT',
					'constraint' => '5',
					'null' => TRUE
				),
				'regularity_score' => array(
					'type' => 'INT',
					'constraint' => '5',
					'null' => TRUE
				),
				'exam_score' => array(
					'type' => 'INT',
					'constraint' => '5',
					'null' => TRUE
				),
				'interview_score' => array(
					'type' => 'INT',
					'constraint' => '5',
					'null' => TRUE
				),
				'ssmi_score' => array(
					'type' => 'INT',
					'constraint' => '5',
					'null' => TRUE
				),
				'order' => array(
					'type' => 'INT',
					'constraint' => '3',
					'null' => TRUE
				),
				'created_at' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'updated_at' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
			) ,
			"keys" => array(
				array("type","order")
			),
		);
     $arr['tb_dealers'] = array(
			"fields" =>array(
				'dealer_code' => array(
					'type' => 'VARCHAR',
					'constraint' => '6',
					'null' => TRUE
				),
				'group' => array(
					'type' => 'VARCHAR',
					'constraint' => '4',
					'null' => TRUE
				),
				'dealer_name' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'region_code' => array(
					'type' => 'VARCHAR',
					'constraint' => '3',
					'null' => TRUE
				),
				'group_id' => array(
					'type' => 'INT',
					'constraint' => '1',
					'null' => TRUE
				),
				'created_at' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'updated_at' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				)
			) ,
			"keys" => array(
                "dealer_code",
                "group"
			),
		);
     $arr['tb_employee'] = array(
			"fields" =>array(
				'dealer_code' => array(
					'type' => 'VARCHAR',
					'constraint' => '6',
					'null' => TRUE
				),
				'branch_code' => array(
					'type' => 'VARCHAR',
					'constraint' => '3',
					'null' => TRUE
				),
				'salesman_code' => array(
					'type' => 'INT',
					'constraint' => '7',
					'null' => TRUE
				),
				'salesman_id' => array(
					'type' => 'INT',
					'constraint' => '11',
					'null' => TRUE
				),
				'firstname' => array(
					'type' => 'VARCHAR',
					'constraint' => '200',
					'null' => TRUE
				),
				'lastname' => array(
					'type' => 'VARCHAR',
					'constraint' => '200',
					'null' => TRUE
				),
				'sex' => array(
					'type' => "enum('M', 'F')",
					'null' => TRUE
				),
				'married' => array(
					'type' => "enum('S', 'M','W','D')",
                    'default' => 'S',
					'null' => TRUE
				),
				'status' => array(
					'type' => "enum('R','A','W','N')",
                    'default' => 'R',
					'null' => TRUE
				),
				'birthday' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'ddms' => array(
					'type' => 'VARCHAR',
					'constraint' => '3',
					'null' => TRUE
				),
				'position' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'fleetsales_code' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'fleetsales_position' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'educational' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'religion' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'start_date' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'approval_date' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'resign_date' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'created_at' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'updated_at' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				)
			) ,
			"keys" => array(
                array("salesman_code"),
                array("salesman_code","ddms","approval_date"),
                array("ddms","approval_date")
			),
		);

	 $arr['tb_exam'] = array(
			"fields" =>array(
				'salesman_code' => array(
					'type' => 'VARCHAR',
					'constraint' => '7',
					'null' => TRUE
				),
				'score' => array(
					'type' => 'INT',
					'constraint' => '3' ,
                    'default' => '0',
					'null' => TRUE
				),
				'exam_name' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'exam_code' => array(
					'type' => 'INT',
					'constraint' => '1',
					'null' => TRUE
				),
				'exam_year' => array(
					'type' => 'YEAR',
					'constraint' => '4',
					'null' => TRUE
				),
				'exam_score' => array(
					'type' => 'INT',
					'constraint' => '3' ,
                    'default' => '0',
					'null' => TRUE
				),
				'created_at' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'updated_at' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				)
			) ,
			"keys" => array(
                array("exam_code","salesman_code"),
                array("exam_code","salesman_code","exam_year")
			),
		);	
	 $arr['tb_interview'] = array(
			"fields" =>array(
				'quarter' => array(
					'type' => 'INT',
					'constraint' => '4',
					'null' => TRUE
				),
				'year' => array(
					'type' => 'YEAR',
					'constraint' => '4' ,
					'null' => TRUE
				),
				'dealer_code' => array(
					'type' => 'INT',
					'constraint' => '7',
					'null' => TRUE
				),
				'branch_code' => array(
					'type' => 'VARCHAR',
					'constraint' => '4',
					'null' => TRUE
				),
				'rating_answer' => array(
					'type' => 'VARCHAR',
					'constraint' => '1' ,
					'null' => TRUE
				),
				'created_at' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'updated_at' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				)
			) ,
			"keys" => array(
                array("dealer_code","branch_code"),
                array("quarter","dealer_code","branch_code"),
			),
		);	
	 $arr['tb_license'] = array(
			"fields" =>array(
				'cl_code' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'code' => array(
					'type' => 'VARCHAR',
					'constraint' => '5',
					'null' => TRUE
				),
				'sale_code' => array(
					'type' => 'INT',
					'constraint' => '11',
					'null' => TRUE
				),
				'sale_id' => array(
					'type' => 'INT',
					'constraint' => '11',
					'null' => TRUE
				),
				'attendance' => array(
					'type' => 'INT',
					'constraint' => '4' ,
					'null' => TRUE
				),
				'result' => array(
					'type' => 'VARCHAR',
					'constraint' => '1' ,
					'null' => TRUE
				),
				'created_at' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'updated_at' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				)
			) ,
			"keys" => array(
				"code",
                array("cl_code","sale_code"),
                array("code","result"),
			),
		);	
	 $arr['tb_sales_summary'] = array(
			"fields" =>array(
				'dealer_code' => array(
					'type' => 'INT',
					'constraint' => '6',
					'null' => TRUE
				),
				'series' => array(
					'type' => 'VARCHAR',
					'constraint' => '5',
					'null' => TRUE
				),
				'vin_no' => array(
					'type' => 'VARCHAR',
					'constraint' => '50',
					'null' => TRUE
				),
				'rs_month' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'dl_month' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'salesman_code' => array(
					'type' => 'VARCHAR',
					'constraint' => '7' ,
					'null' => TRUE
				),
				'fleet_id' => array(
					'type' => 'VARCHAR',
					'constraint' => '7' ,
					'null' => TRUE
				),
				'sorce' => array(
					'type' => 'VARCHAR',
					'constraint' => '7' ,
					'null' => TRUE
				),
				'original_series' => array(
					'type' => 'VARCHAR',
					'constraint' => '7' ,
					'null' => TRUE
				),
				'region_id' => array(
					'type' => 'INT',
					'constraint' => '2' ,
					'null' => TRUE
				),
				'created_at' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'updated_at' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				)
			) ,
			"keys" => array(
                array("rs_month","salesman_code"),
                array("salesman_code"),
                array("vin_no"),
			),
		);	
	 $arr['tb_ssmi'] = array(
			"fields" =>array(
				'month' => array(
					'type' => 'INT',
					'constraint' => '2',
					'null' => TRUE
				),
				'year' => array(
					'type' => 'YEAR',
					'constraint' => '4',
					'null' => TRUE
				),
				'dealer_code' => array(
					'type' => 'INT',
					'constraint' => '7',
					'null' => TRUE
				),
				'branch_code' => array(
					'type' => 'VARCHAR',
					'constraint' => '3',
					'null' => TRUE
				),
				'ssmi' => array(
					'type' => 'float',
					'constraint' => '5,2',
					'null' => TRUE
				),
				'remerk' => array(
					'type' => 'VARCHAR',
					'constraint' => '250' ,
					'null' => TRUE
				),
				'score' => array(
					'type' => 'INT',
					'constraint' => '5' ,
					'null' => TRUE
				),
				'created_at' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'updated_at' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				)
			) ,
			"keys" => array(
                array("dealer_code","branch_code"),
                array("month","dealer_code","branch_code"),
			),
		);	
	 $arr['tb_history'] = array(
			"fields" =>array(
				'sale_code' => array(
					'type' => 'INT',
					'constraint' => '11',
					'null' => TRUE
				),
				'year' => array(
					'type' => 'YEAR',
					'constraint' => '4',
					'null' => TRUE
				),
				'award_code' => array(
					'type' => 'VARCHAR',
					'constraint' => '50',
					'null' => TRUE
				),
				'order' => array(
					'type' => 'INT',
					'constraint' => '2',
					'null' => TRUE
				),
				'score' => array(
					'type' => 'INT',
					'constraint' => '5' ,
					'null' => TRUE
				),
				'created_at' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'updated_at' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				)
			) ,
			"keys" => array(
                array("sale_code","year","award_code"),
			),
		);	


		// /////////////tranning
 
        $arr['tb_trainer'] = array(
			"fields" =>array(
				'dealer_code' => array(
					'type' => 'VARCHAR',
					'constraint' => '6',
					'null' => TRUE
				),
				'dealer_name' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'branch_code' => array(
					'type' => 'VARCHAR',
					'constraint' => '3',
					'null' => TRUE
				),
				'trainer_id' => array(
					'type' => 'VARCHAR',
					'constraint' => '20',
					'null' => TRUE
				),
				'trainer_code' => array(
					'type' => 'VARCHAR',
					'constraint' => '20',
					'null' => TRUE
				),
				'firstname' => array(
					'type' => 'VARCHAR',
					'constraint' => '250',
					'null' => TRUE
				),
				'lastname' => array(
					'type' => 'VARCHAR',
					'constraint' => '250',
					'null' => TRUE
				),
				'apply' => array(
					'type' => 'VARCHAR',
					'constraint' => '3',
					'null' => TRUE
				),
				'apply_year' => array(
					'type' => 'YEAR',
					'constraint' => '4',
					'null' => TRUE
				),
				'rindex' => array(
					'type' => 'VARCHAR',
					'constraint' => '20',
					'null' => TRUE
				),
				'created_at' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'updated_at' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				)
			) ,
			"keys" => array(
                array("dealer_code","branch_code","trainer_code"),
                array("trainer_code"),
			),
		);	

        $arr['tb_telephone_survey'] = array(
			"fields" =>array(
				'dealer_code' => array(
					'type' => 'VARCHAR',
					'constraint' => '6',
					'null' => TRUE
				),
				'branch_code' => array(
					'type' => 'VARCHAR',
					'constraint' => '3',
					'null' => TRUE
				),
				'sm_sex' => array(
					'type' => 'INT',
					'constraint' => '1',
					'null' => TRUE
				),
				'question_num' => array(
					'type' => 'INT',
					'constraint' => '2',
					'null' => TRUE
				),
				'corrected_ans' => array(
					'type' => 'INT',
					'constraint' => '1',
					'null' => TRUE
				),
				'reason_fail' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'rating_answer' => array(
					'type' => 'VARCHAR',
					'constraint' => '1',
					'null' => TRUE
				),
				'interview_date' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'pic' => array(
					'type' => 'INT',
					'constraint' => '3',
					'null' => TRUE
				),
				'quarter' => array(
					'type' => 'INT',
					'constraint' => '1',
					'null' => TRUE
				),
				'year' => array(
					'type' => 'YEAR',
					'constraint' => '4',
					'null' => TRUE
				),
				'index_code' => array(
					'type' => 'VARCHAR',
					'constraint' => '20',
					'null' => TRUE
				),
				'created_at' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'updated_at' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				)
			) ,
			"keys" => array(
                array("dealer_code","branch_code"),
                array("quarter","dealer_code","branch_code"),
			),
		);	

		$arr['tb_trainer_exam'] = array(
			"fields" =>array(
				'salesman_code' => array(
					'type' => 'VARCHAR',
					'constraint' => '7',
					'null' => TRUE
				),
				'score' => array(
					'type' => 'INT',
					'constraint' => '3' ,
                    'default' => '0',
					'null' => TRUE
				),
				'exam_name' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'exam_code' => array(
					'type' => 'INT',
					'constraint' => '1',
					'null' => TRUE
				),
				'exam_year' => array(
					'type' => 'YEAR',
					'constraint' => '4',
					'null' => TRUE
				),
				'exam_score' => array(
					'type' => 'INT',
					'constraint' => '3' ,
                    'default' => '0',
					'null' => TRUE
				),
				'created_at' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'updated_at' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				)
			) ,
			"keys" => array(
                array("exam_code","salesman_code"),
                array("exam_code","salesman_code","exam_year")
			),
		);	


		$arr['tb_trainer_tsc'] = array(
			"fields" =>array(
				'dealer_code' => array(
					'type' => 'VARCHAR',
					'constraint' => '6',
					'null' => TRUE
				),
				'branch_code' => array(
					'type' => 'VARCHAR',
					'constraint' => '3',
					'null' => TRUE
				),
				'salesman_code' => array(
					'type' => 'INT',
					'constraint' => '7',
					'null' => TRUE
				),
				'salesman_id' => array(
					'type' => 'INT',
					'constraint' => '11',
					'null' => TRUE
				),
				'firstname' => array(
					'type' => 'VARCHAR',
					'constraint' => '200',
					'null' => TRUE
				),
				'lastname' => array(
					'type' => 'VARCHAR',
					'constraint' => '200',
					'null' => TRUE
				),
				'sex' => array(
					'type' => "enum('M', 'F')",
					'null' => TRUE
				),
				'married' => array(
					'type' => "enum('S', 'M','W','D')",
                    'default' => 'S',
					'null' => TRUE
				),
				'status' => array(
					'type' => "enum('R','A','W','N')",
                    'default' => 'R',
					'null' => TRUE
				),
				'birthday' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'ddms' => array(
					'type' => 'VARCHAR',
					'constraint' => '3',
					'null' => TRUE
				),
				'position' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'fleetsales_code' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'fleetsales_position' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'educational' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'religion' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'start_date' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'approval_date' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'resign_date' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'created_at' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'updated_at' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				)
			) ,
			"keys" => array(
                array("salesman_code"),
                array("salesman_code","ddms","approval_date"),
                array("ddms","approval_date")
			),
		);

		$arr['tb_training_report'] = array(
			"fields" =>array(
				'training_code' => array(
					'type' => 'VARCHAR',
					'constraint' => '10',
					'null' => TRUE
				),
				'quarter' => array(
					'type' => 'INT',
					'constraint' => '1',
					'null' => TRUE
				),
				'year' => array(
					'type' => 'YEAR',
					'constraint' => '4',
					'null' => TRUE
				),
				'type_a' => array(
					'type' => 'INT',
					'constraint' => '2',
					'null' => TRUE
				),
				'type_b' => array(
					'type' => 'INT',
					'constraint' => '2',
					'null' => TRUE
				),
				'type_c' => array(
					'type' => 'INT',
					'constraint' => '2',
					'null' => TRUE
				),
				'type_d' => array(
					'type' => 'INT',
					'constraint' => '2',
					'null' => TRUE
				),
				'release_date' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'record_date' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'report_index' => array(
					'type' => 'VARCHAR',
					'constraint' => '20',
					'null' => TRUE
				),
				'running_no' => array(
					'type' => 'VARCHAR',
					'constraint' => '20',
					'null' => TRUE
				),
				'created_at' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'updated_at' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				)
			) ,
			"keys" => array(
                array("record_date"),
			),
		);	

		$arr['tb_trainer_ssmi'] = array(
			"fields" =>array(
				'month' => array(
					'type' => 'INT',
					'constraint' => '2',
					'null' => TRUE
				),
				'year' => array(
					'type' => 'YEAR',
					'constraint' => '4',
					'null' => TRUE
				),
				'dealer_code' => array(
					'type' => 'INT',
					'constraint' => '7',
					'null' => TRUE
				),
				'branch_code' => array(
					'type' => 'VARCHAR',
					'constraint' => '3',
					'null' => TRUE
				),
				'ssmi' => array(
					'type' => 'float',
					'constraint' => '5,2',
					'null' => TRUE
				),
				'remerk' => array(
					'type' => 'VARCHAR',
					'constraint' => '250' ,
					'null' => TRUE
				),
				'score' => array(
					'type' => 'INT',
					'constraint' => '5' ,
					'null' => TRUE
				),
				'created_at' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'updated_at' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				)
			) ,
			"keys" => array(
                array("dealer_code","branch_code"),
                array("month","dealer_code","branch_code"),
			),
		);	

		$arr['tb_sm_report'] = array(
			"fields" =>array(
				'dealer_code' => array(
					'type' => 'VARCHAR',
					'constraint' => '6',
					'null' => TRUE
				),
				'branch_code' => array(
					'type' => 'VARCHAR',
					'constraint' => '3',
					'null' => TRUE
				),
				'branch_name' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'training_code' => array(
					'type' => 'VARCHAR',
					'constraint' => '10',
					'null' => TRUE
				),
				'firstname' => array(
					'type' => 'VARCHAR',
					'constraint' => '200',
					'null' => TRUE
				),
				'lastname' => array(
					'type' => 'VARCHAR',
					'constraint' => '200',
					'null' => TRUE
				),
				'training_id' => array(
					'type' => 'VARCHAR',
					'constraint' => '20',
					'null' => TRUE
				),
				'q1' => array(
					'type' => 'INT',
					'constraint' => '3',
					'null' => TRUE
				),
				'q2' => array(
					'type' => 'INT',
					'constraint' => '3',
					'null' => TRUE
				),
				'q3' => array(
					'type' => 'INT',
					'constraint' => '3',
					'null' => TRUE
				),
				'q4' => array(
					'type' => 'INT',
					'constraint' => '3',
					'null' => TRUE
				),
				'created_at' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'updated_at' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				)
			) ,
			"keys" => array(
                array("training_code","dealer_code","branch_code"),
			),
		);	


		$arr['tb_trainer_radar'] = array(
			"fields" =>array(
				'quarter' => array(
					'type' => 'INT',
					'constraint' => '1',
					'null' => TRUE
				),
				'year' => array(
					'type' => 'YEAR',
					'constraint' => '4',
					'null' => TRUE
				),
				'trainer_code' => array(
					'type' => 'VARCHAR',
					'constraint' => '20',
					'null' => TRUE
				),
				'report_no' => array(
					'type' => 'VARCHAR',
					'constraint' => '20',
					'null' => TRUE
				),
				'issue_date' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'type' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'sorce_data' => array(
					'type' => 'VARCHAR',
					'constraint' => '200',
					'null' => TRUE
				),
				'recording_date' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'completed_check' => array(
					'type' => 'BOOLEAN',
					'null' => TRUE
				),
				'report_index' => array(
					'type' => 'VARCHAR',
					'constraint' => '200',
					'null' => TRUE
				),
				'created_at' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'updated_at' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				)
			) ,
			"keys" => array(
                array("trainer_code","quarter","year"),
                array("report_index"),
			),
		);

		$arr['tb_best_practice'] = array(
			"fields" =>array(
				'year' => array(
					'type' => 'YEAR',
					'constraint' => '4',
					'null' => TRUE
				),
				'no' => array(
					'type' => 'INT',
					'constraint' => '2',
					'null' => TRUE
				),
				'trainer_code' => array(
					'type' => 'VARCHAR',
					'constraint' => '20',
					'null' => TRUE
				),
				'dealer_code' => array(
					'type' => 'VARCHAR',
					'constraint' => '8',
					'null' => TRUE
				),
				'br_register' => array(
					'type' => 'VARCHAR',
					'constraint' => '3',
					'null' => TRUE
				),
				'category' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'receive_date' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'receiver' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'update_date' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'best_practice' => array(
					'type' => 'BOOLEAN',
					'null' => TRUE
				),
				'approval' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'Index' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'created_at' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'updated_at' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				)
			) ,
			"keys" => array(
                array("best_practice","trainer_code"),
			),
		);	

		$arr['tb_mgr'] = array(
			"fields" =>array(
				'group_a' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'group' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'dealer_code' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'dealer_name' => array(
					'type' => 'VARCHAR',
					'constraint' => '200',
					'null' => TRUE
				),
				'branch_code' => array(
					'type' => 'VARCHAR',
					'constraint' => '200',
					'null' => TRUE
				),
				'location' => array(
					'type' => 'VARCHAR',
					'constraint' => '200',
					'null' => TRUE
				),
				'mgr_code' => array(
					'type' => 'VARCHAR',
					'constraint' => '200',
					'null' => TRUE
				),
				'mgr_name' => array(
					'type' => 'VARCHAR',
					'constraint' => '200',
					'null' => TRUE
				),
				'mgr_surname' => array(
					'type' => 'VARCHAR',
					'constraint' => '200',
					'null' => TRUE
				),
				'crm_q2' => array(
					'type' => 'INT',
					'constraint' => '4',
					'null' => TRUE
				),
				'crm_q3' => array(
					'type' => 'INT',
					'constraint' => '4',
					'null' => TRUE
				),
				'crm_q4' => array(
					'type' => 'INT',
					'constraint' => '4',
					'null' => TRUE
				),
				'act_q1' => array(
					'type' => 'INT',
					'constraint' => '4',
					'null' => TRUE
				),
				'act_q2' => array(
					'type' => 'INT',
					'constraint' => '4',
					'null' => TRUE
				),
				'act_q3' => array(
					'type' => 'INT',
					'constraint' => '4',
					'null' => TRUE
				),
				'act_q4' => array(
					'type' => 'INT',
					'constraint' => '4',
					'null' => TRUE
				),
				'present_cer' => array(
					'type' => 'VARCHAR',
					'constraint' => '10',
					'null' => TRUE
				),
				'created_at' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'updated_at' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				)
			) ,
			"keys" => array(
                array("mgr_code"),
                array("branch_code"),
                array("dealer_code"),
			),
		);
			

		$arr['tb_mgr_manage_showroom'] = array(
			"fields" =>array(
				'group' => array(
					'type' => 'VARCHAR',
					'constraint' => '6',
					'null' => TRUE
				),
				'dealer_code' => array(
					'type' => 'VARCHAR',
					'constraint' => '6',
					'null' => TRUE
				),
				'dealer_name' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'branch_code' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'location_name' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'grade' => array(
					'type' => 'VARCHAR',
					'constraint' => '2',
					'null' => TRUE
				),
				'created_at' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'updated_at' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				)
			) ,
			"keys" => array(
                array("branch_code","dealer_code"),
			),
		);	

		$arr['tb_mgr_ssmi'] = array(
			"fields" =>array(
				'month' => array(
					'type' => 'INT',
					'constraint' => '2',
					'null' => TRUE
				),
				'year' => array(
					'type' => 'YEAR',
					'constraint' => '4',
					'null' => TRUE
				),
				'dealer_code' => array(
					'type' => 'VARCHAR',
					'constraint' => '6',
					'null' => TRUE
				),
				'branch_code' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'initiation' => array(
					'type' => 'INT',
					'constraint' => '4',
					'null' => TRUE
				),
				'salesperson' => array(
					'type' => 'INT',
					'constraint' => '4',
					'null' => TRUE
				),
				'delivery' => array(
					'type' => 'INT',
					'constraint' => '4',
					'null' => TRUE
				),
				'avg' => array(
					'type' => 'INT',
					'constraint' => '4',
					'null' => TRUE
				),
				'score' => array(
					'type' => 'INT',
					'constraint' => '4',
					'null' => TRUE
				),
				'remark' => array(
					'type' => 'TEXT',
					'null' => TRUE
				),
				'created_at' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'updated_at' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				)
			) ,
			"keys" => array(
                array("month","year",'dealer_code','branch_code'),
			),
		);	

		$arr['tb_mgr_testdrive'] = array(
			"fields" =>array(
				'dealer_code' => array(
					'type' => 'VARCHAR',
					'constraint' => '6',
					'null' => TRUE
				),
				'dealer_name' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'jan' => array(
					'type' => 'INT',
					'constraint' => '3',
					'null' => TRUE
				),
				'fab' => array(
					'type' => 'INT',
					'constraint' => '3',
					'null' => TRUE
				),
				'mar' => array(
					'type' => 'INT',
					'constraint' => '3',
					'null' => TRUE
				),
				'apr' => array(
					'type' => 'INT',
					'constraint' => '3',
					'null' => TRUE
				),
				'may' => array(
					'type' => 'INT',
					'constraint' => '3',
					'null' => TRUE
				),
				'jun' => array(
					'type' => 'INT',
					'constraint' => '3',
					'null' => TRUE
				),
				'jul' => array(
					'type' => 'INT',
					'constraint' => '3',
					'null' => TRUE
				),
				'aug' => array(
					'type' => 'INT',
					'constraint' => '3',
					'null' => TRUE
				),
				'sep' => array(
					'type' => 'INT',
					'constraint' => '3',
					'null' => TRUE
				),
				'oct' => array(
					'type' => 'INT',
					'constraint' => '3',
					'null' => TRUE
				),
				'nov' => array(
					'type' => 'INT',
					'constraint' => '3',
					'null' => TRUE
				),
				'dec' => array(
					'type' => 'INT',
					'constraint' => '3',
					'null' => TRUE
				),
				'score' => array(
					'type' => 'INT',
					'constraint' => '4',
					'null' => TRUE
				),
				'created_at' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'updated_at' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				)
			) ,
			"keys" => array(
                array("dealer_code"),
			),
		);	

		$arr['tb_employee_mgr_1'] = array(
			"fields" =>array(
				'dealer_code' => array(
					'type' => 'VARCHAR',
					'constraint' => '6',
					'null' => TRUE
				),
				'branch_code' => array(
					'type' => 'VARCHAR',
					'constraint' => '3',
					'null' => TRUE
				),
				'salesman_code' => array(
					'type' => 'INT',
					'constraint' => '7',
					'null' => TRUE
				),
				'salesman_id' => array(
					'type' => 'INT',
					'constraint' => '11',
					'null' => TRUE
				),
				'firstname' => array(
					'type' => 'VARCHAR',
					'constraint' => '200',
					'null' => TRUE
				),
				'lastname' => array(
					'type' => 'VARCHAR',
					'constraint' => '200',
					'null' => TRUE
				),
				'sex' => array(
					'type' => "enum('M', 'F')",
					'null' => TRUE
				),
				'married' => array(
					'type' => "enum('S', 'M','W','D')",
                    'default' => 'S',
					'null' => TRUE
				),
				'status' => array(
					'type' => "enum('R','A','W','N')",
                    'default' => 'R',
					'null' => TRUE
				),
				'birthday' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'ddms' => array(
					'type' => 'VARCHAR',
					'constraint' => '3',
					'null' => TRUE
				),
				'position' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'fleetsales_code' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'fleetsales_position' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'educational' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'religion' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'start_date' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'approval_date' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'resign_date' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'created_at' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'updated_at' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				)
			) ,
			"keys" => array(
                array("salesman_code"),
                array("salesman_code","ddms","approval_date"),
                array("ddms","approval_date")
			),
		);
		$arr['tb_employee_mgr_2'] = array(
			"fields" =>array(
				'dealer_code' => array(
					'type' => 'VARCHAR',
					'constraint' => '6',
					'null' => TRUE
				),
				'branch_code' => array(
					'type' => 'VARCHAR',
					'constraint' => '3',
					'null' => TRUE
				),
				'salesman_code' => array(
					'type' => 'INT',
					'constraint' => '7',
					'null' => TRUE
				),
				'salesman_id' => array(
					'type' => 'INT',
					'constraint' => '11',
					'null' => TRUE
				),
				'firstname' => array(
					'type' => 'VARCHAR',
					'constraint' => '200',
					'null' => TRUE
				),
				'lastname' => array(
					'type' => 'VARCHAR',
					'constraint' => '200',
					'null' => TRUE
				),
				'sex' => array(
					'type' => "enum('M', 'F')",
					'null' => TRUE
				),
				'married' => array(
					'type' => "enum('S', 'M','W','D')",
                    'default' => 'S',
					'null' => TRUE
				),
				'status' => array(
					'type' => "enum('R','A','W','N')",
                    'default' => 'R',
					'null' => TRUE
				),
				'birthday' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'ddms' => array(
					'type' => 'VARCHAR',
					'constraint' => '3',
					'null' => TRUE
				),
				'position' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'fleetsales_code' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'fleetsales_position' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'educational' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'religion' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'start_date' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'approval_date' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'resign_date' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'created_at' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'updated_at' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				)
			) ,
			"keys" => array(
                array("salesman_code"),
                array("salesman_code","ddms","approval_date"),
                array("ddms","approval_date")
			),
		);
		$arr['tb_employee_mgr_3'] = array(
			"fields" =>array(
				'dealer_code' => array(
					'type' => 'VARCHAR',
					'constraint' => '6',
					'null' => TRUE
				),
				'branch_code' => array(
					'type' => 'VARCHAR',
					'constraint' => '3',
					'null' => TRUE
				),
				'salesman_code' => array(
					'type' => 'INT',
					'constraint' => '7',
					'null' => TRUE
				),
				'salesman_id' => array(
					'type' => 'INT',
					'constraint' => '11',
					'null' => TRUE
				),
				'firstname' => array(
					'type' => 'VARCHAR',
					'constraint' => '200',
					'null' => TRUE
				),
				'lastname' => array(
					'type' => 'VARCHAR',
					'constraint' => '200',
					'null' => TRUE
				),
				'sex' => array(
					'type' => "enum('M', 'F')",
					'null' => TRUE
				),
				'married' => array(
					'type' => "enum('S', 'M','W','D')",
                    'default' => 'S',
					'null' => TRUE
				),
				'status' => array(
					'type' => "enum('R','A','W','N')",
                    'default' => 'R',
					'null' => TRUE
				),
				'birthday' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'ddms' => array(
					'type' => 'VARCHAR',
					'constraint' => '3',
					'null' => TRUE
				),
				'position' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'fleetsales_code' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'fleetsales_position' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'educational' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'religion' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'start_date' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'approval_date' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'resign_date' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'created_at' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'updated_at' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				)
			) ,
			"keys" => array(
                array("salesman_code"),
                array("salesman_code","ddms","approval_date"),
                array("ddms","approval_date")
			),
		);
		$arr['tb_employee_mgr_4'] = array(
			"fields" =>array(
				'dealer_code' => array(
					'type' => 'VARCHAR',
					'constraint' => '6',
					'null' => TRUE
				),
				'branch_code' => array(
					'type' => 'VARCHAR',
					'constraint' => '3',
					'null' => TRUE
				),
				'salesman_code' => array(
					'type' => 'INT',
					'constraint' => '7',
					'null' => TRUE
				),
				'salesman_id' => array(
					'type' => 'INT',
					'constraint' => '11',
					'null' => TRUE
				),
				'firstname' => array(
					'type' => 'VARCHAR',
					'constraint' => '200',
					'null' => TRUE
				),
				'lastname' => array(
					'type' => 'VARCHAR',
					'constraint' => '200',
					'null' => TRUE
				),
				'sex' => array(
					'type' => "enum('M', 'F')",
					'null' => TRUE
				),
				'married' => array(
					'type' => "enum('S', 'M','W','D')",
                    'default' => 'S',
					'null' => TRUE
				),
				'status' => array(
					'type' => "enum('R','A','W','N')",
                    'default' => 'R',
					'null' => TRUE
				),
				'birthday' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'ddms' => array(
					'type' => 'VARCHAR',
					'constraint' => '3',
					'null' => TRUE
				),
				'position' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'fleetsales_code' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'fleetsales_position' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'educational' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'religion' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'start_date' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'approval_date' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'resign_date' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'created_at' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'updated_at' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				)
			) ,
			"keys" => array(
                array("salesman_code"),
                array("salesman_code","ddms","approval_date"),
                array("ddms","approval_date")
			),
		);
		$arr['tb_employee_mgr_5'] = array(
			"fields" =>array(
				'dealer_code' => array(
					'type' => 'VARCHAR',
					'constraint' => '6',
					'null' => TRUE
				),
				'branch_code' => array(
					'type' => 'VARCHAR',
					'constraint' => '3',
					'null' => TRUE
				),
				'salesman_code' => array(
					'type' => 'INT',
					'constraint' => '7',
					'null' => TRUE
				),
				'salesman_id' => array(
					'type' => 'INT',
					'constraint' => '11',
					'null' => TRUE
				),
				'firstname' => array(
					'type' => 'VARCHAR',
					'constraint' => '200',
					'null' => TRUE
				),
				'lastname' => array(
					'type' => 'VARCHAR',
					'constraint' => '200',
					'null' => TRUE
				),
				'sex' => array(
					'type' => "enum('M', 'F')",
					'null' => TRUE
				),
				'married' => array(
					'type' => "enum('S', 'M','W','D')",
                    'default' => 'S',
					'null' => TRUE
				),
				'status' => array(
					'type' => "enum('R','A','W','N')",
                    'default' => 'R',
					'null' => TRUE
				),
				'birthday' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'ddms' => array(
					'type' => 'VARCHAR',
					'constraint' => '3',
					'null' => TRUE
				),
				'position' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'fleetsales_code' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'fleetsales_position' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'educational' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'religion' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'start_date' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'approval_date' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'resign_date' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'created_at' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'updated_at' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				)
			) ,
			"keys" => array(
                array("salesman_code"),
                array("salesman_code","ddms","approval_date"),
                array("ddms","approval_date")
			),
		);
		$arr['tb_employee_mgr_6'] = array(
			"fields" =>array(
				'dealer_code' => array(
					'type' => 'VARCHAR',
					'constraint' => '6',
					'null' => TRUE
				),
				'branch_code' => array(
					'type' => 'VARCHAR',
					'constraint' => '3',
					'null' => TRUE
				),
				'salesman_code' => array(
					'type' => 'INT',
					'constraint' => '7',
					'null' => TRUE
				),
				'salesman_id' => array(
					'type' => 'INT',
					'constraint' => '11',
					'null' => TRUE
				),
				'firstname' => array(
					'type' => 'VARCHAR',
					'constraint' => '200',
					'null' => TRUE
				),
				'lastname' => array(
					'type' => 'VARCHAR',
					'constraint' => '200',
					'null' => TRUE
				),
				'sex' => array(
					'type' => "enum('M', 'F')",
					'null' => TRUE
				),
				'married' => array(
					'type' => "enum('S', 'M','W','D')",
                    'default' => 'S',
					'null' => TRUE
				),
				'status' => array(
					'type' => "enum('R','A','W','N')",
                    'default' => 'R',
					'null' => TRUE
				),
				'birthday' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'ddms' => array(
					'type' => 'VARCHAR',
					'constraint' => '3',
					'null' => TRUE
				),
				'position' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'fleetsales_code' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'fleetsales_position' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'educational' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'religion' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'start_date' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'approval_date' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'resign_date' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'created_at' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'updated_at' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				)
			) ,
			"keys" => array(
                array("salesman_code"),
                array("salesman_code","ddms","approval_date"),
                array("ddms","approval_date")
			),
		);
		$arr['tb_employee_mgr_7'] = array(
			"fields" =>array(
				'dealer_code' => array(
					'type' => 'VARCHAR',
					'constraint' => '6',
					'null' => TRUE
				),
				'branch_code' => array(
					'type' => 'VARCHAR',
					'constraint' => '3',
					'null' => TRUE
				),
				'salesman_code' => array(
					'type' => 'INT',
					'constraint' => '7',
					'null' => TRUE
				),
				'salesman_id' => array(
					'type' => 'INT',
					'constraint' => '11',
					'null' => TRUE
				),
				'firstname' => array(
					'type' => 'VARCHAR',
					'constraint' => '200',
					'null' => TRUE
				),
				'lastname' => array(
					'type' => 'VARCHAR',
					'constraint' => '200',
					'null' => TRUE
				),
				'sex' => array(
					'type' => "enum('M', 'F')",
					'null' => TRUE
				),
				'married' => array(
					'type' => "enum('S', 'M','W','D')",
                    'default' => 'S',
					'null' => TRUE
				),
				'status' => array(
					'type' => "enum('R','A','W','N')",
                    'default' => 'R',
					'null' => TRUE
				),
				'birthday' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'ddms' => array(
					'type' => 'VARCHAR',
					'constraint' => '3',
					'null' => TRUE
				),
				'position' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'fleetsales_code' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'fleetsales_position' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'educational' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'religion' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'start_date' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'approval_date' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'resign_date' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'created_at' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'updated_at' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				)
			) ,
			"keys" => array(
                array("salesman_code"),
                array("salesman_code","ddms","approval_date"),
                array("ddms","approval_date")
			),
		);
		$arr['tb_employee_mgr_8'] = array(
			"fields" =>array(
				'dealer_code' => array(
					'type' => 'VARCHAR',
					'constraint' => '6',
					'null' => TRUE
				),
				'branch_code' => array(
					'type' => 'VARCHAR',
					'constraint' => '3',
					'null' => TRUE
				),
				'salesman_code' => array(
					'type' => 'INT',
					'constraint' => '7',
					'null' => TRUE
				),
				'salesman_id' => array(
					'type' => 'INT',
					'constraint' => '11',
					'null' => TRUE
				),
				'firstname' => array(
					'type' => 'VARCHAR',
					'constraint' => '200',
					'null' => TRUE
				),
				'lastname' => array(
					'type' => 'VARCHAR',
					'constraint' => '200',
					'null' => TRUE
				),
				'sex' => array(
					'type' => "enum('M', 'F')",
					'null' => TRUE
				),
				'married' => array(
					'type' => "enum('S', 'M','W','D')",
                    'default' => 'S',
					'null' => TRUE
				),
				'status' => array(
					'type' => "enum('R','A','W','N')",
                    'default' => 'R',
					'null' => TRUE
				),
				'birthday' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'ddms' => array(
					'type' => 'VARCHAR',
					'constraint' => '3',
					'null' => TRUE
				),
				'position' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'fleetsales_code' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'fleetsales_position' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'educational' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'religion' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'start_date' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'approval_date' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'resign_date' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'created_at' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'updated_at' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				)
			) ,
			"keys" => array(
                array("salesman_code"),
                array("salesman_code","ddms","approval_date"),
                array("ddms","approval_date")
			),
		);
		$arr['tb_employee_mgr_9'] = array(
			"fields" =>array(
				'dealer_code' => array(
					'type' => 'VARCHAR',
					'constraint' => '6',
					'null' => TRUE
				),
				'branch_code' => array(
					'type' => 'VARCHAR',
					'constraint' => '3',
					'null' => TRUE
				),
				'salesman_code' => array(
					'type' => 'INT',
					'constraint' => '7',
					'null' => TRUE
				),
				'salesman_id' => array(
					'type' => 'INT',
					'constraint' => '11',
					'null' => TRUE
				),
				'firstname' => array(
					'type' => 'VARCHAR',
					'constraint' => '200',
					'null' => TRUE
				),
				'lastname' => array(
					'type' => 'VARCHAR',
					'constraint' => '200',
					'null' => TRUE
				),
				'sex' => array(
					'type' => "enum('M', 'F')",
					'null' => TRUE
				),
				'married' => array(
					'type' => "enum('S', 'M','W','D')",
                    'default' => 'S',
					'null' => TRUE
				),
				'status' => array(
					'type' => "enum('R','A','W','N')",
                    'default' => 'R',
					'null' => TRUE
				),
				'birthday' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'ddms' => array(
					'type' => 'VARCHAR',
					'constraint' => '3',
					'null' => TRUE
				),
				'position' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'fleetsales_code' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'fleetsales_position' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'educational' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'religion' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'start_date' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'approval_date' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'resign_date' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'created_at' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'updated_at' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				)
			) ,
			"keys" => array(
                array("salesman_code"),
                array("salesman_code","ddms","approval_date"),
                array("ddms","approval_date")
			),
		);
		$arr['tb_employee_mgr_10'] = array(
			"fields" =>array(
				'dealer_code' => array(
					'type' => 'VARCHAR',
					'constraint' => '6',
					'null' => TRUE
				),
				'branch_code' => array(
					'type' => 'VARCHAR',
					'constraint' => '3',
					'null' => TRUE
				),
				'salesman_code' => array(
					'type' => 'INT',
					'constraint' => '7',
					'null' => TRUE
				),
				'salesman_id' => array(
					'type' => 'INT',
					'constraint' => '11',
					'null' => TRUE
				),
				'firstname' => array(
					'type' => 'VARCHAR',
					'constraint' => '200',
					'null' => TRUE
				),
				'lastname' => array(
					'type' => 'VARCHAR',
					'constraint' => '200',
					'null' => TRUE
				),
				'sex' => array(
					'type' => "enum('M', 'F')",
					'null' => TRUE
				),
				'married' => array(
					'type' => "enum('S', 'M','W','D')",
                    'default' => 'S',
					'null' => TRUE
				),
				'status' => array(
					'type' => "enum('R','A','W','N')",
                    'default' => 'R',
					'null' => TRUE
				),
				'birthday' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'ddms' => array(
					'type' => 'VARCHAR',
					'constraint' => '3',
					'null' => TRUE
				),
				'position' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'fleetsales_code' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'fleetsales_position' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'educational' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'religion' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'start_date' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'approval_date' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'resign_date' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'created_at' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'updated_at' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				)
			) ,
			"keys" => array(
                array("salesman_code"),
                array("salesman_code","ddms","approval_date"),
                array("ddms","approval_date")
			),
		);
		$arr['tb_employee_mgr_11'] = array(
			"fields" =>array(
				'dealer_code' => array(
					'type' => 'VARCHAR',
					'constraint' => '6',
					'null' => TRUE
				),
				'branch_code' => array(
					'type' => 'VARCHAR',
					'constraint' => '3',
					'null' => TRUE
				),
				'salesman_code' => array(
					'type' => 'INT',
					'constraint' => '7',
					'null' => TRUE
				),
				'salesman_id' => array(
					'type' => 'INT',
					'constraint' => '11',
					'null' => TRUE
				),
				'firstname' => array(
					'type' => 'VARCHAR',
					'constraint' => '200',
					'null' => TRUE
				),
				'lastname' => array(
					'type' => 'VARCHAR',
					'constraint' => '200',
					'null' => TRUE
				),
				'sex' => array(
					'type' => "enum('M', 'F')",
					'null' => TRUE
				),
				'married' => array(
					'type' => "enum('S', 'M','W','D')",
                    'default' => 'S',
					'null' => TRUE
				),
				'status' => array(
					'type' => "enum('R','A','W','N')",
                    'default' => 'R',
					'null' => TRUE
				),
				'birthday' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'ddms' => array(
					'type' => 'VARCHAR',
					'constraint' => '3',
					'null' => TRUE
				),
				'position' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'fleetsales_code' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'fleetsales_position' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'educational' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'religion' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'start_date' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'approval_date' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'resign_date' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'created_at' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'updated_at' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				)
			) ,
			"keys" => array(
                array("salesman_code"),
                array("salesman_code","ddms","approval_date"),
                array("ddms","approval_date")
			),
		);
		$arr['tb_employee_mgr_12'] = array(
			"fields" =>array(
				'dealer_code' => array(
					'type' => 'VARCHAR',
					'constraint' => '6',
					'null' => TRUE
				),
				'branch_code' => array(
					'type' => 'VARCHAR',
					'constraint' => '3',
					'null' => TRUE
				),
				'salesman_code' => array(
					'type' => 'INT',
					'constraint' => '7',
					'null' => TRUE
				),
				'salesman_id' => array(
					'type' => 'INT',
					'constraint' => '11',
					'null' => TRUE
				),
				'firstname' => array(
					'type' => 'VARCHAR',
					'constraint' => '200',
					'null' => TRUE
				),
				'lastname' => array(
					'type' => 'VARCHAR',
					'constraint' => '200',
					'null' => TRUE
				),
				'sex' => array(
					'type' => "enum('M', 'F')",
					'null' => TRUE
				),
				'married' => array(
					'type' => "enum('S', 'M','W','D')",
                    'default' => 'S',
					'null' => TRUE
				),
				'status' => array(
					'type' => "enum('R','A','W','N')",
                    'default' => 'R',
					'null' => TRUE
				),
				'birthday' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'ddms' => array(
					'type' => 'VARCHAR',
					'constraint' => '3',
					'null' => TRUE
				),
				'position' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'fleetsales_code' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'fleetsales_position' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'educational' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'religion' => array(
					'type' => 'VARCHAR',
					'constraint' => '100',
					'null' => TRUE
				),
				'start_date' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'approval_date' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'resign_date' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'created_at' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				),
				'updated_at' => array(
					'type' => 'DATETIME',
					'null' => TRUE
				)
			) ,
			"keys" => array(
                array("salesman_code"),
                array("salesman_code","ddms","approval_date"),
                array("ddms","approval_date")
			),
		);
		
		




		


$config['table_settings'] = $arr;
