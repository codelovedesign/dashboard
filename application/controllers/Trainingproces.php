<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ini_set('max_execution_time', 3000000); //300 seconds = 5 minutes30
ini_set("memory_limit","2024M");//128

class Trainingproces extends MY_Controller {

    var $year = 2016;
    var $set=2 ;
    var $db2;
    var $dbname_default = "saleaward_";

    var $current_date ;
    var $page_level_js = array(
		'assets/plugins/parsley/dist/parsley.js',
		'assets/js/award-processor.js',
	);
	public function __construct(){
		parent::__construct();

        $this->load->helper(array('form', 'url'));
        $this->current_date = date("Y-m-d h:i:s");
        $this->load->library('form_validation');

        $this->load->model('employee_model');
        $this->load->model('award_model');
        $this->load->model('set_model');

        $database_new_name = $this->dbname_default. "2016" ;
        $this->db2 = $this->load->database("otherdb", TRUE);
        $this->db2->db_select($database_new_name);
        $this->set = 2;


	}
    public function index(){
    }

    public function test(){

        $database_new_name = $this->dbname_default. "2016" ;
        $this->db2 = $this->load->database("otherdb", TRUE);
        $this->db2->db_select($database_new_name);
        $this->set = 2;

        $series_arr  = array(
            array("CARY"),
            array("COLA"),
            array("VIOS"),
            array("YARI"),
            array("HRBC"),
            array("HR4D"),
            array("HLPV"),
            array("AVAN"),
            array("INNO"),
        );

        $this->update_ssmi_score();

        $dealer_code = "11312";
        $branch_code = "KN";

        //2
        $point2 = 0;
        echo $this->get_training("210T016");
        // echo $this->get_ssmi_score($dealer_code,$branch_code);
        // $point2_2 +=$this->get_interview_score($dealer_code,$branch_code);

        
    }

    public function compile(){

        $this->year = $this->input->post('year');
        $this->set = $this->input->post('set_award');
        $employee_level = $this->input->post('employee_level');
        $award_type = $this->input->post('award_type');


        if(empty($this->year) || empty($this->set)){
            redirect('Processor');
        }
		
        $r =  $this->award_model->get_award_type_by_id($award_type);
        if($r)
        {
            $database_new_name = $this->dbname_default. $this->year ;
            $this->db2 = $this->load->database("otherdb", TRUE);
            $this->db2->db_select($database_new_name);

            // $this->update_ssmi_score();
            // var_dump($this->db2);
            if($award_type != 7){
                $license_arr=json_decode($r['license'],true) ;
                $arr = array($license_arr,$r['license_count'],$r['data_type'] ,$r['limit']);
                // echo "กำลังประมวลผล...";
                $this->run($arr);
            }
            else
            {
                $this->halloffame();
            }
        }
     
    }

    public function halloffame(){
        $where=array(
            'year >' => $this->year -3 ,
        );
        $this->db2->select('sale_code,count(year) as count,GROUP_CONCAT(year)');
        $this->db2->group_by("sale_code"); // Produces: GROUP BY title
        $this->db2->having('count(year)','2');  // Produces: HAVING user_id = 45
        $query = $this->db2->get_where('tb_history', $where );
        // echo $this->db->last_query();
        // echo $query->num_rows() ;
        // exit();
        $sale_code_arr = array();
        if ($query->num_rows() > 0) {
            $result = $query->result();
            foreach ($result as $rs) {
             $sale_code_arr[] = $rs->sale_code ;
            //  echo $rs->sale_code . "<br/>";
            }         
           unset($result,$rs);
        }

        $this->db2->select('*');
        $this->db2->where("tb_award.order >",0);
        $this->db2->where("year",$this->year);
        $this->db2->where("type <",4);
        if(count($sale_code_arr) > 0 ) {
            $this->db2->where_in("sale_code",$sale_code_arr);
        } 
        $query = $this->db2->get("tb_award");

        $sale_code_arr = array();
        if ($query->num_rows() > 0) {
            $result = $query->result();
            foreach ($result as $rs) {
                // var_dump($rs);
                // echo $rs->sale_code . "<br/>";
                $arr = array(
                    "type"=> 4,
                    "sale_code"=> $rs->sale_code ,
                    "name"=> $rs->name ,
                    "lastname"=> $rs->lastname ,
                    "branch_code"=> $rs->branch_code ,
                    "dealer_code"=> $rs->dealer_code ,
                    "total_score"=> $rs->total_score ,
                    "year"=> $this->year ,
                    "created_at"=> $this->current_date ,
                );

                $this->insert_sales_award($arr);
            }         
           unset($result,$rs);
           $this->create_order(4);

        }

        redirect('competition/' . $this->year , 'refresh');

    }

    public function run($param = array()){
        
        $starttime = microtime(true);
        $this->update_ssmi_score();

        
        $sql= "SELECT * FROM tb_trainer" ;
        $query = $this->db2->query($sql);

        echo "<pre/>";
        // echo $query->num_rows();
        if ($query->num_rows() > 0) {
            $result = $query->result();
            foreach ($result as $rs) {
                
                // $group_id = 0;
                // $this->db2->where("dealer_code", $rs->dealer_code );
                // $query_group = $this->db2->get('tb_dealers');
                // if ($query_group->num_rows() > 0) {
                //     $r = $query_group->row_array();
                //     $group_id =  $r['group_id'] ; 
                //     $dealer_name =  $r['dealer_name'] ; 
                // }
              
               $code = $rs->trainer_code; 
            //    echo "<br/>";
                $dealer_code = $rs->dealer_code;
                $branch_code = $rs->branch_code;
                $dealer_name = $rs->dealer_name;


                $point1 = 0;
                $point12 = 0;
                $point13 = 0;
                $point14 = 0;
                $point12 = 0;
                $point13 = 0;
                $point14 = 0;
                $point2_1 = 0;
                $point2_2 = 0;
                $point5 = 0;
                $point52 = 0;


                $point1 =$this->get_interview_score($dealer_code,$branch_code);
                $point12 =$this->get_exam_score($dealer_code,$branch_code);
                $point13 =$this->get_tsclicense($dealer_code,$branch_code);
                $point14 =$this->get_ssmi_score($dealer_code,$branch_code);

                $point2_1 =$this->get_training($code);
                $point2_2 =$this->get_sm_report($code);
                
                $point4 = 500;

                
                $point5 = $this->get_trainerRader($code);
                $point52 = $this->get_practiceReport($code);
            

                $score = $point1 + $point12 + $point13 + $point14 + $point2_1 + $point2_2 + $point4 + $point5 + $point52 ;
                // $point3 +=$this->get_ssmi_score($dealer_code,$branch_code);
                // $score += $point3 ;

                // echo  "1.1 =>" . $point1 ."<br/>";
                // echo  "1.2 =>" . $point12 ."<br/>";
                // echo  "1.3 =>" . $point13 ."<br/>";
                // echo  "1.4 =>" . $point14 ."<br/>";
                // echo  "2.1 =>" . $point2_1 ."<br/>";
                // echo  "2.2 =>" . $point2_2 ."<br/>";
                // echo  "5.1 =>" . $point5 ."<br/>";
                // echo  "5.2 =>" . $point52 ."<br/>";
            
                // echo "--------------------------<br/>";
                
                $arr = array(
                    "type"=> 5 ,
                    "sale_code"=> $code ,
                    "name"=> $rs->firstname ,
                    "lastname"=> $rs->lastname ,
                    "branch_code"=> $rs->branch_code ,
                    "dealer_code"=> $rs->dealer_code ,
                    "dealer_name"=> $dealer_name ,
                    "total_score"=> $score ,
                    "year"=> $this->year ,
                    "created_at"=> $this->current_date ,
                );

                $this->insert_sales_award($arr , 5 );

            }
            exit();

            $this->create_order($param[2],$param[3]);
            redirect('competition/' . $this->year , 'refresh');
            //จัดอันดับ     
            $endtime = microtime(true);
            echo $duration = $endtime - $starttime; //calculates total time taken
            echo " เสร็จแล้ว";    

        }
    }
    
     public function get_training($code){
         $where = array(
          'training_code' => $code,
          'year' => $this->year,
        );
        $query = $this->db2->get_where('tb_training_report', $where );
        // echo $this->db2->last_query();

        $score = 0;
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            // var_dump($result);
            foreach ($result as $rs) {
                $quarter = $rs['quarter'] ;
                $type_a = $rs['type_a'] ;
                $type_b = $rs['type_b'] ;
                $type_c = $rs['type_c'] ;
                $type_d = $rs['type_d'] ;

                $where = array(
                    "quarter"=> $quarter ,
                    'set_id'=> $this->set ,
                );
                $query = $this->db->get_where('tb_training_rating', $where );
                // $this->db->last_query();
                if ($query->num_rows() > 0) {
                    $r = $query->row_array();
                    $score +=  $r['score_a'] ; 
                    $score +=  $r['score_b'] ; 
                    $score +=  $r['score_c'] ; 
                    $score +=  $r['score_d'] ; 
                }
            }         
        }
        return $score ;
   }

   public function get_trainerRader($code){
       $where = array(
          'trainer_code' => $code,
          'year' => $this->year,
          'completed_check' => '1',
        );
         $this->db2->group_by('quarter'); 
        $query = $this->db2->get_where('tb_trainer_radar', $where );
        $score = 0;
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            foreach ($result as $rs) {
                $quarter = $rs['quarter'] ;
               
                $where = array(
                    "quarter"=> $quarter ,
                    'set_id'=> $this->set ,
                );
                $query = $this->db->get_where('tb_radar_rating', $where );
                // $this->db->last_query();
                if ($query->num_rows() > 0) {
                    $r = $query->row_array();
                    $score +=  $r['score'] ; 
                }
            }         
        }
        return $score ;
   }


   public function get_practiceReport($code){
        $where = array(
          'trainer_code' => $code,
          'year' => $this->year,
          'no <' => '5',
        );
         $this->db2->order_by('trainer_code', 'desc'); 
         $this->db2->order_by('no', 'asc'); 

        $query = $this->db2->get_where('tb_best_practice', $where );
        $score = 0;
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            foreach ($result as $rs) {
                $best_practice = $rs['best_practice'] ;
                $no = $rs['no'] ;
               
                $where = array(
                    "is_best"=> $best_practice ,
                    'set_id'=> $this->set ,
                );
                $query = $this->db->get_where('tb_best_practice_rating', $where );
                // $this->db->last_query();
                $score =0 ;
                if ($query->num_rows() > 0) {
                    $r = $query->row_array();
                    

                    switch ($no) {
                        case 1:
                            $score +=  $r['no1_score'] ;
                            break;
                        case 2:
                            $score +=  $r['no2_score'] ;
                            break;
                        case 3:
                            $score +=  $r['no3_score'] ;
                            break;
                        case 4:
                            $score +=  $r['no4_score'] ;
                            break;
                        
                        default:
                            $score = 0;
                            break;
                    }

                }
            }         
        }
        return $score ;
   }

   public function get_sm_report($code) {
         $where = array(
          'training_code' => $code,
        //   'year' => $this->year,
        );
        $query = $this->db2->get_where('tb_sm_report', $where );
        $score = 0;
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            foreach ($result as $rs) {
                $score += $rs['q1'] ;
                $score += $rs['q2'] ;
                $score += $rs['q3'] ;
                $score += $rs['q4'] ;
               
            }
        }

        return $score;
   }


    public function get_tsclicense($dealer_code,$branch_code){
        $end ='31-12-'.$this->year ;
        $arr = $this->get_sale_branch($dealer_code,$branch_code,$end);
        $total = count($arr);
        $tsc_arr = $this->get_sale_award(array("TSC","RSC"),1,$arr);
        $tsc = count($tsc_arr) ;

        if($total > 0){
            $percentage= ($tsc / $total) *100 ;
        }
        else
        {
            $percentage = 0;
        }

        $score = 0 ;
        $where = array(
            'set_id' => $this->set,
        );

        $query = $this->db->get_where('tb_performance_rating', $where );
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            // var_dump($result); 
            foreach ($result as $rs) {

                $operation = $rs['operation'] ;
                $point1 = $rs['point'] ;
                $point2 = $rs['point2'] ;

                $condition = false ;
                switch ($operation) {
                    case 1 :
                    if($percentage > $point1){
                        $condition = true ;
                    } 
                        break;
                    case 2 :
                    if($percentage >= $point1){
                        $condition = true ;
                    } 
                        break;
                    case 3 :
                    if($percentage < $point1){
                        $condition = true ;
                    } 
                        break;
                    case 4 :
                    if($percentage <= $point1){
                        $condition = true ;
                    } 
                        break;
                    case 5 :
                    if($percentage = $point1){
                        $condition = true ;
                    } 
                        break;
                    case 6 :
                    if($percentage >= $point1 && $percentage <= $point2){
                        $condition = true ;
                    } 
                        break;
                    default:
                        $condition = false ;
                        break;
                }

                    if($condition)
                    {
                        $score  = $rs['score'] ;
                        break;
                    }

            }         
        }

        return $score ;

    }
    public function get_sale_branch($dealer_code,$branch_code , $enddate = null ){
        // $approval_date = date('Y-m-d',strtotime($this->year .'-12-31')) ;

        $sql= "SELECT salesman_code FROM tb_trainer_tsc" ;
        $sql .= " WHERE ddms IN ('01','02','03','04') " ;
        if($enddate) {
            $sql .= " AND resign_date <= '{$enddate}'" ;
        }
        // $sql .= " AND approval_date <= '{$approval_date}'" ;
       $sql .= " AND dealer_code ='{$dealer_code}' AND branch_code = '{$branch_code}' " ;
        
        $query = $this->db2->query($sql);

        $data = array();
        if ($query->num_rows() > 0) {
            $result = $query->result();
            foreach ($result as $rs) {
                $data[]= $rs->salesman_code;
            }
        }

        return $data ;
    }

     public function get_sale_award($name_award = array(), $number_course=1 , $sale_arr = array() ){

        $this->db2->select("*,count(code) as sumcode ,GROUP_CONCAT(code SEPARATOR ',') ")
        ->where_in("code", $name_award )
        ->where("result","P")
        ->having("sumcode >=",$number_course) 
        ->group_by(array('sale_code')); 

        if(!count($sale_arr))
        {
            $sale_arr=array(''); 
        }
        $this->db2->where_in("sale_code",$sale_arr) ;
        // ->group_by(array('code','sale_code')); 
        $query = $this->db2->get('tb_license');
        //  echo $this->db->last_query();

        // echo $query->num_rows();
        $sale_code_arr = array();
        if ($query->num_rows() > 0) {
            $result = $query->result();
            foreach ($result as $rs) {
               $sale_code_arr[] = $rs->sale_code ;
            }         
           unset($result,$rs);
        }

        return $sale_code_arr;
   }

    public function get_exam_score($dealer_code,$branch_code) {

         $Quarter_arr = array(
             '31-3-'.$this->year ,
             '30-6-'.$this->year ,
             '30-9-'.$this->year ,
             '31-12-'.$this->year ,
         );
        
        $score = 0;

         foreach($Quarter_arr as $key => $q ){
            $emp_arr=$this->get_sale_branch($dealer_code,$branch_code,$q) ;
            $total = count($emp_arr);
            $where = array(
                'exam_code' => ($key+1),
                'exam_year' => $this->year,
            );
            // $this->db2->limit(4);
            if(count($emp_arr)==0)
            {
                continue;
            }
            $this->db2->where_in('salesman_code', $emp_arr );
            $query = $this->db2->get_where('tb_trainer_exam', $where );
            $rowcount = $query->num_rows();
            $countpass =0 ;
            if ($query->num_rows() > 0) {
                $result = $query->result_array();
                
                foreach ($result as $rs) {
                    $exam_score = $rs['exam_score'] ;
                    if($exam_score >= 16)
                    {
                        $countpass++;
                    }
                }
             }
             
            $percentage = ($rowcount /$total ) *100;
            $rateScorePass = ($countpass /$total ) *100;
            if($countpass==0)
            {
                $rateScorePass = 0 ;
            }
            // $rateScorePass = 90;
            $where = array(
                'set_id' => $this->set,
            );
            $query = $this->db->get_where('tb_tweb_rating', $where );
            // echo $this->db->last_query();

            if ($query->num_rows() > 0) {
                $result = $query->result_array();
                // var_dump($result); 
                foreach ($result as $rs) {

                    $operation = $rs['operation'] ;
                    $point1 = $rs['exam_rate'] ;
                    $point2 = $rs['point2'] ;

                    // echo $percentage ." || " . $rateScorePass;
                    $condition = false ;
                    switch ($operation) {
                        case 1 :
                        if($percentage > $point1){
                            $condition = true ;
                        } 
                            break;
                        case 2 :
                        if($percentage >= $point1){
                            $condition = true ;
                        } 
                            break;
                        case 3 :
                        if($percentage < $point1){
                            $condition = true ;
                        } 
                            break;
                        case 4 :
                        if($percentage <= $point1){
                            $condition = true ;
                        } 
                            break;
                        case 5 :
                        if($percentage = $point1){
                            $condition = true ;
                        } 
                            break;
                        case 6 :
                        if($percentage >= $point1 && $percentage <= $point2){
                            $condition = true ;
                        } 
                            break;
                        default:
                            $condition = false ;
                            break;
                    }

                     if($condition)
                     {
                        if($rateScorePass >= 90){
                            $score  += $rs['ninety'] ;
                            //  echo $rs['ninety'] . "  xx<br/>";
                        }
                        else if($rateScorePass >= 80)
                        {
                            $score  += $rs['eighty'] ;
                            //  echo $rs['eighty'] . "  xx<br/>";
                        }


                        break;
                      }


                }         
            }
         }

        
        return $score ;
   }









    //ประสิทธิภาพการขาย รายเดือน
    public function get_sale($id){
         //ดึงยอดขายปีการแข่งขัน

        // $this->db->select('count(id) as total');

         $where = array(
        //   'dl_month <=' => date('Y-m-d',strtotime($this->year.'-12-31')) ,
        //   'dl_month >=' => date('Y-m-d',strtotime($this->year.'-1-1')) ,
        //   'fleet_id IS NULL' => null,
          'salesman_code' => $id,
        );
        // $this->db->group_by('MONTH(dl_month)'); 

        $query = $this->db->get_where('tb_sales', $where );
        // echo $this->db->last_query();
        $result =  array();
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            // var_dump($result); 
            // return $score ;
            // foreach ($result as $rs) {
                // $total = $rs['total'];
                // $score += $this->SalesPerformance($total , $group_id);
            // }         
        }

        return $result ;
        // $this->db->close();




    }

   public function create_order($type,$max= null){

    

    $database_new_name = $this->dbname_default. $this->year ;
    $this->db2 = $this->load->database("otherdb", TRUE);
    $this->db2->db_select($database_new_name);

    // $this->db2->where('type', $type);
    $this->db2->update('tb_award', array("order"=>""));

    $where = array(
        "type" => $type ,
    );
    if($max){
        $this->db2->limit($max);  
    }
    $this->db2->order_by('total_score', 'desc'); 
    $query = $this->db2->get_where('tb_award', $where );

    if ($query->num_rows() > 0) {
        $result = $query->result();
        foreach ($result as $key =>$rs) {
            $arr = array(
            "order" => ($key +1 ),
            );

            $this->db2->where('id', $rs->id);
            $this->db2->update('tb_award', $arr);
            // echo $this->db->last_query();
        }   
    }
   
   } 

   public function insert_sales_award($arr , $limit = null){
    
    //แต่ละรางวัล ห้ามซ้ำ ถ้าหากมีข้อมูลซ้ำจะอัพเดทแทน
    if($arr['type'] < 4){
        $this->db2->where("type <", $arr['type']);
        $this->db2->where("order >" , "0");
        // $this->db->order_by('score', 'desc'); 
        // $this->db->limit($limit);
    }
    else{
        $this->db2->where("type", $arr['type'] );
    }

    $query = $this->db2->get_where('tb_award', array("sale_code" => $arr['sale_code']) );
                // echo $this->db->last_query();


    if ($query->num_rows() == 0) {

           $query = $this->db2->get_where('tb_award', array("sale_code" => $arr['sale_code'] ));
           if ($query->num_rows() == 0) {
                $this->db2->insert('tb_award', $arr);
           }
           else{
               $row = $query->row();
                if (isset($row))
                {
                    $arr2=array("updated_at"=>$this->current_date);
                    $arr=(array_merge($arr,$arr2));
                    unset($arr['created_at']);

                    $this->db2->where('id', $row->id);
                    $this->db2->update('tb_award', $arr);

                }
           }

    }
    else if($arr['type'] == 4)
    {
        $row = $query->row();
        if (isset($row))
        {
            $arr2=array("updated_at"=>$this->current_date);
            $arr=(array_merge($arr,$arr2));
            unset($arr['created_at']);

            $this->db2->where('id', $row->id);
            $this->db2->update('tb_award', $arr);

        }
    }
    else
    {
        $row = $query->row();

        // var_dump($row);
        if (isset($row))
        {
            $this->db2->where('sale_code', $arr['sale_code']);
            $this->db2->where('type', $arr['type']);
            $this->db2->delete('tb_award');

        }
    }
      
   }


 
  public function get_interview_score($dealer_code,$branch_code) {
         $where = array(
          'dealer_code' => $dealer_code,
          'branch_code' => $branch_code,
          'year' => $this->year,
        );
        $this->db2->limit(4);
        $query = $this->db2->get_where('tb_telephone_survey', $where );
        // echo $this->db2->last_query();
        $score = 0;
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            // var_dump($result); 
            foreach ($result as $rs) {
                $quarter = $rs['quarter'] ;
                $rating_answer = $rs['rating_answer'] ;

                $score += $this->call_interview_rat($quarter, $rating_answer);
            }         
        }

        // var_dump($score);
        return $score ;
   }


   public function call_interview_rat($quarter,$rate=null){
        $where = array(
          'quarter' => $quarter ,
          'rate' => strtoupper($rate) ,
          'set_id' => $this->set ,
        );

        $this->db->limit(1);
        $this->db->select('score');
        $query = $this->db->get_where('tb_interview_rating', $where );
        // $this->db->last_query();
        $score =0 ;
        if ($query->num_rows() > 0) {
            $r = $query->row_array();
            $score =  $r['score'] ; 
        }

        return $score ;

   }

   public function update_ssmi_score() {
         $where = array(
        //   'dealer_code' => $dealer_code,
        //   'branch_code' => $branch_code,
          'year' => $this->year,
        );
        // $this->db->limit(12);
        $query = $this->db2->get_where('tb_trainer_ssmi', $where );
        // echo $this->db->last_query();
        $score = 0;
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            foreach ($result as $rs) {
                $ssmi = $rs['ssmi'] ;
                $ssmi = (int)$ssmi;
                $month = $rs['month'] ;
                $score=$this->call_ssmi_score($ssmi,$month);
                
                $arr = array(
                    "score" => $score ,
                );

                $this->db2->where('id', $rs['id']);
                $this->db2->update('tb_trainer_ssmi', $arr);

            }         
        }

        // echo " update_ssmi_score success " . $query->num_rows() ;

        return $score ;
   }
   private function call_ssmi_score($ssmi,$month) {

         $where = array(
          'month' => $month,
          'set_id' => $this->set,
        );
        // $this->db->limit(12);
        $query = $this->db->get_where('tb_ssmi_rating', $where );
        // echo $this->db->last_query();
        $score = 0;
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            foreach ($result as $rs) {
                $operation = $rs['operation'] ;
                $point1 = $rs['point1'] ;
                $point2 = $rs['point2'] ;

                $condition = false ;
                switch ($operation) {
                    case 1 :
                       if($ssmi > $point1){
                        $condition = true ;
                       } 
                        break;
                    case 2 :
                       if($ssmi >= $point1){
                        $condition = true ;
                       } 
                        break;
                    case 3 :
                       if($ssmi < $point1){
                        $condition = true ;
                       } 
                        break;
                    case 4 :
                       if($ssmi <= $point1){
                        $condition = true ;
                       } 
                        break;
                    case 5 :
                       if($ssmi = $point1){
                        $condition = true ;
                       } 
                        break;
                    case 6 :
                       if($ssmi >= $point1 && $ssmi <= $point2){
                        $condition = true ;
                       } 
                        break;
                    default:
                       $condition = false ;
                        break;
                }


                if($condition)
                {
                  $score  = $rs['score'] ;
                  break;
                }

            }         
        }

        return $score ;
   }

   public function get_ssmi_score($dealer_code,$branch_code) {
         $where = array(
          'dealer_code' => $dealer_code,
          'branch_code' => $branch_code,
          'year' => $this->year,
        );
        $this->db2->limit(12);
        $query = $this->db2->get_where('tb_trainer_ssmi', $where );
        // echo $this->db2->last_query();
        $score = 0;
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            foreach ($result as $rs) {
                $score += $rs['score'] ;
            }         
        }


        return $score ;
   }

   public function get_condition($name){

        $where = array(
          'name' => $name ,
          'set_id' => $this->set ,
        );
        $this->db->limit(1);

        $query = $this->db->get_where('tb_condition', $where );
        // $this->db->last_query();
        $score =0 ;
        if ($query->num_rows() > 0) {
            $r = $query->row_array();
            $score =  $r['score'] ; 
        }

        return $score ;

   }


    //ประสิทธิภาพการขาย รายเดือน
    public function get_Month_Performance($id , $group_id , $series_arr){
         //ดึงยอดขายปีการแข่งขัน
        $series = array() ;
        foreach($series_arr as $item){
            $series = array_merge($series,$item);
        }  

        // var_dump($series);
        $this->db2->select('count(id) as total');

         $where = array(
          'dl_month <=' => date('Y-m-d',strtotime($this->year.'-12-31')) ,
          'dl_month >=' => date('Y-m-d',strtotime($this->year.'-1-1')) ,
        //   'fleet_id IS NULL' => null,
          'salesman_code' => $id,
        //   'sorce' => 'DL',
        );
        $this->db2->where_in("series", $series);

        $this->db2->group_by('MONTH(dl_month)'); 

        $query = $this->db2->get_where('tb_sales_summary', $where );
        // echo $this->db->last_query();
        $score = 0;
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            // var_dump($result); 
            foreach ($result as $rs) {
                $total = $rs['total'];
                $score += $this->SalesPerformance($total , $group_id);
            }         
        }


        return $score ;

    }

    public function SalesPerformance($total,$group_id){

         $where = array(
          'units <=' => $total ,
          'group_id' => $group_id ,
          'set_id' => $this->set ,
          'active' => '1' ,
        );

        $this->db->order_by('units', 'desc'); 
        $this->db->limit(1);

        $this->db->select('score');
        $query = $this->db->get_where('tb_month_score', $where );
        // echo $this->db->last_query();
        // $this->db->last_query(); ÷÷ 
        $score =0 ;
        if ($query->num_rows() > 0) {
            $r = $query->row_array();
            $score =  $r['score'] ; 
        }
        return $score ;
    }

	
}
