<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ini_set('max_execution_time', 3000000); //300 seconds = 5 minutes30
ini_set("memory_limit","2024M");//128

class Mgrprocessor extends MY_Controller {

    var $year = 2016;
    var $set=3 ;
    var $db2;
    var $dbname_default = "saleaward_";

    var $current_date ;
    var $page_level_js = array(
		'assets/plugins/parsley/dist/parsley.js',
		'assets/js/award-processor.js',
	);
	public function __construct(){
		parent::__construct();

        $this->load->helper(array('form', 'url'));
        $this->current_date = date("Y-m-d h:i:s");
        $this->load->library('form_validation');

        $this->load->model('employee_model');
        $this->load->model('award_model');
        $this->load->model('set_model');

        $database_new_name = $this->dbname_default. "2016" ;
        $this->db2 = $this->load->database("otherdb", TRUE);
        $this->db2->db_select($database_new_name);
        $this->set = 3;


	}
    public function index(){
    }

    public function test(){

        $database_new_name = $this->dbname_default. "2016" ;
        $this->db2 = $this->load->database("otherdb", TRUE);
        $this->db2->db_select($database_new_name);
        $this->set = 2;

        $series_arr  = array(
            array("CARY"),
            array("COLA"),
            array("VIOS"),
            array("YARI"),
            array("HRBC"),
            array("HR4D"),
            array("HLPV"),
            array("AVAN"),
            array("INNO"),
        );

        $this->update_ssmi_score();

        $dealer_code = "11312";
        $branch_code = "KN";

        //2
        $point2 = 0;
        echo $this->get_training("210T016");
        // echo $this->get_ssmi_score($dealer_code,$branch_code);
        // $point2_2 +=$this->get_interview_score($dealer_code,$branch_code);

        
    }

    public function compile(){

        $this->year = $this->input->post('year');
        $this->set = $this->input->post('set_award');
        $employee_level = $this->input->post('employee_level');
        $award_type = $this->input->post('award_type');


        if(empty($this->year) || empty($this->set)){
            redirect('Processor');
        }
		
        $r =  $this->award_model->get_award_type_by_id($award_type);
        if($r)
        {
            $database_new_name = $this->dbname_default. $this->year ;
            $this->db2 = $this->load->database("otherdb", TRUE);
            $this->db2->db_select($database_new_name);

            // $this->update_ssmi_score();
            // var_dump($this->db2);
            if($award_type != 7){
                $license_arr=json_decode($r['license'],true) ;
                $arr = array($license_arr,$r['license_count'],$r['data_type'] ,$r['limit']);
                // echo "กำลังประมวลผล...";
                $this->run($arr);
            }
           
        }
     
    }

  
    public function run($param = array()){
        
        $starttime = microtime(true);
        $this->update_ssmi_score();

        $sql= "SELECT * FROM tb_mgr " ;
        $query = $this->db2->query($sql);

        // echo $query->num_rows();
        if ($query->num_rows() > 0) {
            $result = $query->result();
            foreach ($result as $rs) {
                
                // $group_id = 0;
                $this->db2->where("dealer_code", $rs->dealer_code );
                $query_group = $this->db2->get('tb_dealers');
                if ($query_group->num_rows() > 0) {
                    $r = $query_group->row_array();
                    $group_id =  $r['group_id'] ; 
                    $dealer_name =  $r['dealer_name'] ; 
                }
              
                $code = $rs->mgr_code; 
                $dealer_code = $rs->dealer_code;
                $branch_code = $rs->branch_code;
                $dealer_name = $rs->dealer_name;

                // echo $rs->mgr_name ; 
                // echo " ";
                // echo $rs->mgr_surname ;
                // echo " ";
                $score = 0;


                $score +=$this->get_month_avg($dealer_code,$branch_code);
                $score +=$this->get_mgr_crm($branch_code,$dealer_code);
                $score +=$this->get_mgr_activity($branch_code,$dealer_code);
                $score +=$this->get_mgr_testdrive($branch_code,$dealer_code);
                $score +=$this->get_ssmi_score($dealer_code,$branch_code);
                $score +=$this->get_mgr_priceSurveyScore($branch_code,$dealer_code);
                $score +=$this->get_mgr_saleman($branch_code,$dealer_code);
                $score +=$this->get_mgr_manage_showroom($branch_code,$dealer_code);
               
                // echo "----<br/>";
                
                $arr = array(
                    "type"=> 7 ,
                    "sale_code"=> $code ,
                    "name"=> $rs->mgr_name ,
                    "lastname"=> $rs->mgr_surname ,
                    "branch_code"=> $rs->branch_code ,
                    "dealer_code"=> $rs->dealer_code ,
                    "dealer_name"=> $dealer_name ,
                    "total_score"=> $score ,
                    "year"=> $this->year ,
                    "created_at"=> $this->current_date ,
                );

                $this->insert_sales_award($arr , 7 );

            }
            // exit();

            $this->create_order($param[2],$param[3]);
            redirect('competition/' . $this->year , 'refresh');
            //จัดอันดับ     
            $endtime = microtime(true);
            echo $duration = $endtime - $starttime; //calculates total time taken
            echo " เสร็จแล้ว";    

        }
    }


    public function insert_sales_award($arr , $limit = null){
   
    $this->db2->where("type", $arr['type'] );
    $query = $this->db2->get_where('tb_award', array("sale_code" => $arr['sale_code']) );
    // echo $this->db->last_query();
    if ($query->num_rows() == 0) {

           $query = $this->db2->get_where('tb_award', array("sale_code" => $arr['sale_code'] ));
           if ($query->num_rows() == 0) {
                $this->db2->insert('tb_award', $arr);
           }
           else{
               $row = $query->row();
                if (isset($row))
                {
                    $arr2=array("updated_at"=>$this->current_date);
                    $arr=(array_merge($arr,$arr2));
                    unset($arr['created_at']);

                    $this->db2->where('id', $row->id);
                    $this->db2->update('tb_award', $arr);

                }
           }

    }
   
    else
    {
        $row = $query->row();

        // var_dump($row);
        if (isset($row))
        {
            $this->db2->where('sale_code', $arr['sale_code']);
            $this->db2->where('type', $arr['type']);
            $this->db2->delete('tb_award');

        }
    }
      
   }

   private function get_mgr_manage_showroom($branch_code,$dealer_code) {

        $score = 0;
        $where = array(
          'branch_code' => $branch_code,
          'dealer_code' => $dealer_code ,
        );
        $query = $this->db2->get_where('tb_mgr_manage_showroom', $where );
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            foreach ($result as $rs) {
                $grade = $rs['grade'] ;
                
                $where = array(
                    "grade"=> $grade ,
                    'set_id'=> $this->set ,
                );
                $query = $this->db->get_where('tb_assessment_rateing', $where );
                if ($query->num_rows() > 0) {
                    $r = $query->row_array();
                    
                    $score +=  $r['score'] ;
                }
            }
        }

        return $score ;

   }

   private function get_mgr_saleman($branch_code,$dealer_code) {

        $score = 0;
        $where = array(
          'branch_code' => $branch_code,
          'dealer_code' => $dealer_code ,
          'resign_date >'=> $this->year . '-4-1' ,   
          'resign_date <'=> $this->year . '-12-31' ,   
        );
        $query = $this->db2->get_where('tb_employee', $where );
        // echo $this->db2->last_query();
       $resign=$query->num_rows();


        $where = " (YEAR(resign_date) = 0 OR resign_date > '{$this->year}-12-31')  ";
        $this->db2->where($where);
        $where = array(
            'branch_code' => $branch_code,
            'dealer_code' => $dealer_code ,
        );
        // $this->db2->where('YEAR(resign_date) = 0', null, false);
        // $this->db2->or_where('resign_date >', $this->year . '-12-31' );
        $query = $this->db2->get_where('tb_employee', $where );
        // echo $this->db2->last_query();
        $active =$query->num_rows();
        $percentage = 0;

        if($active+$resign){
           $percentage =  $resign / ($active+$resign)*100;
        }
        // echo $percentage .' ==> ';
        $where = array(
            'set_id'=> $this->set ,
        );
        $score = 0;
        $this->db->order_by('point', 'asc');
        $query = $this->db->get_where('tb_mgrsaleman_rateing', $where );
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            // var_dump($result); 
            foreach ($result as $rs) {

                $operation = $rs['operation'] ;
                $point1 = $rs['point'] ;
                $point2 = $rs['point2'] ;

                $condition = false ;
                $condition=condition($operation,$percentage,$point1,$point2);
                if($condition)
                {
                    $score  = $rs['score'] ;
                    break;
                }

            }         
        }
        

        return $score ;

   }

    private function update_ssmi_score() {
         $where = array(
        //   'dealer_code' => $dealer_code,
        //   'branch_code' => $branch_code,
          'year' => $this->year,
        );
        // $this->db->limit(12);
        $query = $this->db2->get_where('tb_mgr_ssmi', $where );
        // echo $this->db->last_query();
        $score = 0;
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            foreach ($result as $rs) {
                $ssmi = $rs['avg'] ;
                $ssmi = (int)$ssmi;
                $month = $rs['month'] ;
                $score=$this->call_ssmi_score($ssmi,$month);
                
                $arr = array(
                    "score" => $score ,
                );

                $this->db2->where('id', $rs['id']);
                $this->db2->update('tb_mgr_ssmi', $arr);

            }         
        }

        // echo " update_ssmi_score success " . $query->num_rows() ;

        return $score ;
   }
   private function call_ssmi_score($ssmi,$month) {

         $where = array(
          'month' => $month,
          'set_id' => $this->set,
        );
        // $this->db->limit(12);
        $query = $this->db->get_where('tb_ssmi_rating', $where );
        // echo $this->db->last_query();
        $score = 0;
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            foreach ($result as $rs) {
                $operation = $rs['operation'] ;
                $point1 = $rs['point1'] ;
                $point2 = $rs['point2'] ;

                $condition = false ;
                $condition=condition($operation,$ssmi,$point1,$point2);
                if($condition)
                {
                    $score  = $rs['score'] ;
                    break;
                }

            }         
        }

        return $score ;
   }

   private function get_ssmi_score($dealer_code,$branch_code) {
         $where = array(
          'dealer_code' => $dealer_code,
          'branch_code' => $branch_code,
          'year' => $this->year,
        );
        $this->db2->limit(12);
        $query = $this->db2->get_where('tb_mgr_ssmi', $where );
        //  echo $this->db2->last_query();
        $score = 0;
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            foreach ($result as $rs) {
                $score += $rs['score'] ;
            }         
        }


        return $score ;
   }

   private function get_mgr_priceSurveyScore($branch_code,$dealer_code){
       return 600;
   }

   private function get_mgr_testdrive($branch_code,$dealer_code){

       $where = array(
          'dealer_code' => $dealer_code,
        );
        $this->db2->limit(1);
        $query = $this->db2->get_where('tb_mgr_testdrive', $where );
        //  echo $this->db2->last_query();
        $score = 0;
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            foreach ($result as $rs) {
                $score += $rs['score'] ;
            }         
        }

        return $score ;
   }
   private function get_mgr_activity($branch_code,$dealer_code){

       $where = array(
          'dealer_code' => $dealer_code,
          'branch_code' => $branch_code,
        );
        $this->db2->limit(1);
        $query = $this->db2->get_where('tb_mgr', $where );
        //  echo $this->db2->last_query();
        $score = 0;
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            foreach ($result as $rs) {
                $score += $rs['act_q1'] ;
                $score += $rs['act_q2'] ;
                $score += $rs['act_q3'] ;
                $score += $rs['act_q4'] ;
            }         
        }

        return $score ;
   }

   private function get_mgr_crm($branch_code,$dealer_code){

       $where = array(
          'dealer_code' => $dealer_code,
          'branch_code' => $branch_code,
        );
        $this->db2->limit(1);
        $query = $this->db2->get_where('tb_mgr', $where );
        //  echo $this->db2->last_query();
        $score = 0;
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            foreach ($result as $rs) {
                $score += $rs['crm_q2'] ;
                $score += $rs['crm_q3'] ;
                $score += $rs['crm_q4'] ;
            }         
        }

        return $score ;
   }

   public function get_region_avg($region='01',$month='1'){

       //$regioncode_arr = array("01","02","")


       //1 ดึงรหัสสาขาทั้งหมดภายในภาค
       $where = array(
          'region_code' => $region,
        );
        $query = $this->db2->get_where('tb_dealers', $where );
        //  echo $this->db2->last_query();
        $dealer_code = array();
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            foreach ($result as $rs) {
                $dealer_code[] = $rs['dealer_code'] ;
            }         
        }

        // bug พนักงานลาออกภายในเดือน
        // $where = " (YEAR(resign_date) = 0 OR resign_date > LAST_DAY(1-{$month}-{$this->year}))  ";
        // $this->db2->where($where);
        $this->db2->where_in("ddms", array("01","02","03","04"));
        $this->db2->where_in("dealer_code",$dealer_code );
        $this->db2->where("status",'A');
        $query = $this->db2->get('tb_employee');
        $totleSale = $query->num_rows();
        // echo $this->db2->last_query();

        $Totle = 0;
        $this->db2->where_in("dealer_code",$dealer_code );
        $this->db2->where("MONTH(dl_month)",$month );
        $this->db2->where("YEAR(dl_month)",$this->year );
        $query = $this->db2->get('tb_sales_summary');
        // $this->db2->last_query();
        $totle = $query->num_rows();
        $avg = 0;
        if($Totle){
         $avg = $totle  / $totleSale ;
        }

        return $avg;

   }

   public function get_month_avg($dealer_code,$branch_code){

        // $branch_code
        // region_code
         $where = array(
          'dealer_code' => $dealer_code,
        );
        $query = $this->db2->get_where('tb_dealers', $where );
        $region_code = '';
        //  echo $this->db2->last_query();
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            foreach ($result as $rs) {
                $region_code = $rs['region_code'] ;
            }         
        }

       $region = 0;
       $score = 0 ;
       $i =1;
       for($i =1;$i <=1 ; $i++){
           //ค่าเฉลี่ย
            $avg =$this->get_region_avg($region_code ,$i);

            // $where = " (YEAR(resign_date) = 0 OR resign_date > LAST_DAY(1-{$i}-{$this->year}))  ";
            // $this->db2->where($where);
            $this->db2->where_in("ddms", array("01","02","03","04"));
            $this->db2->where("dealer_code",$dealer_code );
            $this->db2->where("branch_code",$branch_code );
            $this->db2->where("status",'A');
            $query = $this->db2->get('tb_employee'); //พนักงานขาย
            // echo $this->db2->last_query();
            $Totle = $query->num_rows();
            $saleman_code = array();
            if ($query->num_rows() > 0) {
                $result = $query->result_array();
                foreach ($result as $rs) {
                    $saleman_code[] = $rs['salesman_code'] ;
                }         
            }

            
            $this->db2->select("salesman_code");
            $this->db2->where("dealer_code",$dealer_code );
            if($saleman_code){
            $this->db2->where_in("salesman_code",$saleman_code );

            }
            $this->db2->where("MONTH(dl_month)",$i );
            $this->db2->where("YEAR(dl_month)",$this->year );
            $this->db2->group_by("salesman_code");
            $this->db2->having('count(*) >', $avg);  // Produces: HAVING user_id = 45
            $query = $this->db2->get('tb_sales_summary'); //ยอดขาย

            // $totleSale = $query->num_rows();
            // echo $this->db2->last_query();
            $count = 0;
            if ($query->num_rows() > 0) {
                $result = $query->result_array();
                foreach ($result as $rs) {
                    // $count += $rs['salesman_code'] ;
                    $count += 1 ;
                }         
            }

            $percentage = 0;

        //     echo " จำนวนพนักงาน =>" . $Totle . " ||";
        //     echo " พนักงานขายเกิน =>" . $count . " || ";
        //     if($Totle){
        //          echo "ประสิทธิภาพ =>" . $percentage =($count*100 ) / $Totle  ;
        //     }
        //    echo "----<br/><br/>";
           // tb_sales_summary

           if($percentage==100)
           {
               $score += 200;
           }
           else if($percentage >=90  && $percentage < 99.99)
           {
                $score += 100;
           }
           else if($percentage >=80  && $percentage < 89.99)
           {
                $score += 50;
           }
           else if($percentage < 80)
           {
                $score = 0;
           }


       }

    //    exit();

       return $score ;

   }
	
}
