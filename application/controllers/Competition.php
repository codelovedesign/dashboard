<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Competition extends MY_Controller {

     var $page_level_css = array(
        "assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css" ,
        "assets/plugins/DataTables/extensions/Buttons/css/buttons.bootstrap.min.css" ,
        "assets/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css",
        "assets/plugins/bootstrap-datepicker/css/datepicker.css",
        "assets/plugins/bootstrap-datepicker/css/datepicker3.css",
    );

	var $page_level_js = array(
            'assets/plugins/DataTables/media/js/jquery.dataTables.js',
            'assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js' ,
            "assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js",
            "assets/plugins/DataTables/extensions/Buttons/js/dataTables.buttons.min.js",
            "assets/plugins/DataTables/extensions/Buttons/js/buttons.bootstrap.min.js",
            "assets/plugins/DataTables/extensions/Buttons/js/buttons.flash.min.js",
            "assets/plugins/DataTables/extensions/Buttons/js/jszip.min.js",
            "assets/plugins/DataTables/extensions/Buttons/js/pdfmake.min.js",
            "assets/plugins/DataTables/extensions/Buttons/js/vfs_fonts.min.js",
            "assets/plugins/DataTables/extensions/Buttons/js/buttons.html5.min.js",
            "assets/plugins/DataTables/extensions/Buttons/js/buttons.print.min.js",
            "assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js",
            "assets/plugins/bootstrap-daterangepicker/moment.js",
            "assets/plugins/bootstrap-eonasdan-datetimepicker/build/js/bootstrap-datetimepicker.min.js",
            "assets/js/table-manage-buttons.demo.js",
            "assets/js/form-schedule.js",
		);

	public function __construct(){
		parent::__construct();

		$this->load->model('award_model');
		$this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');

	}

    public function page($year = null){
        $search_arr = array();

        $this->middle = 'competition/index';
        $this->title = 'ผลการแข่งขัน';
        $this->js = array('FormPlugins.init();', 'TableManageButtons.init();');
        // $this->data['search'] = $search_arr ;
        // if($id){
        //     $this->data['current'] = $this->schedule_model->get_schedulebyId($id);
        // }
       
        $this->data['breadcrumb'] = array(
                array('name'=>'ผลการแข่งขัน' , 'link' => BASE_URL('competition') , 'active' => false ) ,
                array('name'=>'ผลการแข่งขันประจำปี ' . $year , 'link' => BASE_URL("competition") , 'active' => true )
            ); 

        $this->view();
    }

	public function index($year=null,$type=0)
	{
       $search_arr = array();
        //  $productID =  $this->uri->segment(3);
        $awd_year = $this->award_model->get_award_year();

        // print_r($awd_year);
        // exit();
        if(count($awd_year)==0){
            redirect('/competition/noresult', 'refresh');
        }


        $this->middle = 'competition/index';
        $this->title = 'ผลการแข่งขันประจำปี ' .$year;
        $this->js = array('TableManageButtons.init();');



        $this->data['award_type'] = $this->award_model->get_award_type();
        $this->data['years'] = $this->award_model->get_award_year();
        $this->data['curent_year'] = $year ;
        $where = array(
            "type"=>$type,
            // "order !=" => "0"
        );
        $this->data['type'] = $type ;
        $this->data['get_award'] = $this->award_model->get_award($where , $year);

        
        $this->data['breadcrumb'] = array(
                array('name'=>'ผลการแข่งขัน' , 'link' => BASE_URL('competition') , 'active' => false ) ,
                array('name'=>'ผลการแข่งขันประจำปี ' . $year , 'link' => BASE_URL("competition") , 'active' => true )
            ); 

        $this->view();
	}

    public function noresult() {
        
        $this->middle = 'global/alert/alert-result';
        $this->title = 'ผลการแข่งขันประจำปี';
        $this->data['result'] = "ยังไม่มีผลการแข่ง";
        $this->view();
    }
    


}
