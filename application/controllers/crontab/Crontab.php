<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ini_set('max_execution_time', 30000); //300 seconds = 5 minutes30
ini_set("memory_limit","2024M");//128
class Crontab extends MY_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->dbforge();
	}

	public function index()
	{
		
	}


	public function createTable(){
		if ($this->dbforge->create_database('my_db'))
		{
				echo 'Database created!';
		}

		$this->load->database('my_db');


		// if ($this->dbforge->drop_database('my_db'))
		// {
		// 		echo 'Database deleted!';
		// }
		// $this->dbforge->create_table('table_name');

	}

	

	
	function mb_detect_encoding ($string, $enc=null, $ret=null) { 
		
		static $enclist = array( 
			'UTF-8', 'ASCII', 
			'ISO-8859-1', 'ISO-8859-2', 'ISO-8859-3', 'ISO-8859-4', 'ISO-8859-5', 
			'ISO-8859-6', 'ISO-8859-7', 'ISO-8859-8', 'ISO-8859-9', 'ISO-8859-10', 
			'ISO-8859-13', 'ISO-8859-14', 'ISO-8859-15', 'ISO-8859-16', 
			'Windows-1251', 'Windows-1252', 'Windows-1254', 
			);
		
		$result = false; 
		
		foreach ($enclist as $item) { 
			$sample = iconv($item, $item, $string); 
			if (md5($sample) == md5($string)) { 
				if ($ret === NULL) { $result = $item; } else { $result = true; } 
				break; 
			}
		}
		
	return $result; 
	} 

	public function dateExcel2date($data){
		$result_data = '';
		// var_dump($data['work_startdate']);
		if($data){
			$result_data =  date("Y-m-d", PHPExcel_Shared_Date::ExcelToPHP($data)); 
		}

		return $result_data;
	}

	public function iso8859_11toUTF8($string) {
	
		if (preg_match("[\241-\377]", $string)) 
		return $string;

		$iso8859_11 = array(
			"\xa1" => "\xe0\xb8\x81",
			"\xa2" => "\xe0\xb8\x82",
			"\xa3" => "\xe0\xb8\x83",
			"\xa4" => "\xe0\xb8\x84",
			"\xa5" => "\xe0\xb8\x85",
			"\xa6" => "\xe0\xb8\x86",
			"\xa7" => "\xe0\xb8\x87",
			"\xa8" => "\xe0\xb8\x88",
			"\xa9" => "\xe0\xb8\x89",
			"\xaa" => "\xe0\xb8\x8a",
			"\xab" => "\xe0\xb8\x8b",
			"\xac" => "\xe0\xb8\x8c",
			"\xad" => "\xe0\xb8\x8d",
			"\xae" => "\xe0\xb8\x8e",
			"\xaf" => "\xe0\xb8\x8f",
			"\xb0" => "\xe0\xb8\x90",
			"\xb1" => "\xe0\xb8\x91",
			"\xb2" => "\xe0\xb8\x92",
			"\xb3" => "\xe0\xb8\x93",
			"\xb4" => "\xe0\xb8\x94",
			"\xb5" => "\xe0\xb8\x95",
			"\xb6" => "\xe0\xb8\x96",
			"\xb7" => "\xe0\xb8\x97",
			"\xb8" => "\xe0\xb8\x98",
			"\xb9" => "\xe0\xb8\x99",
			"\xba" => "\xe0\xb8\x9a",
			"\xbb" => "\xe0\xb8\x9b",
			"\xbc" => "\xe0\xb8\x9c",
			"\xbd" => "\xe0\xb8\x9d",
			"\xbe" => "\xe0\xb8\x9e",
			"\xbf" => "\xe0\xb8\x9f",
			"\xc0" => "\xe0\xb8\xa0",
			"\xc1" => "\xe0\xb8\xa1",
			"\xc2" => "\xe0\xb8\xa2",
			"\xc3" => "\xe0\xb8\xa3",
			"\xc4" => "\xe0\xb8\xa4",
			"\xc5" => "\xe0\xb8\xa5",
			"\xc6" => "\xe0\xb8\xa6",
			"\xc7" => "\xe0\xb8\xa7",
			"\xc8" => "\xe0\xb8\xa8",
			"\xc9" => "\xe0\xb8\xa9",
			"\xca" => "\xe0\xb8\xaa",
			"\xcb" => "\xe0\xb8\xab",
			"\xcc" => "\xe0\xb8\xac",
			"\xcd" => "\xe0\xb8\xad",
			"\xce" => "\xe0\xb8\xae",
			"\xcf" => "\xe0\xb8\xaf",
			"\xd0" => "\xe0\xb8\xb0",
			"\xd1" => "\xe0\xb8\xb1",
			"\xd2" => "\xe0\xb8\xb2",
			"\xd3" => "\xe0\xb8\xb3",
			"\xd4" => "\xe0\xb8\xb4",
			"\xd5" => "\xe0\xb8\xb5",
			"\xd6" => "\xe0\xb8\xb6",
			"\xd7" => "\xe0\xb8\xb7",
			"\xd8" => "\xe0\xb8\xb8",
			"\xd9" => "\xe0\xb8\xb9",
			"\xda" => "\xe0\xb8\xba",
			"\xdf" => "\xe0\xb8\xbf",
			"\xe0" => "\xe0\xb9\x80",
			"\xe1" => "\xe0\xb9\x81",
			"\xe2" => "\xe0\xb9\x82",
			"\xe3" => "\xe0\xb9\x83",
			"\xe4" => "\xe0\xb9\x84",
			"\xe5" => "\xe0\xb9\x85",
			"\xe6" => "\xe0\xb9\x86",
			"\xe7" => "\xe0\xb9\x87",
			"\xe8" => "\xe0\xb9\x88",
			"\xe9" => "\xe0\xb9\x89",
			"\xea" => "\xe0\xb9\x8a",
			"\xeb" => "\xe0\xb9\x8b",
			"\xec" => "\xe0\xb9\x8c",
			"\xed" => "\xe0\xb9\x8d",
			"\xee" => "\xe0\xb9\x8e",
			"\xef" => "\xe0\xb9\x8f",
			"\xf0" => "\xe0\xb9\x90",
			"\xf1" => "\xe0\xb9\x91",
			"\xf2" => "\xe0\xb9\x92",
			"\xf3" => "\xe0\xb9\x93",
			"\xf4" => "\xe0\xb9\x94",
			"\xf5" => "\xe0\xb9\x95",
			"\xf6" => "\xe0\xb9\x96",
			"\xf7" => "\xe0\xb9\x97",
			"\xf8" => "\xe0\xb9\x98",
			"\xf9" => "\xe0\xb9\x99",
			"\xfa" => "\xe0\xb9\x9a",
			"\xfb" => "\xe0\xb9\x9b"
			);

		$string=strtr($string,$iso8859_11);
		return $string;
	}

	public function get_data_excel($inputFileName){

		// $inputFileName = "media/Active SM.xlsx";  
		$inputFileType = PHPExcel_IOFactory::identify($inputFileName);  
		$objReader = PHPExcel_IOFactory::createReader($inputFileType);  
		$objReader->setReadDataOnly(true);  
		$objPHPExcel = $objReader->load($inputFileName);  

		$objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
		$highestRow = $objWorksheet->getHighestRow();
		$highestColumn = $objWorksheet->getHighestColumn();

		$headingsArray = $objWorksheet->rangeToArray('A1:'.$highestColumn.'1',null, true, true, true);
		$headingsArray = $headingsArray[1];

		$r = -1;
		$namedDataArray = array();
		for ($row = 2; $row <= $highestRow; ++$row) {
			$dataRow = $objWorksheet->rangeToArray('A'.$row.':'.$highestColumn.$row,null, true, true, true);
		    //  var_dump($dataRow);

			if ((isset($dataRow[$row]['A'])) && ($dataRow[$row]['A'] > '')) {
				++$r;
				foreach($headingsArray as $columnKey => $columnHeading) {
					$namedDataArray[$r][$columnHeading] = $dataRow[$row][$columnKey];
				}
			}
		}

		return  $namedDataArray ;

	}

	public function rs_data(){

		$inputFileName = "media/RS_Data.xlsx";  
		$result = $this->get_data_excel($inputFileName);
		$i = 0;
		foreach ($result as $data) {
				$i++;
				
			    $day = date("Y-m-d H:i:s");
			    

				
				$data_array  = array(
					'dealer_code'=> $data['Dealer Code'],
					'series'=> $data['Series'],
					'vin_no'=> $data['VIN No'] ,
					'rs_month'=> $this->dateExcel2date($data['RSMonth']) ,
					'dl_month'=> $this->dateExcel2date($data['DLMonth']) ,
					'salesman_code'=> $data['Salesman Code'],
					'fleet_id'=> $data['Fleet ID'],
					'sorce'=> $data['Sorce'],
					'original_series'=> $data['OriginalSeries'],
					'region_id'=> $data['Region'],
					'created_date'=> $day ,
				);

				$query = $this->db
					->where('vin_no',$data['VIN No'])
					->get('tb_sales');

				if ($query->num_rows() > 0) {
					$this->db->where('vin_no', $data['VIN No']);
					$this->db->update('tb_sales', $data_array);
				}
				else
				{
					$this->db->insert('tb_sales', $data_array);
					$id = $this->db->insert_id();
				}

				
		}

	}

	public function interview_Data(){

		$inputFileName = "media/Interview.xlsx";  
		$result = $this->get_data_excel($inputFileName);
		$i = 0;
		foreach ($result as $data) {
				$i++;

				// echo "<pre>";
				// var_dump($data['Br_Code']);
				if(empty($data['Br_Code'])){
					$data['Br_Code'] = '';
				}

				if(empty($data['Rating_Answer'])){
					$data['Rating_Answer'] = '';
				}
				
			    $day = date("Y-m-d H:i:s");
			    $codeIndex ="";
			    $year ="";
			    if(isset($data['Quarter'])){
					$code_arr= explode("-",$data['Quarter']);
					$codeIndex = (int)$code_arr[0];
					$year = "20" .$code_arr[1];
			    }
				
				$data_array  = array(
					'quarter'=> $codeIndex ,
					'year'=> $year ,
					'dealer_code'=> $data['Dlr_Code'],
					'branch_code'=> $data['Br_Code'],
					'rating_answer'=> $data['Rating_Answer'],
					'created_at'=> $day ,
				);

				$query = $this->db
					->where('dealer_code',$data['Dlr_Code'])
					->where('branch_code',$data['Br_Code'])
					->where('quarter',$codeIndex )
					->where('year',$year )
					->get('tb_interview');

				if ($query->num_rows() > 0) {
					$this->db
					->where('dealer_code',$data['Dlr_Code'])
					->where('branch_code',$data['Br_Code'])
					->where('quarter',$codeIndex )
					->where('year',$year )
					->update('tb_interview', $data_array);
				}
				else
				{
					$this->db->insert('tb_interview', $data_array);
					$id = $this->db->insert_id();
				}
		}
	}

	public function ssmi_Data(){

		$inputFileName = "media/SSMI_Data.xlsx";  
		$result = $this->get_data_excel($inputFileName);
		$i = 0;
		foreach ($result as $data) {
				$i++;
				
			    $day = date("Y-m-d H:i:s");
			    $codeIndex ="";
			    $year ="";
			    if(isset($data['Month'])){
					$code_arr= explode("-",$data['Month']);
					$codeIndex = (int)$code_arr[0];
					$year = "20" .$code_arr[1]-43;
			    }
				
				$data_array  = array(
					'month'=> $codeIndex ,
					'year'=> $year ,
					'dealer_code'=> $data['Dlr_Code'],
					'branch_code'=> $data['Br'],
					'ssmi'=> $data['SSMI'],
					'remerk'=> $data['Remark'],
					// 'score'=> $data['SSMI_Score'],
					'created_at'=> $day ,
				);

				$query = $this->db
					->where('dealer_code',$data['Dlr_Code'])
					->where('branch_code',$data['Br'])
					->where('month',$codeIndex )
					->where('year',$year )
					->get('tb_ssmi');

				if ($query->num_rows() > 0) {
					$this->db
					->where('dealer_code',$data['Dlr_Code'])
					->where('branch_code',$data['Br'])
					->where('month',$codeIndex )
					->where('year',$year )
					->update('tb_ssmi', $data_array);
				}
				else
				{
					$this->db->insert('tb_ssmi', $data_array);
					$id = $this->db->insert_id();
				}
		}
	}

	public function dealers_Data(){

		$inputFileName = "media/Dlr_Group.xlsx";  
		$result = $this->get_data_excel($inputFileName);
		$i = 0;
		foreach ($result as $data) {
				$i++;
				
			    $day = date("Y-m-d H:i:s");
			   
			   if(!isset($data['Code'])) {
				   $data['Code']= '';
			   }
				
				$data_array  = array(
					'dealer_code'=> $data['Dlr_code'],
					'group'=> $data['Group'],
					'dealer_name'=> $data['Dlr_name'],
					'region_code'=> $data['Region'],
					'group_id'=> $data['Code'],
					'created_at'=> $day ,
				);

				$query = $this->db
					->where('dealer_code',$data['Dlr_code'])
					->get('tb_dealers');

				if ($query->num_rows() > 0) {
					$this->db
					->where('dealer_code',$data['Dlr_code'])
					->update('tb_dealers', $data_array);
				}
				else
				{
					$this->db->insert('tb_dealers', $data_array);
					$id = $this->db->insert_id();
				}
		}
	}

	public function ExamData(){

		$inputFileName = "media/Exam_Data.xlsx";  
		$result = $this->get_data_excel($inputFileName);
		$i = 0;
		foreach ($result as $data) {
				$i++;
				
			    $day = date("Y-m-d H:i:s");
			    $codeIndex ="";
			    $year ="";
			    if(isset($data['Exam_Code'])){
					$code_arr= explode("-",$data['Exam_Code']);
					$codeIndex = (int)$code_arr[0];
					$year = "20" .$code_arr[1];
			    }
				
				$data_array  = array(
					'salesman_code'=> $data['SM_Code'],
					'exam_name'=> $data['E_Name'],
					'score'=> $data['Score'],
					'exam_code'=> $codeIndex ,
					'exam_year'=> $year ,
					'exam_score'=> $data['Score'],
					'created_at'=> $day ,
				);

				$query = $this->db
					->where('salesman_code',$data['SM_Code'])
					->where('exam_code',$codeIndex )
					->where('exam_year',$year )
					->get('tb_exam');

				if ($query->num_rows() > 0) {
					$this->db->where('salesman_code', $data['SM_Code']);
					$this->db->where('exam_code',$codeIndex ) ;
					$this->db->where('exam_year', $year ) ;
					$this->db->update('tb_exam', $data_array);
				}
				else
				{
					$this->db->insert('tb_exam', $data_array);
					$id = $this->db->insert_id();
				}

				
		}

	}
	public function HistoryAwardData(){

		$inputFileName = "media/Award_History.xlsx";  
		$result = $this->get_data_excel($inputFileName);
		$i = 0;
		foreach ($result as $data) {
				$i++;
				
			    $day = date("Y-m-d H:i:s");
			    $codeIndex ="";
			    $year ="";
			    if(isset($data['year'])){
					$year = ( $data['year'] - 543 );
			    }
				
				$data_array  = array(
					'sale_code'=> $data['SalesCode'],
					'award_code'=> $data['Award_Code'],
					'score'=> $data['Score'],
					'year'=> $year ,
					'order'=> $data['order'],
					'created_at'=> $day ,
				);

				$query = $this->db
					->where('sale_code',$data['SalesCode'])
					->where('award_code',$data['Award_Code'])
					->where('year',$year )
					->get('tb_history');

				if ($query->num_rows() > 0) {
					$this->db->where('sale_code',$data['SalesCode'])
					->where('award_code',$data['Award_Code'])
					->where('year',$year )
					->update('tb_history', $data_array);
				}
				else
				{
					$this->db->insert('tb_history', $data_array);
					$id = $this->db->insert_id();
				}

				
		}

	}

	public function LicenseTraining_Data(){

		$inputFileName = "media/License_Training.xlsx";  
		$result = $this->get_data_excel($inputFileName);
		$i = 0;
		foreach ($result as $data) {
				$i++;
				
			    $day = date("Y-m-d H:i:s");
			    $codeIndex ="";
			    $year ="";
			    if(isset($data['Cl_Code'])){
					$code = substr($data['Cl_Code'],0,3) ;
			    }
				
				$data_array  = array(
					'sale_code'=> $data['S_Code'],
					'sale_id'=> $data['S_ID'],
					'cl_code'=> $data['Cl_Code'],
					'code'=> $code,
					'attendance'=> $data['Attendance'] ,
					'result'=> $data['TR_Result'] ,
					'created_at'=> $day ,
				);

				$query = $this->db
					->where('sale_code',$data['S_Code'])
					->where('cl_code',$data['Cl_Code'])
					->get('tb_license');

				if ($query->num_rows() > 0) {
					$this->db->where('sale_code',$data['S_Code'])
					->where('cl_code',$data['Cl_Code'])
					->update('tb_license', $data_array);
				}
				else
				{
					$this->db->insert('tb_license', $data_array);
					$id = $this->db->insert_id();
				}

				
		}

	}

	public function employee_data()
	{
		$inputFileName = "media/Active_SM.xlsx";  
		$inputFileType = PHPExcel_IOFactory::identify($inputFileName);  
		$objReader = PHPExcel_IOFactory::createReader($inputFileType);  
		$objReader->setReadDataOnly(true);  
		$objPHPExcel = $objReader->load($inputFileName);  

		$objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
		$highestRow = $objWorksheet->getHighestRow();
		$highestColumn = $objWorksheet->getHighestColumn();

		$headingsArray = $objWorksheet->rangeToArray('A1:'.$highestColumn.'1',null, true, true, true);
		$headingsArray = $headingsArray[1];

		$r = -1;
		$namedDataArray = array();
		for ($row = 2; $row <= $highestRow; ++$row) {
			$dataRow = $objWorksheet->rangeToArray('A'.$row.':'.$highestColumn.$row,null, true, true, true);
		    //  var_dump($dataRow);

			if ((isset($dataRow[$row]['A'])) && ($dataRow[$row]['A'] > '')) {
				++$r;
				foreach($headingsArray as $columnKey => $columnHeading) {
					$namedDataArray[$r][$columnHeading] = $dataRow[$row][$columnKey];
				}
			}
		}

		
		$i = 0;
		foreach ($namedDataArray as $data) {
				$i++;
				
			    $day = date("Y-m-d H:i:s");

				
				$data_array  = array(
					'dealer_code'=> $data['Dealer Code'],
					'branch_code'=> $data['Branch Code'],
					'salesman_code'=> $data['Salesman Code'],
					'firstname'=> $data['Salesman Name'],
					'lastname'=> $data['Salesman Surname'],
					'salesman_id'=> $data['Salesman ID'],
					'sex'=> $data['Salesman sex'],
					'married'=> $data['Salesman Status(Married)'],
					'status'=> $data['Salesman status'],
					'birthday'=> $this->dateExcel2date($data['Salesman Birthday']) ,
					'start_date'=> $this->dateExcel2date($data['Start date']) ,
					'updated_date'=> $this->dateExcel2date($data['Update Date']) ,
					'ddms'=> $data['Normal Sales Code'],
					'position'=> $data['Normal Sales Position'],
					'fleetsales_code'=> $data['Fleet Sales Code'],
					'fleetsales_position'=> 'Fleet Sales Position',
					'educational'=> $data['Educational Insituation'],
					'religion'=> $data['Religion'],
					'approval_date'=> $this->dateExcel2date($data['Salesman Approval Date']) ,
					'resign_date'=> $this->dateExcel2date($data['Resign Date']) ,
					'created_date'=> $day ,
				);

				// echo "<pre>";
				// var_dump($data_array);
				// exit();

				$query = $this->db
					->select("*", false)
					->where('salesman_code',$data['Salesman Code'])
					->get('tb_employee');
				if ($query->num_rows() > 0) {

					$this->db->where('salesman_code', $data['Salesman Code']);
					$this->db->update('tb_employee', $data_array);
				}
				else
				{
					$this->db->insert('tb_employee', $data_array);
					$id = $this->db->insert_id();
				}

				
		}


		echo "SUCCESS";
	
	}

	public function run()
	{
		$in_charset = 'UTF-8';   // == 'windows-874'
		$out_charset = 'UTF-8';

		$opts = array(
		'http'=>array(
			'method'=>"GET",
			'header'=> implode("\r\n", array(
						'Content-type: text/plain; charset=' . $in_charset
						))
		)
		);

		// $context = stream_context_create($opts);
		// $contents = file_get_contents('./media/Current_SM.txt',false, $context);
		// $recrod = explode("\n",$contents);

		$key =0;

		// echo "<pre>";
		// foreach($recrod as $key => $seller){
		$lines=array();
		$f=fopen('./media/Current_SM.txt',"r");
		while(!feof($f)){
			 $line=fgets($f, 65535);
			 $line = $this->iso8859_11toUTF8($line);
			
			$data = explode(",",$line);
			if(count($data)==20 && $key > 0)
			{

			
			// echo $this->mb_detect_encoding($data[3]);			
			// echo $this->iso8859_11toUTF8($data[3]);
			//  echo  iconv('iso-8859-1', 'utf-8', $data[3]) ;

			

	
			$day = date("Y-m-d H:i:s");
			// echo $data[9] . "<br/>" ;
			// echo date('Y-m-d', strtotime($data[9]) ) ;

			$arr  = array(
				'dealer_code'=> $data[0],
				'branch_code'=> $data[1],
				'salesman_code'=> $data[2],
				'firstname'=> $data[3],
				'lastname'=> $data[4],
				'salesman_id'=> $data[5],
				'sex'=> $data[6],
				'married'=> $data[7],
				'status'=> $data[8],
				'birthday'=> date('Y-m-d', strtotime($data[9]) ),
				'start_date'=> date('Y-m-d', strtotime($data[10]) ),
				'updated_date'=> date('Y-m-d', strtotime($data[11]) ),
				'ddms'=> $data[12],
				'position'=> $data[13],
				'fleetsales_code'=> date('Y-m-d', strtotime($data[14])),
				'fleetsales_position'=> $data[15],
				'educational'=> $data[16],
				'religion'=> $data[17],
				'approval_date'=> date('Y-m-d', strtotime($data[18]) ),
				'resign_date'=> date('Y-m-d', strtotime($data[19]) ),
				'created_date'=> $day ,
			);

			


				$this->db->where('salesman_code =', $data[2] );
				$query = $this->db->get('tb_employee');
				if($query->num_rows())
				{
					$this->db->where('salesman_code', $data[2]);
					$this->db->update('tb_employee', $arr);
					// echo "update {$key}";
				}
				else
				{
					$this->db->insert('tb_employee', $arr);
					$lastid = $this->db->insert_id();
				}


			}
			else
			{
		 	    // echo "<pre>";

				 echo $line . "<br/>";
				 echo "---------------------------------------------------------------------";
				 echo count($data) . "<br/>";

				// var_dump($arr);

			}

			// if($key > 100 )
			// exit();	

			$key ++;
		}
		fclose($f);
		

		echo $key;
		// echo 'Plain    : ', iconv("UTF-8", "ISO-8859-1", $fileText), PHP_EOL;

	}

	public function delivery(){
		

		$key =0;

		// echo "<pre>";
		// foreach($recrod as $key => $seller){
		$lines=array();
		$f=fopen('./media/RS1503.txt',"r");
		while(!feof($f)){
			echo  $line=fgets($f, 65535);
			//  $line = $this->iso8859_11toUTF8($line);
			
			$data = explode(",",$line);

			echo "<pre/>";
			var_dump($data);

			if($key > 1 )
			exit();	
		}
		fclose($f);
		echo $key;
	}

	function __destruct() {
       echo "success";
     }


}