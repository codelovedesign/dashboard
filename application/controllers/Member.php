<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member extends MY_Controller {

	// var $page_level_js = array(
	// 	'assets/plugins/DataTables/media/js/jquery.dataTables.js',
	// 	'assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js',
	// 	'assets/plugins/DataTables/extensions/AutoFill/js/dataTables.autoFill.min.js',
	// 	'assets/plugins/DataTables/extensions/AutoFill/js/autoFill.bootstrap.min.js',
	// 	'assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js',
	// 	'assets/js/table-manage-autofill.demo.js',
	// );

	var $page_level_js = array(
			'assets/js/employee.js',
			'assets/plugins/parsley/dist/parsley.js'
		);

	public function __construct(){
		parent::__construct();

		$this->load->model('employee_model');
		$this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');

	}

	public function index()
	{
		redirect('/member/memberList/', 'refresh');
	}

	public function employee($id = '')
	{

		$this->title = 'ເພີ່ມສະມາຊິກ';
		$this->middle = 'member/employee_profile';
		$this->js = array('employee.init();');
		$this->data['agent'] = $this->employee_model->get_employee_type(1);

		$this->data['employee'] = null ;
		if($id)
		{
			$this->data['employee'] = $this->employee_model->get_employee_id($id); 
			$this->title = 'แก้ไขพนักงาน';

			$this->data['breadcrumb'] = array(
				array('name'=>'ຈັດການສະມາຊິກ' , 'link' => BASE_URL('member') , 'active' => false ) ,
				array('name'=>'แก้ไขพนักงาน' , 'link' => BASE_URL($this->middle) , 'active' => true )
			); 

		}
		else
		{
			$this->data['breadcrumb'] = array(
				array('name'=>'ຈັດການສະມາຊິກ' , 'link' => BASE_URL('member') , 'active' => false ) ,
				array('name'=>'ເພີ່ມສະມາຊິກ' , 'link' => BASE_URL($this->middle) , 'active' => true )
			); 
		}

		
		$this->view();
	}

   public function upload(){

	   echo "xxxx";

   }

	public function save_employee()
	{

		$date = date("Y-m-d");

		$id = '';
		$user = '';
		$password = '';
		
		$title = $_POST['title'];
		$type = $_POST['type'];
		$firstname = $_POST['firstname'];
		$lastName = $_POST['lastname'];
		$parent = $_POST['parent'];
		if(isset($_POST['id']))
		{
			$id = $_POST['id'];
		}


		$member_arr  = array(
				//  "id"=> $id ,
				// 'user' => $user,
				// 'password' => $password , 
				'title' => "$title" ,
				'firstname' => $firstname ,
				'lastname' => $lastName ,
				'type' => $type	,
				'parent_id'=> $parent ,
				'careted_at' => $date ,
				'updated_at' => $date
			);


		if($type==1)
		{
			$user= $_POST['user'];
			$password = $_POST['password'];

			$arr  = array(
				'user' => $user,
			);

			if(!empty($password))
			{
				$arr['password'] = md5($password) ;
			}

			$member_arr = array_merge($member_arr, $arr);

		}

		if(isset($id))
		{
			$Last_id = $this->employee_model->update_employee($member_arr,$id); 
		}
		else
		{
			$Last_id = $this->employee_model->set_employee($member_arr); 
			
		}

		redirect('/member/employee/'. $Last_id , 'refresh');
		// return false;    

	}
    public function memberList()
	{
		
		$search_arr = array();

		if(isset($_POST['code'])){
			$search_arr['id'] = $_POST['code'];
		}

		if(isset($_POST['type'])){
			$search_arr['type'] = $_POST['type'];
		}

		$this->middle = 'member/memberList';
		$this->title = 'ລາຍຊື່ສະມາຊິກ';
		$this->js = array('TableManageAutofill.init();');
		$this->data['search'] = $search_arr ;
		$this->data['result'] = $this->employee_model->get_employee($search_arr);
		$this->data['breadcrumb'] = array(
				array('name'=>'ຈັດການສະມາຊິກ' , 'link' => BASE_URL('member') , 'active' => false ) ,
				array('name'=>'ລາຍຊື່ສະມາຊິກ' , 'link' => BASE_URL($this->middle) , 'active' => true )
			  ); 

		$this->view();
	}



}
