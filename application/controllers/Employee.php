<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Employee extends MY_Controller {

	var $page_level_css = array(
        "assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css" ,
        "assets/plugins/DataTables/extensions/Buttons/css/buttons.bootstrap.min.css" ,
        "assets/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css",
        "assets/plugins/bootstrap-datepicker/css/datepicker.css",
        "assets/plugins/bootstrap-datepicker/css/datepicker3.css",
    );

	var $page_level_js = array(
            'assets/plugins/DataTables/media/js/jquery.dataTables.js',
            'assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js' ,
            "assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js",
            "assets/plugins/DataTables/extensions/Buttons/js/dataTables.buttons.min.js",
            "assets/plugins/DataTables/extensions/Buttons/js/buttons.bootstrap.min.js",
            "assets/plugins/DataTables/extensions/Buttons/js/buttons.flash.min.js",
            "assets/plugins/DataTables/extensions/Buttons/js/jszip.min.js",
            "assets/plugins/DataTables/extensions/Buttons/js/pdfmake.min.js",
            "assets/plugins/DataTables/extensions/Buttons/js/vfs_fonts.min.js",
            "assets/plugins/DataTables/extensions/Buttons/js/buttons.html5.min.js",
            "assets/plugins/DataTables/extensions/Buttons/js/buttons.print.min.js",
            "assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js",
            "assets/plugins/bootstrap-daterangepicker/moment.js",
            "assets/plugins/bootstrap-eonasdan-datetimepicker/build/js/bootstrap-datetimepicker.min.js",
            "assets/js/table-manage-buttons.demo.js",
            "assets/js/form-schedule.js",
		);

	public function __construct(){
		parent::__construct();
		
		if (!$this->is_logged_in())
        {
			redirect('login', 'refresh');
        }

		// $this->load->model('schedule_model');
		$this->load->model('lotto_model');
		$this->load->model('employee_model');
	}

	public function index()
	{
		
		$this->middle = 'employee/index';
		$this->title = 'ฐานข้อมูลพนักงาน';
		// $this->data['count_employee'] = $this->employee_model->get_employee_typeCount(1);
		// $this->data['count_seller'] = $this->employee_model->get_employee_typeCount(2);
		// $this->data['schedule'] = $schedule_last;
		// $this->data['total_summary'] = $this->lotto_model->total_sale_summary($schedule_last->id);

		$this->data['breadcrumb'] = array(
			array('name'=>'การปรับปรุงฐานข้อมูล' , 'link' => BASE_URL('competition') , 'active' => false ) ,
			array('name'=>'ฐานข้อมูลพนักงาน' , 'link' => BASE_URL("competition") , 'active' => true )
		); 
        $this->js = array('FormPlugins.init();', 'TableManageButtons.init();');
		$this->view();
	}

     public function upload(){

        $this->middle = 'employee/upload';
        $this->title = 'อัพโหลดข้อมูลพนักงาน';


        $this->data['breadcrumb'] = array(
            array('name'=>'การปรับปรุงฐานข้อมูล' , 'link' => BASE_URL('competition') , 'active' => false ) ,
            array('name'=>'อัพโหลดข้อมูลพนักงาน' , 'link' => BASE_URL("employee/upload") , 'active' => true )
        ); 
        $this->js = array('FormPlugins.init();');
        $this->view();
   }
}
