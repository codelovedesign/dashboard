<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Condition extends MY_Controller {

	var $page_level_css = array(
		"assets/plugins/gritter/css/jquery.gritter.css"
	);

	var $page_level_js = array(
		'assets/plugins/parsley/dist/parsley.js',
		"assets/plugins/gritter/js/jquery.gritter.js",
		'assets/js/condition.js',
	);
	
	public function __construct(){
		parent::__construct();
		
		if (!$this->is_logged_in())
        {
			redirect('login', 'refresh');
        }

		// $this->load->model('schedule_model');
		$this->load->model('dealers_model');
		$this->load->model('set_model');
		$this->load->model('employee_model');
		$this->load->model('award_model');
		$this->load->model('condition_model');
	}

	public function index($param=null)
	{
		
		$this->middle = 'condition/condition';
		$this->title = 'สร้างเงื่อนไข';

		$this->data['levels']= $this->employee_model->get_employee_level();
		$this->data['groups']= $this->dealers_model->get_group();

		$this->js = array('Condition.init();');
		$this->view();
	}
	public function edit($id){

		$this->middle = 'condition/condition';
		$this->title = 'แก้ไขเงื่อนไข';

		$this->data['levels']= $this->employee_model->get_employee_level();
		$this->data['groups']= $this->dealers_model->get_group();

		$this->js = array('Condition.init();');
		$this->view();

	}

	public function get_json_award($level){
		 $where = array(
			 "employee_level_id" => $level
		 );
		 $json=$this->award_model->get_award_type($where);
		 echo json_encode($json);
	}

	public function savedata(){
		$err= array();
		$element = array();
		$condition_arr = array();

		
		$knowledge_score=$this->input->post("knowledge_score");
		$condition_arr['knowledge_score'] = $knowledge_score ;

		$performance_score = $this->input->post("performance_score");
		$condition_arr['performance_score'] = $performance_score ;

		$ssmi_score  = $this->input->post("ssmi_score");
		$condition_arr['ssmi_score'] = $ssmi_score ;

		//month score
		$max_month  = $this->input->post("max_month");
		$condition_arr['max_month'] = $max_month ;

		$unit_start  = $this->input->post("unit_start");
		$condition_arr['unit_start'] = $unit_start ;

		$unit_end  = $this->input->post("unit_end");
		$condition_arr['unit_end'] = $unit_end ;

		$element_arr= array();
		$unit_arr= array();

		$month_score = array();
		$group_arr = $this->dealers_model->get_group();
		if(is_array($group_arr))
		{
			foreach($group_arr as $key => $group){
				$inputname = "month_score_". $group->initial ; 
				$sum = $_POST[$inputname];
				$sum_arr[]=max($sum);
				// $month_score[]=$sum ;
				$element_arr[] = $inputname ;
			}
		}
		
		if(is_array($sum_arr))
		{
			foreach($sum_arr as $key => $sum ){
				if($max_month != $sum*12)
				{
					$err[] =  "ตัวเลขยอดขายรายเดือน ไม่ถูกต้อง !";
					// $ele_arr=$element_arr[$key];
					// $element=array($element,$ele_arr);
					array_push($element,$element_arr[$key]);

				}
			}
		}

	    unset($element_arr,$element_arr , $sum_arr);
		// end month  score 


		$totalScore = $knowledge_score + $performance_score + $ssmi_score ; 
		if($totalScore/100 != 100)
		{
			$err[] =  "คะแนนรวมไม่ถูกต้องกรุณาลองอีกครั้ง !";
			array_push($element,"knowledge_score","performance_score","ssmi_score");
		}

		//performance_score by series
		$series_steadiness  = $this->input->post("series_steadiness");
		$condition_arr['series_steadiness'] = $series_steadiness ;

		if($max_month + $series_steadiness != $performance_score )
		{
			$err[] =  "ตัวเลขยอดขายรายเดือน และ  ความสม่ำเสมอในการขายรถทุก series ไม่ถูกต้อง !";
			array_push($element,"series_steadiness","max_month");
		}

		$series_always  = $this->input->post("series_always");
		$condition_arr['series_always'] = $series_always ;

		$series_momth_all  = $this->input->post("series_momth_all");
		$condition_arr['series_momth_all'] = $series_momth_all ;

		
		$exam_score  = $this->input->post("exam_score");
		$condition_arr['exam_score'] = $series_momth_all ;

		
		$interview_score  = $this->input->post("interview_score");
		$condition_arr['interview_score'] = $interview_score ;

		if($exam_score + $interview_score != $knowledge_score )
		{
			$err[] =  "การทดสอบความรู้ทางระบบ E-Exam ผ่าน T-web และ การสำรวจทางโทรศัพท์ คะแนนสูงสุด ไม่ถูกต้อง !";
			array_push($element,"exam_score","interview_score");
		}

		if(count($err) < 1)
		{
		// save จริง
		$main_type_award=$this->input->post("main_type_award");
		$main_type_name=$this->input->post("main_type_name");
		$main_set_name=$this->input->post("main_set_name");

		$arr = array(
			"name" => $main_set_name ,
			"level_id" => $main_type_award ,
			"level_name" => trim($main_type_name) ,
			"status" => '1' ,
			"created_at" => date('Y-m-d H:i:s') ,
		);
		// insert_set
		$set_id =$this->condition_model->insert_set($arr);

		//knowledge_score
		foreach($condition_arr as $key => $data){
			$arr = array(
				"name" => $key ,
				"score" => $data ,
				"set_id" => $set_id  
			);

			$where= array("name"=>$key , "set_id" => $set_id ) ;
			$count=$this->condition_model->get_condition_count($where);
			if($count){
				$this->condition_model->update_condition_score($where,$arr);
			}
			else
			{
				$this->condition_model->insert_condition_score($arr);
			}
		}

		$this->condition_model->delete_month_score($set_id,$unit_start,$unit_end);
		if(is_array($group_arr))
		{
			$score_arr = array();
			foreach($group_arr as $key => $group){
				$inputname = "month_score_". $group->initial ; 
				$score_arr = $_POST[$inputname];
                
				$n = 0;
				for($i =$unit_start ; $i <= $unit_end ; $i++ )
				{
					// $unit_arr[] = $i ;
					$arr = array(
						"units"=> $i ,
						"group_id" => $group->id ,
						"score" => $score_arr[$n] , 
						"set_id" => $set_id , 
						"active" => '1', 
					);
					$this->condition_model->insert_month_score($arr);
					$n++;
				}
			}
		}

		$avanza_group_id  = $this->input->post("avanza_group_id");
		if(is_array($avanza_group_id))
		{
			$avanza_frequency  = $this->input->post("avanza_frequency");
			$avanza_score  = $this->input->post("avanza_score");
			$avanza_allmonth_score  = $this->input->post("avanza_allmonth_score");

			foreach($avanza_group_id as $key => $data){
				$arr = array(
					"set_id"=> $set_id  ,
					"group_id"=> $data  ,
					"frequency"=> $avanza_frequency[$key]  ,
					"score"=> $avanza_score[$key]  ,
					"allmonth_score"=> $avanza_allmonth_score[$key]  ,
					"created_at"=> date("Y-m-d h:i:s") ,
				);

			   $this->condition_model->insert_avanza_score($arr);
			}
		}

		$interview_rate  = $this->input->post("interview_rate");
		if(is_array($interview_rate))
		{
			$where=array("set_id"=> $set_id);
			$count = $this->condition_model->get_interview_count($where);
			foreach($interview_rate as $key => $data){
				for($i =1 ; $i <= 4 ; $i++)
				{
					$inputname = "interview_quarter". $i ; 
				    $score = $_POST[$inputname][$key];

					$arr = array(
						"set_id"=> $set_id  ,
						"quarter"=> $i  ,
						"rate"=> $data ,
						"score"=> $score ,
					);

					if($count){
						$where=$arr = array(
							"set_id"=> $set_id  ,
							"quarter"=> $i  ,
							"rate"=> $data ,
						);
			   			$this->condition_model->update_interview_score($where,array("score"=> $score));
					}else
					{
			   			$this->condition_model->insert_interview_score($arr);
					}
				}

			}
		}


		$row_point  = $this->input->post("row_point");
		if(is_array($row_point))
		{
			$operation_point_arr  = $this->input->post("operation_point");
			$point1_arr  = $this->input->post("point1");
			$point2_arr  = $this->input->post("point2");

			$where=array("set_id"=> $set_id);
			$count = $this->condition_model->get_ssmi_count($where);
			foreach($row_point as $key => $data){

				for($i =1 ; $i <= 12 ; $i++)
				{
					$inputname = "ssmi__month_". $i ; 
				    $score = $_POST[$inputname][$key];

					$arr = array(
						"set_id"=> $set_id ,
						"operation"=> $operation_point_arr[$key]  ,
						"point1"=> $point1_arr[$key] ,
						"point2"=> $point2_arr[$key] ,
						"month"=> $i ,
						"score"=> $score ,
						"point_row"=> ($key+1) ,
						"created_at"=> date('Y-m-d h:i:s') ,
					);

					if($count){
						$where=$arr = array(
							"set_id"=> $set_id  ,
							"quarter"=> $i  ,
							"rate"=> $data ,
						);
			   			$this->condition_model->update_ssmi_score($where,array("score"=> $score));
					}else
					{
			   			$this->condition_model->insert_ssmi_score($arr);
					}
				}

			}
		 }

		  $arr = array(
				"status"=> true ,
				"message" => "บันทึกข้อมูลสำเร็จ" ,
				"element" => ""
		   );


		}
		else
		{
			$arr = array(
				"status"=> false ,
				"message" => $err ,
				"element" => $element
		   );

		}
		//end if


		
        echo  json_encode($arr);

	}

	public function saveTrainer(){

		$err= array();
		$element = array();
		$condition_arr = array();

		
		$interview_max_score=$this->input->post("interview_max_score");
		$condition_arr['interview_max_score'] = $interview_max_score ;



		$exam_max_score=$this->input->post("exam_max_score");
		$condition_arr['exam_max_score'] = $exam_max_score ;

		
		$element_arr= array();
		$unit_arr= array();

		// $month_score = array();
		// $group_arr = $this->dealers_model->get_group();
		// if(is_array($group_arr))
		// {
		// 	foreach($group_arr as $key => $group){
		// 		$inputname = "month_score_". $group->initial ; 
		// 		$sum = $_POST[$inputname];
		// 		$sum_arr[]=max($sum);
		// 		// $month_score[]=$sum ;
		// 		$element_arr[] = $inputname ;
		// 	}
		// }

	    // unset($element_arr,$element_arr , $sum_arr);
		// end month  score 


		// $totalScore = $knowledge_score + $performance_score + $ssmi_score ; 
		// if($totalScore/100 != 100)
		// {
		// 	$err[] =  "คะแนนรวมไม่ถูกต้องกรุณาลองอีกครั้ง !";
		// 	array_push($element,"knowledge_score","performance_score","ssmi_score");
		// }


		if(count($err) < 1)
		{
		// save จริง
		$main_type_award=$this->input->post("main_type_award");
		$main_type_name=$this->input->post("main_type_name");
		$main_set_name=$this->input->post("main_set_name");

		$arr = array(
			"name" => $main_set_name ,
			"level_id" => $main_type_award ,
			"level_name" => trim($main_type_name) ,
			"status" => '1' ,
			"created_at" => date('Y-m-d H:i:s') ,
		);
		// insert_set
		$set_id =$this->condition_model->insert_set($arr);

		//knowledge_score
		foreach($condition_arr as $key => $data){
			$arr = array(
				"name" => $key ,
				"score" => $data ,
				"set_id" => $set_id  
			);

			$where= array("name"=>$key , "set_id" => $set_id ) ;
			$count=$this->condition_model->get_condition_count($where);
			if($count){
				$this->condition_model->update_condition_score($where,$arr);
			}
			else
			{
				$this->condition_model->insert_condition_score($arr);
			}
		}

		// $this->condition_model->delete_month_score($set_id,$unit_start,$unit_end);
		// if(is_array($group_arr))
		// {
		// 	$score_arr = array();
		// 	foreach($group_arr as $key => $group){
		// 		$inputname = "month_score_". $group->initial ; 
		// 		$score_arr = $_POST[$inputname];
                
		// 		$n = 0;
		// 		for($i =$unit_start ; $i <= $unit_end ; $i++ )
		// 		{
		// 			// $unit_arr[] = $i ;
		// 			$arr = array(
		// 				"units"=> $i ,
		// 				"group_id" => $group->id ,
		// 				"score" => $score_arr[$n] , 
		// 				"set_id" => $set_id , 
		// 				"active" => '1', 
		// 			);
		// 			$this->condition_model->insert_month_score($arr);
		// 			$n++;
		// 		}
		// 	}
		// }


		$interview_rate  = $this->input->post("interview_rate");
		if(is_array($interview_rate))
		{
			$where=array("set_id"=> $set_id);
			$count = $this->condition_model->get_interview_count($where);
			foreach($interview_rate as $key => $data){
				for($i =1 ; $i <= 4 ; $i++)
				{
					$inputname = "interview_quarter". $i ; 
				    $score = $_POST[$inputname][$key];

					$arr = array(
						"set_id"=> $set_id  ,
						"quarter"=> $i  ,
						"rate"=> $data ,
						"score"=> $score ,
					);

					if($count){
						$where=$arr = array(
							"set_id"=> $set_id  ,
							"quarter"=> $i  ,
							"rate"=> $data ,
						);
			   			$this->condition_model->update_interview_score($where,array("score"=> $score));
					}else
					{
			   			$this->condition_model->insert_interview_score($arr);
					}
				}

			}
		}


		

		  $arr = array(
				"status"=> true ,
				"message" => "บันทึกข้อมูลสำเร็จ" ,
				"element" => ""
		   );


		}
		else
		{
			$arr = array(
				"status"=> false ,
				"message" => $err ,
				"element" => $element
		   );

		}
		//end if


		
        echo  json_encode($arr);
		
	}
	
	public function saveMgr(){


	}

	public function setlist()
	{
		$this->page_level_css = array(
        "assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css" ,
        "assets/plugins/DataTables/extensions/Buttons/css/buttons.bootstrap.min.css" ,
        "assets/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css",
        "assets/plugins/bootstrap-datepicker/css/datepicker.css",
        "assets/plugins/bootstrap-datepicker/css/datepicker3.css",
    );

		$this->page_level_js = array(
            'assets/plugins/DataTables/media/js/jquery.dataTables.js',
            'assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js' ,
            "assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js",
            "assets/plugins/DataTables/extensions/Buttons/js/dataTables.buttons.min.js",
            "assets/plugins/DataTables/extensions/Buttons/js/buttons.bootstrap.min.js",
            "assets/plugins/DataTables/extensions/Buttons/js/buttons.flash.min.js",
            "assets/plugins/DataTables/extensions/Buttons/js/jszip.min.js",
            "assets/plugins/DataTables/extensions/Buttons/js/pdfmake.min.js",
            "assets/plugins/DataTables/extensions/Buttons/js/vfs_fonts.min.js",
            "assets/plugins/DataTables/extensions/Buttons/js/buttons.html5.min.js",
            "assets/plugins/DataTables/extensions/Buttons/js/buttons.print.min.js",
            "assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js",
            "assets/plugins/bootstrap-daterangepicker/moment.js",
            "assets/plugins/bootstrap-eonasdan-datetimepicker/build/js/bootstrap-datetimepicker.min.js",
            "assets/js/table-manage-buttons.demo.js",
            "assets/js/form-schedule.js",
		);

		$this->load->model('set_model');

		
		$this->middle = 'condition/index';
		$this->title = 'เลือกกติกาเงื่อนไข';
		$this->data['List'] = $this->set_model->get_allset();
		$this->data['breadcrumb'] = array(
			array('name'=>'กติกาการแข่งขัน' , 'link' => BASE_URL('competition') , 'active' => false ) ,
			array('name'=>'เลือกกติกาเงื่อนไข' , 'link' => BASE_URL("competition") , 'active' => true )
		); 
        $this->js = array('TableManageButtons.init();');
		$this->view();
	}


	public function updatRules()
	{
		$this->page_level_css = array(
        "assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css" ,
        "assets/plugins/DataTables/extensions/Buttons/css/buttons.bootstrap.min.css" ,
        "assets/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css",
        "assets/plugins/bootstrap-datepicker/css/datepicker.css",
        "assets/plugins/bootstrap-datepicker/css/datepicker3.css",
    );

		$this->page_level_js = array(
            'assets/plugins/DataTables/media/js/jquery.dataTables.js',
            'assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js' ,
            "assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js",
            "assets/plugins/DataTables/extensions/Buttons/js/dataTables.buttons.min.js",
            "assets/plugins/DataTables/extensions/Buttons/js/buttons.bootstrap.min.js",
            "assets/plugins/DataTables/extensions/Buttons/js/buttons.flash.min.js",
            "assets/plugins/DataTables/extensions/Buttons/js/jszip.min.js",
            "assets/plugins/DataTables/extensions/Buttons/js/pdfmake.min.js",
            "assets/plugins/DataTables/extensions/Buttons/js/vfs_fonts.min.js",
            "assets/plugins/DataTables/extensions/Buttons/js/buttons.html5.min.js",
            "assets/plugins/DataTables/extensions/Buttons/js/buttons.print.min.js",
            "assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js",
            "assets/plugins/bootstrap-daterangepicker/moment.js",
            "assets/plugins/bootstrap-eonasdan-datetimepicker/build/js/bootstrap-datetimepicker.min.js",
            "assets/js/table-manage-buttons.demo.js",
            "assets/js/form-schedule.js",
		);
		
		$this->middle = 'condition/updat-rules';
		$this->title = 'ปรัปปรุงกติกา';
		$this->data['breadcrumb'] = array(
			array('name'=>'กติกาการแข่งขัน' , 'link' => BASE_URL('competition') , 'active' => false ) ,
			array('name'=>'ปรัปปรุงกติกา' , 'link' => BASE_URL("competition/updatRules") , 'active' => true )
		); 
        $this->js = array('FormPlugins.init();', 'TableManageButtons.init();');
		$this->view();
	}

	public function delete_rules($id){
		$this->set_model->delete_set($id);
		$this->condition_model->delete_interview_score($id);
		$this->condition_model->delete_ssmi_score($id);
		$this->condition_model->delete_month_by_set($id);
		$this->condition_model->delete_avanza_score($id);
		$this->condition_model->delete_condition_score($id);
		redirect('condition/setlist', 'refresh');
	}


	public function get_json_htmlssmi($id=null) {


		 $html = '<tr>
				<td>
					<a data-toggle="tooltip" title="ลบแถว"  class="btn btn-danger btn-xs btn-circle m-r-5 btn-handleDeletePointRow" >
						<i class="fa fa-minus" aria-hidden="true"></i>
					</a>
					<input class="form-control input-sm" name="row_point[]" type="hidden" value="1"  />
				</td>
				<td>
					<select name="operation_point[]" class="form-control sel-handChangOption">
						<option value="1" > > </option>
						<option value="2" > >= </option>
						<option value="3" > < </option>
						<option value="4" > <= </option>
						<option value="5" > = </option>
						<option value="6" >ระหว่าง</option>
					</select>
				</td>
				<td>
					<div class="row">
						<div class="col-md-12">
							<input class="form-control input-sm" name="point1[]" type="text" value=""  />
						</div>    
						<div class="col-md-12 hide">
							<span>To</span> <input class="form-control input-sm" min="1" name="point2[]"  type="text" value=""  />
						</div>    
					</div>
				</td>';

					for($i =1 ; $i <= 12 ; $i++){
						$html .= "<td><input class='form-control input-sm' type='number' name='ssmi__month_{$i}[]' value=''  /></td>";
					}
                                            
                $html .= "</tr>";

	    $arr = array(
			"html"=>$html ,
			"status" => true 
		);

		echo  json_encode($arr);		
	}


	public function get_json_MonthScore($start=1,$end=1,$set=null)
	{
		echo ' <table class="table table-bordered table-striped">
				<thead>
				<tr>
					<th width="120">กลุ่ม</th> ';
					for($i =$start ; $i <= $end ; $i++ )
					{
						echo "<th class='text-center'>{$i} คัน</th>";
					}
		  echo '</tr>
				</thead>
				<tbody>';

				$group_arr = $this->dealers_model->get_group();
				if(is_array($group_arr))
				{
					foreach($group_arr as $key => $group){
						echo "<tr>
								<td>{$group->name}</td>";
								for($i =$start ; $i <= $end ; $i++ )
								{
									echo '<td><input min="1" name="month_score_' . $group->initial .'[]" class="form-control input-sm limitscore"  type="number" value="0"  /></td>';
								}

						echo'</tr>';
					}
				}
					
				
		  echo '</tbody>
			</table>';	
	}
}
