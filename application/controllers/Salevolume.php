<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Salevolume extends MY_Controller {

    var $page_level_css = array(
        "assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css" ,
        "assets/plugins/DataTables/extensions/Buttons/css/buttons.bootstrap.min.css" ,
        "assets/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css",
        "assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js",
    );

	var $page_level_js = array(
            'assets/plugins/DataTables/media/js/jquery.dataTables.js',
            'assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js' ,
            "assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js",
            "assets/plugins/DataTables/extensions/Buttons/js/dataTables.buttons.min.js",
            "assets/plugins/DataTables/extensions/Buttons/js/buttons.bootstrap.min.js",
            "assets/plugins/DataTables/extensions/Buttons/js/buttons.flash.min.js",
            "assets/plugins/DataTables/extensions/Buttons/js/jszip.min.js",
            "assets/plugins/DataTables/extensions/Buttons/js/pdfmake.min.js",
            "assets/plugins/DataTables/extensions/Buttons/js/vfs_fonts.min.js",
            "assets/plugins/DataTables/extensions/Buttons/js/buttons.html5.min.js",
            "assets/plugins/DataTables/extensions/Buttons/js/buttons.print.min.js",
            "assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js",
            "assets/js/table-manage-buttons.demo.js",
		);


	public function __construct(){
		parent::__construct();

		$this->load->model('lotto_model');
		$this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');

	}

	public function index()
	{

        $search_arr = array();
		$schedule_id = $this->input->post('schedule_id');

		if(!empty($schedule_id)){
			$search_arr['schedule_id'] = $schedule_id;
		}
		// var_dump($search_arr);
		// exit();

        $this->middle = 'salevolume/table';
		$this->title = 'ສະຫຼຸບຍອດຂາຍ';
		$this->js = array('TableManageButtons.init();');
		$this->data['search'] = $search_arr ;
		$this->data['result'] = $this->lotto_model->get_salevolume($search_arr);
		$this->data['period'] = $this->lotto_model->get_period();
		$this->data['breadcrumb'] = array(
				array('name'=>'ສະຫຼຸບຍອດຂາຍ' , 'link' => BASE_URL('salevolume') , 'active' => false ) ,
				array('name'=>'ສະຫຼຸບຍອດຂາຍງວດ' , 'link' => BASE_URL("salevolume/table") , 'active' => true )
			  ); 

		$this->view();


	}


    public function SalesSummary()
	{
		
		$search_arr = array();
		$schedule_id = $this->input->post('schedule_id');

		if(!empty($schedule_id)){
			$search_arr['schedule_id'] = $schedule_id;
		}

        $this->middle = 'salevolume/sales_summary';
		$this->title = 'ຄົ້ນຫາໃບບິນຂາຍເລກ';
		$this->js = array('TableManageButtons.init();');
		$this->data['search'] = $search_arr ;
		$this->data['result'] = $this->lotto_model->get_salesummary($search_arr);
		$this->data['period'] = $this->lotto_model->get_period();
		$this->data['breadcrumb'] = array(
				array('name'=>'ສະຫຼຸບຍອດຂາຍ' , 'link' => BASE_URL('salevolume') , 'active' => false ) ,
				array('name'=>'ຄົ້ນຫາໃບບິນຂາຍເລກ' , 'link' => BASE_URL("salevolume/table") , 'active' => true )
			  ); 

		$this->view();
	}



}
