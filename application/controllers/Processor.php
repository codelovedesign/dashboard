<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ini_set('max_execution_time', 3000000); //300 seconds = 5 minutes30
ini_set("memory_limit","2024M");//128

class Processor extends MY_Controller {

    var $year = 2016;
    var $set ;
    var $db2;
    var $dbname_default = "saleaward_";

    var $current_date ;
    var $page_level_js = array(
		'assets/plugins/parsley/dist/parsley.js',
		'assets/js/award-processor.js',
	);
	public function __construct(){
		parent::__construct();

        
        $this->load->helper(array('form', 'url'));
        $this->current_date = date("Y-m-d h:i:s");
        $this->load->library('form_validation');

        $this->load->model('employee_model');
        $this->load->model('award_model');
        $this->load->model('set_model');

	}
    public function index(){

        $this->middle = 'processor/index';
		$this->title = 'ประมวลผลรางวัล';
        // echo "xxxxxxxx";
        // exit();

		$this->data['employee_level'] = $this->employee_model->get_employee_level();
		$this->data['award_year'] = $this->award_model->get_award_year();

		
         $this->data['breadcrumb'] = array(
            array('name'=>'ผลการแข่งขัน' , 'link' => BASE_URL('report') , 'active' => false ) ,
            array('name'=>'ประมวลผลรางวัล' , 'link' => BASE_URL("report/nation") , 'active' => true )
        ); 
		$this->js = array('report.init();');
		$this->view();

        
    }

    public function get_option_award_type($id){
        $arr = array("employee_level_id"=>$id);
        $result =  $this->award_model->get_award_type($arr,"data_type");
        $html = "";
        if(is_array($result)){
            foreach($result as $key => $item){
                $html .=  "<option value='{$item->id}'>{$item->name_th}</option>";
            }
        }


        echo $html ;

    }

    public function get_option_set($id){
        $arr = array("level_id"=>$id);
        $result =  $this->set_model->get_set($arr);
        $html = "";
        if(is_array($result)){
            foreach($result as $key => $item){
                $html .=  "<option value='{$item->id}'>{$item->name}</option>";
            }
        }


        echo $html ;

    }
    public function test(){

        $database_new_name = $this->dbname_default. "2016" ;
        $this->db2 = $this->load->database("otherdb", TRUE);
        $this->db2->db_select($database_new_name);
        $this->set = 1;

        $series_arr  = array(
            array("CARY"),
            array("COLA"),
            array("VIOS"),
            array("YARI"),
            array("HRBC"),
            array("HR4D"),
            array("HLPV"),
            array("AVAN"),
            array("INNO"),
        );


        echo $this->get_Month_Performance(124139,2, $series_arr ) ;
        echo "=> get_Month_Performance<br/>";

        $a = $this->get_series_always(124139,2, $series_arr ) ;

        echo $a . " => get_series_always <br/>";

        $b = $this->get_series_momth_all(124139,2) ;
        echo $b . " =>get_series_momth_all <br/>";

        $c  =$this->get_series_avanza_score(124139,2, array("AVAN", "INNO"));
        echo $c . " => get_series_avanza_score <br/>";

        $total = $a+ $b + $c ;

        $this->update_ssmi_score();
        echo $this->get_ssmi_score("11247","BB");

        var_dump($total);
    }

    public function CheckdataUpload($level){

        $this->db->select('name,table_name');
        $this->db->where('table_name is NOT NULL', NULL, FALSE);
        $query = $this->db->get_where('tb_import', array('level_id' => $level) );
        if ($query->num_rows() > 0) {
            $result = $query->result();
            // echo $this->db->last_query();
            foreach ($result as $rs) {
                $table_name =  $rs->table_name;

                $this->db2->select('id');
                $query = $this->db2->get("{$table_name}");
                if ($query->num_rows() == 0) {
                    $data_arr[] = $rs->name ;
                }  
            }       
           unset($result,$rs);
        }

        if(count($data_arr)){
            return true;
        }
        else
        {
            return false;
        }
    }

    public function error_import_data($level,$year ){


        $this->year = $year ;
        $database_new_name = $this->dbname_default. $this->year ;
        $this->db2 = $this->load->database("otherdb", TRUE);
        $this->db2->db_select($database_new_name);

        $this->db->select('name,table_name');
        $this->db->where('table_name is NOT NULL', NULL, FALSE);
        $query = $this->db->get_where('tb_import', array('level_id' => $level) );
        if ($query->num_rows() > 0) {
            $result = $query->result();
            // echo $this->db->last_query();
            foreach ($result as $rs) {
                $table_name =  $rs->table_name;

                $this->db2->select('id');
                $query = $this->db2->get("{$table_name}");
                if ($query->num_rows() == 0) {
                    $data_arr[] = $rs->name ;
                }  
            }       
           unset($result,$rs);
        }

        $views = array(
            'title'=> 'ข้อความจากระบบ',
            'head'=> 'ฐานข้อมูลในระบบไม่สมบูรณ์ กรุณาตรวจสอบข้อมูล',
            'data_error'=> $data_arr ,
            'callback'=> base_url(). "adjust/import/1/" . $this->year ,
            'theme' => array(
                'file' => 'alert',
                'module' => 'global',
                'template' => 'index',
                'theme_name' => 'theme_sales'
            )
        );

        $this->load->view($views['theme']['theme_name'] . '/layouts/'  . $views['theme']['template'], $views  );

            // exit();
    }

    public function compile(){

        $this->year = $this->input->post('year');
        $this->set = $this->input->post('set_award');


        $employee_level = $this->input->post('employee_level');
        $award_type = $this->input->post('award_type');
        // $this->form_validation->set_rules('year', 'Year', 'trim|required');
        if(empty($this->year) || empty($this->set)){
            redirect('Processor');
        }
		
        $r =  $this->award_model->get_award_type_by_id($award_type);
        if($r)
        {
            $database_new_name = $this->dbname_default. $this->year ;
            $this->db2 = $this->load->database("otherdb", TRUE);
            $this->db2->db_select($database_new_name);

            //เช็ค เทเบิลที่จะประมวลผลมีข้อมูลไหม
            if($this->CheckdataUpload(1)){
                redirect('Processor/error_import_data/1/' . $this->year,'');
            }

                $this->update_ssmi_score();
                if($award_type != 7){
                    $license_arr=json_decode($r['license'],true) ;
                    $arr = array($license_arr,$r['license_count'],$r['data_type'] ,$r['limit']);
                    echo "กำลังประมวลผล...";
                    $this->run($arr);
                }
                else
                {
                    $this->halloffame();
                }
            

        }
        // if(is_array($result)){
        //     foreach($result as $key => $item){
        //         $html .=  "<option value='{$item->id}'>{$item->name_th}</option>";
        //     }
        // }

        // type จะเปลี่ยนทำให้ได้แค่รางวัลเล็กสุด
        // $awd[3] = array(array("TSC","RSC"),1,$index , 25);
        // $awd[2] = array(array("TSE","RSE"),3,$index , 50 );
        // $awd[1] = array(array("TSG","RSG"),3,$index , 75 );
        // $this->run($awd[$index]);
        // $this->halloffame();
    }

    public function halloffame(){
        $where=array(
            'year >' => $this->year -3 ,
        );
        $this->db2->select('sale_code,count(year) as count,GROUP_CONCAT(year)');
        $this->db2->group_by("sale_code"); // Produces: GROUP BY title
        $this->db2->having('count(year)','2');  // Produces: HAVING user_id = 45
        $query = $this->db2->get_where('tb_history', $where );
        // echo $this->db->last_query();
        // echo $query->num_rows() ;
        // exit();
        $sale_code_arr = array();
        if ($query->num_rows() > 0) {
            $result = $query->result();
            foreach ($result as $rs) {
             $sale_code_arr[] = $rs->sale_code ;
            //  echo $rs->sale_code . "<br/>";
            }         
           unset($result,$rs);
        }

        $this->db2->select('*');
        $this->db2->where("tb_award.order >",0);
        $this->db2->where("year",$this->year);
        $this->db2->where("type <",4);
        if(count($sale_code_arr) > 0 ) {
            $this->db2->where_in("sale_code",$sale_code_arr);
        } 
        $query = $this->db2->get("tb_award");

        $sale_code_arr = array();
        if ($query->num_rows() > 0) {
            $result = $query->result();
            foreach ($result as $rs) {
                // var_dump($rs);
                // echo $rs->sale_code . "<br/>";
                $arr = array(
                    "type"=> 4,
                    "sale_code"=> $rs->sale_code ,
                    "name"=> $rs->name ,
                    "lastname"=> $rs->lastname ,
                    "branch_code"=> $rs->branch_code ,
                    "dealer_code"=> $rs->dealer_code ,
                    "total_score"=> $rs->total_score ,
                    "year"=> $this->year ,
                    "created_at"=> $this->current_date ,
                );

                $this->insert_sales_award($arr);
            }         
           unset($result,$rs);
           $this->create_order(4);

        }

        redirect('competition/' . $this->year , 'refresh');

    }

    public function run($param = array()){
        
        $starttime = microtime(true);

        $employee_arr = $this->get_sale_award($param[0],$param[1]);
        
        $sql= "SELECT id,dealer_code,salesman_code,branch_code,firstname,lastname FROM tb_employee em " ;
        if(count($employee_arr)>0)
        {
           $code_str= "'" .implode("','", $employee_arr) . "'";
           $sql .= " WHERE em.salesman_code IN({$code_str})" ;
        }
        else{
            $sql .= " WHERE em.id != '' ";
        }
        // foreach($where as $key => $w){
            $sql .= " AND em.ddms IN ('01','02','03','04') " ;
            $approval_date = date('Y-m-d',strtotime($this->year .'-12-31')) ;
            $sql .= " AND approval_date <= '{$approval_date}'" ;
        // }
        
        $query = $this->db2->query($sql);

        // echo $query->num_rows();
        if ($query->num_rows() > 0) {
            $result = $query->result();
            // var_dump($result);
            foreach ($result as $rs) {
                $score = 0;
                $point1 =0;
                $point1_2 =0;
                $point2 =0;
                $point2_2 =0;
                $point3 =0;

                $group_id = 0;
                $this->db2->where("dealer_code", $rs->dealer_code );
                $query_group = $this->db2->get('tb_dealers');
                if ($query_group->num_rows() > 0) {
                    $r = $query_group->row_array();
                    $group_id =  $r['group_id'] ; 
                    $dealer_name =  $r['dealer_name'] ; 
                }
                else{
                    // echo "<br/> ERROR ไม่มีรหัสกลุ่ม " . $rs->dealer_code . " ||";
                }


            //    var_dump($group_id);
                $code = $rs->salesman_code;
                $dealer_code = $rs->dealer_code;
                $branch_code = $rs->branch_code;

                $series_arr  = array(
                    array("CARY"),
                    array("COLA"),
                    array("VIOS"),
                    array("YARI"),
                    array("HRBC"),
                    array("HR4D"),
                    array("HLPV"),
                    array("AVAN"),
                    array("INNO"),
                );
                //1
                $point1 +=$this->get_Month_Performance($code, $group_id , $series_arr);
               

                $point1_2 +=$this->get_series_always($code, $group_id , $series_arr);
                $point1_2 +=$this->get_series_momth_all($code, $group_id);


                // array("AVAN","INNO"));
                $point1_2 +=$this->get_series_avanza_score($code, $group_id ,  array("AVAN","INNO") ) ;

                $score += $point1;
                $score += $point1_2;

                //2
                $point2 +=$this->get_exam_score($code);
                $point2_2 +=$this->get_interview_score($dealer_code,$branch_code);

                $score += $point2 ;
                $score += $point2_2 ;

                // //3
                $point3 +=$this->get_ssmi_score($dealer_code,$branch_code);
                $score += $point3 ;

                
                $arr = array(
                    "type"=> $param[2],
                    "sale_code"=> $code ,
                    "name"=> $rs->firstname ,
                    "lastname"=> $rs->lastname ,
                    "branch_code"=> $rs->branch_code ,
                    "dealer_code"=> $rs->dealer_code ,
                    "dealer_name"=> $dealer_name ,
                    "total_score"=> $score ,
                    "monthly_score"=> $point1 ,
                    "regularity_score"=> $point1_2 ,
                    "exam_score"=> $point2 ,
                    "interview_score"=> $point2_2 ,
                    "ssmi_score"=> $point3 ,
                    "year"=> $this->year ,
                    "created_at"=> $this->current_date ,
                );

                $this->insert_sales_award($arr , $param[3]);

            }
            

            $this->create_order($param[2],$param[3]);
            redirect('competition/' . $this->year , 'refresh');
            //จัดอันดับ     
            $endtime = microtime(true);
            echo $duration = $endtime - $starttime; //calculates total time taken
            echo " เสร็จแล้ว";    

        }
    }


    //ประสิทธิภาพการขาย รายเดือน
    public function get_sale($id){
         //ดึงยอดขายปีการแข่งขัน

        // $this->db->select('count(id) as total');

         $where = array(
        //   'dl_month <=' => date('Y-m-d',strtotime($this->year.'-12-31')) ,
        //   'dl_month >=' => date('Y-m-d',strtotime($this->year.'-1-1')) ,
        //   'fleet_id IS NULL' => null,
          'salesman_code' => $id,
        );
        // $this->db->group_by('MONTH(dl_month)'); 

        $query = $this->db->get_where('tb_sales', $where );
        // echo $this->db->last_query();
        $result =  array();
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            // var_dump($result); 
            // return $score ;
            // foreach ($result as $rs) {
                // $total = $rs['total'];
                // $score += $this->SalesPerformance($total , $group_id);
            // }         
        }

        return $result ;
        // $this->db->close();




    }

   public function create_order($type,$max= null){

    

    $database_new_name = $this->dbname_default. $this->year ;
    $this->db2 = $this->load->database("otherdb", TRUE);
    $this->db2->db_select($database_new_name);

    // $this->db2->where('type', $type);
    $this->db2->update('tb_award', array("order"=>""));

    $where = array(
        "type" => $type ,
    );
    if($max){
        $this->db2->limit($max);  
    }
    $this->db2->order_by('total_score', 'desc'); 
    $query = $this->db2->get_where('tb_award', $where );

    if ($query->num_rows() > 0) {
        $result = $query->result();
        foreach ($result as $key =>$rs) {
            $arr = array(
            "order" => ($key +1 ),
            );

            $this->db2->where('id', $rs->id);
            $this->db2->update('tb_award', $arr);
            // echo $this->db->last_query();
        }   
    }
   
   } 

   public function insert_sales_award($arr , $limit = null){
    
    //แต่ละรางวัล ห้ามซ้ำ ถ้าหากมีข้อมูลซ้ำจะอัพเดทแทน
    if($arr['type'] < 4){
        $this->db2->where("type <", $arr['type']);
        $this->db2->where("order >" , "0");
        // $this->db->order_by('score', 'desc'); 
        // $this->db->limit($limit);
    }
    else{
        $this->db2->where("type", $arr['type'] );
    }

    $query = $this->db2->get_where('tb_award', array("sale_code" => $arr['sale_code']) );
                // echo $this->db->last_query();


    if ($query->num_rows() == 0) {

           $query = $this->db2->get_where('tb_award', array("sale_code" => $arr['sale_code'] ));
           if ($query->num_rows() == 0) {
                $this->db2->insert('tb_award', $arr);
           }
           else{
               $row = $query->row();
                if (isset($row))
                {
                    $arr2=array("updated_at"=>$this->current_date);
                    $arr=(array_merge($arr,$arr2));
                    unset($arr['created_at']);

                    $this->db2->where('id', $row->id);
                    $this->db2->update('tb_award', $arr);

                }
           }

    }
    else if($arr['type'] == 4)
    {
        $row = $query->row();
        if (isset($row))
        {
            $arr2=array("updated_at"=>$this->current_date);
            $arr=(array_merge($arr,$arr2));
            unset($arr['created_at']);

            $this->db2->where('id', $row->id);
            $this->db2->update('tb_award', $arr);

        }
    }
    else
    {
        $row = $query->row();

        // var_dump($row);
        if (isset($row))
        {
            $this->db2->where('sale_code', $arr['sale_code']);
            $this->db2->where('type', $arr['type']);
            $this->db2->delete('tb_award');

        }
    }
      
   }

   public function get_sale_award($name_award = array(), $number_course=1 ){

        $this->db2->select("*,count(code) as sumcode ,GROUP_CONCAT(code SEPARATOR ',') ")
        ->where_in("code", $name_award )
        ->where("result","P")
        ->having("sumcode >=",$number_course) 
        ->group_by(array('sale_code')); 
        // ->group_by(array('code','sale_code')); 
        $query = $this->db2->get('tb_license');
        //  echo $this->db->last_query();

        // echo $query->num_rows();
        $sale_code_arr = array();
        if ($query->num_rows() > 0) {
            $result = $query->result();
            foreach ($result as $rs) {
               $sale_code_arr[] = $rs->sale_code ;
            }         
           unset($result,$rs);
        }

        return $sale_code_arr;
   }

   public function get_series_always($id , $group_id  , $series_arr = array()  ){
        $total = 0;
        foreach($series_arr as $key  => $item ){
            $this->db2->select('series');
            $where = array(
            'dl_month <=' => date('Y-m-d',strtotime($this->year.'-12-31')) ,
            'dl_month >=' => date('Y-m-d',strtotime($this->year.'-1-1')) ,
            // 'fleet_id IS NULL' => null,
            'salesman_code' => $id,
            // 'sorce' => 'DL',
            );
            $this->db2->where_in("series", $item);
            $this->db2->group_by('MONTH(dl_month)'); 
            // $this->db->group_by('series'); 
            // $this->db->order_by('series', 'desc'); 

            $query = $this->db2->get_where('tb_sales_summary', $where );
            // echo $this->db->last_query();
            // echo "<br/>";
            // echo "<br/>";
            $score = 0;
            if ($query->num_rows() > 0) {
                $result = $query->result_array();
                foreach ($result as $rs) {
                    if($rs['series'] != 'AVAN' && $rs['series'] != 'INNO')
                    {
                        $score += $this->get_condition("series_always");
                        // var_dump($score); 
                    }
                }    
                // var_dump($item);
                // echo " : " . $score . "<br/>";
                $total += $score ;     
            }



        }


        // echo "<pre>";
        // var_dump($score);
        return $total ;
   }
   public function get_series_momth_all($id , $group_id){
        $this->db2->select('GROUP_CONCAT(MONTH(dl_month)) allmonth ,series');
        // $this->db->select('count(*) as total ,GROUP_CONCAT(MONTH(dl_month)) allmonth ,MONTH(dl_month) as month,series');

         $where = array(
          'dl_month <=' => date('Y-m-d',strtotime($this->year.'-12-31')) ,
          'dl_month >=' => date('Y-m-d',strtotime($this->year.'-1-1')) ,
        //   'fleet_id IS NULL' => null,
          'salesman_code' => $id,
        //   'sorce' => 'DL',
        );
         $this->db2->group_by('series'); 
         $this->db2->order_by('series', 'desc'); 

        $query = $this->db2->get_where('tb_sales_summary', $where );
        // echo $this->db->last_query();
        $score = 0;
        if ($query->num_rows() > 0) {
            $result = $query->result_array();

            foreach ($result as $rs) {
                if($rs['series'] != 'AVAN' || $rs['series'] != 'INNO')
                {
                    $array = explode(",",$rs['allmonth']);
                    $allmonth_arr = array_unique( $array );
                    if(count($allmonth_arr)==12){
                        $score += $this->get_condition("series_momth_all");
                    }
                }
            }         
        }



        return $score ;
   }
   
   public function get_series_avanza_score($id , $group_id ,$series = array() ){
        $where = array(
          'group_id' => $group_id ,
        );

        $freqcy_score =1;
        $month_score =0;
        $allmonth_score =0;
        $query = $this->db->get_where('tb_avanza_score', $where );
        if ($query->num_rows() > 0) {
            $r = $query->row_array();
            $freqcy_score = $r['frequency'];
            $month_score = $r['score'];
            $allmonth_score = $r['allmonth_score']; 
        }
        // echo "<pre>";
         $this->db2->select('MONTH(dl_month) as month,series');
         $where = array(
          'dl_month <=' => date('Y-m-d',strtotime($this->year.'-12-31')) ,
          'dl_month >=' => date('Y-m-d',strtotime($this->year.'-1-1')) ,
        //   'fleet_id IS NULL' => null,
          'salesman_code' => $id,
        //   'sorce' => 'DL',
        );

        //  $this->db->where_in('series', array("AVAN","INNO"));
        // echo explode( "," ,$series );
        // var_dump($series);

         $this->db2->where_in('series', $series );
         $this->db2->group_by('series'); 
         $this->db2->group_by('MONTH(dl_month)'); 
         $this->db2->order_by('series', 'desc'); 

        $query = $this->db2->get_where('tb_sales_summary', $where );
        // echo $this->db2->last_query();
        // echo "<br/>";
        $score = 0;
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $oldfrequency=0;
            $index = 0 ;
            foreach ($result as $rs) {
                    // $array = explode(",",$rs['allmonth']);
                    // $allmonth_arr = array_unique( $array );
                    // var_dump($allmonth_arr);

                    //ปัดขึ้น
                    if($freqcy_score == 0){
                        $freqcy_score = 1;
                    }

                    $frequency = ceil($rs['month']/$freqcy_score) ;

                    if($frequency > $oldfrequency ){
                        $oldfrequency = $frequency ;
                        $score += $month_score;
                        $index ++;
                    }

            }

            if(12/$freqcy_score == $index ){
                $score += $allmonth_score ;         
            }
        }

        // var_dump($score);
        return $score ;
   }


   public function get_exam_score($id) {

         $where = array(
          'salesman_code' => $id,
          'exam_year' => $this->year,
        );
        $this->db2->limit(4);
        $query = $this->db2->get_where('tb_exam', $where );
        // echo $this->db->last_query();
        $score = 0;
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            // var_dump($result); 
            foreach ($result as $rs) {
                $score += $rs['exam_score'] ;
            }         
        }

        $score = $score * 12.5;

        return $score ;
   }

  public function get_interview_score($dealer_code,$branch_code) {
  //บางสาขาไม่มีคะแนน
         $where = array(
          'dealer_code' => $dealer_code,
          'branch_code' => $branch_code,
          'year' => $this->year,
        );
        $this->db2->limit(4);
        $query = $this->db2->get_where('tb_interview', $where );
        // echo $this->db->last_query();
        $score = 0;
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            // var_dump($result); 
            foreach ($result as $rs) {
                $quarter = $rs['quarter'] ;
                $rating_answer = $rs['rating_answer'] ;

                $score += $this->call_interview_rat($quarter, $rating_answer);
            }         
        }

        // var_dump($score);
        return $score ;
   }


   public function call_interview_rat($quarter,$rate=null){
        $where = array(
          'quarter' => $quarter ,
          'rate' => strtoupper($rate) ,
          'set_id' => $this->set ,
        );

        $this->db->limit(1);
        $this->db->select('score');
        $query = $this->db->get_where('tb_interview_rating', $where );
        // $this->db->last_query();
        $score =0 ;
        if ($query->num_rows() > 0) {
            $r = $query->row_array();
            $score =  $r['score'] ; 
        }

        return $score ;

   }

   public function update_ssmi_score() {
         $where = array(
        //   'dealer_code' => $dealer_code,
        //   'branch_code' => $branch_code,
          'year' => $this->year,
        );
        // $this->db->limit(12);
        $query = $this->db2->get_where('tb_ssmi', $where );
        // echo $this->db->last_query();
        $score = 0;
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            foreach ($result as $rs) {
                $ssmi = $rs['ssmi'] ;
                $ssmi = (int)$ssmi;
                $month = $rs['month'] ;
                $score=$this->call_ssmi_score($ssmi,$month);
                
                $arr = array(
                    "score" => $score ,
                );

                $this->db2->where('id', $rs['id']);
                $this->db2->update('tb_ssmi', $arr);

            }         
        }

        // echo " update_ssmi_score success " . $query->num_rows() ;

        return $score ;
   }
   private function call_ssmi_score($ssmi,$month) {

         $where = array(
          'month' => $month,
          'set_id' => $this->set,
        );
        // $this->db->limit(12);
        $query = $this->db->get_where('tb_ssmi_rating', $where );
        // echo $this->db->last_query();
        $score = 0;
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            foreach ($result as $rs) {
                $operation = $rs['operation'] ;
                $point1 = $rs['point1'] ;
                $point2 = $rs['point2'] ;

                $condition = false ;
                switch ($operation) {
                    case 1 :
                       if($ssmi > $point1){
                        $condition = true ;
                       } 
                        break;
                    case 2 :
                       if($ssmi >= $point1){
                        $condition = true ;
                       } 
                        break;
                    case 3 :
                       if($ssmi < $point1){
                        $condition = true ;
                       } 
                        break;
                    case 4 :
                       if($ssmi <= $point1){
                        $condition = true ;
                       } 
                        break;
                    case 5 :
                       if($ssmi = $point1){
                        $condition = true ;
                       } 
                        break;
                    case 6 :
                       if($ssmi >= $point1 && $ssmi <= $point2){
                        $condition = true ;
                       } 
                        break;
                    default:
                       $condition = false ;
                        break;
                }


                if($condition)
                {
                  $score  = $rs['score'] ;
                  break;
                }

            }         
        }

        return $score ;
   }

   public function get_ssmi_score($dealer_code,$branch_code) {
         $where = array(
          'dealer_code' => $dealer_code,
          'branch_code' => $branch_code,
          'year' => $this->year,
        );
        $this->db2->limit(12);
        $query = $this->db2->get_where('tb_ssmi', $where );
        //  echo $this->db2->last_query();
        $score = 0;
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            foreach ($result as $rs) {
                $score += $rs['score'] ;
            }         
        }


        return $score ;
   }

  
   public function get_condition($name){

        $where = array(
          'name' => $name ,
          'set_id' => $this->set ,
        );
        $this->db->limit(1);

        $query = $this->db->get_where('tb_condition', $where );
        // $this->db->last_query();
        $score =0 ;
        if ($query->num_rows() > 0) {
            $r = $query->row_array();
            $score =  $r['score'] ; 
        }

        return $score ;

   }


    //ประสิทธิภาพการขาย รายเดือน
    public function get_Month_Performance($id , $group_id , $series_arr){
         //ดึงยอดขายปีการแข่งขัน
        $series = array() ;
        foreach($series_arr as $item){
            $series = array_merge($series,$item);
        }  

        // var_dump($series);
        $this->db2->select('count(id) as total');

         $where = array(
          'dl_month <=' => date('Y-m-d',strtotime($this->year.'-12-31')) ,
          'dl_month >=' => date('Y-m-d',strtotime($this->year.'-1-1')) ,
        //   'fleet_id IS NULL' => null,
          'salesman_code' => $id,
        //   'sorce' => 'DL',
        );
        $this->db2->where_in("series", $series);

        $this->db2->group_by('MONTH(dl_month)'); 

        $query = $this->db2->get_where('tb_sales_summary', $where );
        // echo $this->db->last_query();
        $score = 0;
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            // var_dump($result); 
            foreach ($result as $rs) {
                $total = $rs['total'];
                $score += $this->SalesPerformance($total , $group_id);
            }         
        }


        return $score ;

    }

    public function SalesPerformance($total,$group_id){

         $where = array(
          'units <=' => $total ,
          'group_id' => $group_id ,
          'set_id' => $this->set ,
          'active' => '1' ,
        );

        $this->db->order_by('units', 'desc'); 
        $this->db->limit(1);

        $this->db->select('score');
        $query = $this->db->get_where('tb_month_score', $where );
        // echo $this->db->last_query();
        // $this->db->last_query(); ÷÷ 
        $score =0 ;
        if ($query->num_rows() > 0) {
            $r = $query->row_array();
            $score =  $r['score'] ; 
        }
        return $score ;
    }

	
}
