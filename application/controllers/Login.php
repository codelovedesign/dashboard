<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MY_Controller {

	public $no_login = false ;
	public function __construct() 
    {       
        parent:: __construct();
		
		$this->load->model('user_model');
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');

		$this->middle = 'login';
		$this->title = 'Sign In';
		$this->data['title']="Sing In";
		$this->data['sub_title']="ยินดีต้อนรับเข้าสู่ระบบ";
    }



	public function index()
	{

		$this->form_validation->set_rules('username', 'Username', 'trim|required');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|callback_checkUsernamePassword');
		
		// var_dump($this->form_validation->run());

		if ($this->form_validation->run() == false )
		{
			$this->view("blank");
		}
		else
		{
			redirect('dashboard', 'refresh');
		}


	}

	public function checkUsernamePassword($password)
    {
		$username = $this->input->post('username');

		$result = $this->user_model->login($username , $password );
		if($result)
		{
			$sess_array = array();
			foreach($result as $row)
			{
				$sess_array = array(
					'id' => $row->id,
					'username' => $row->username ,
					'name' => 'admin admin' ,
					'avatar' => base_url() . 'assets/img/user.jpg'
				);
				$this->session->set_userdata('logged_in', $sess_array);


				// var_dump($this->session);
				// exit();
			}
			return true;
		}
		else
		{
			$this->form_validation->set_message('checkUsernamePassword', 'Invalid username or password');
        	return false;
		}
    }


	public function logout()
	{
		$user_data = $this->session->all_userdata();
		foreach ($user_data as $key => $value) {
			if ($key != 'session_id' && $key != 'ip_address' && $key != 'user_agent' && $key != 'last_activity') {
				$this->session->unset_userdata($key);
			}
		}
		$this->session->sess_destroy();
		redirect('dashboard', 'refresh');
	}



	

	
	
}
