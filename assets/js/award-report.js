

var handLevelSelect = function(){

   var $target =  $("select[name='award_type']");

   console.log($target);
    $("select[name='employee_level']").change(function (e) {
        $.post("/report/get_option_award_type/" + $(this).val() ,function(data){
            $target.empty().append(data);
        });
    });

};


/* Application Controller
------------------------------------------------ */
var report = function () {
	"use strict";
	
	return {
		//main function
		init: function () {
		    handLevelSelect();
		}
  };
}();