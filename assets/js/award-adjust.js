var baseurl = $("body").data("baseurl") ;
var handAdjustSave = function(){

   $('body').on('keydown', 'input.numberic', function(e){-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()});

   var $form =  $("form#year");
   var $btnSubmit =  $(".btn-submit");
   var $model = $('#modal-message') ; 
   var $body = $model.find(".modal-body"); 


   $form.submit(function(){

        $model.find("#page-loader").removeClass("hide");
        $btnSubmit.prop("disabled",true) ;
        $model.modal('show') ;
        $body.empty();
        data = $(this).serialize();

        //    console.log(baseurl);

            // $.post(baseurl +"Adjust/createDatabase/" , data ,function(data){
            //     $btnSubmit.prop("disabled",false) ;
            //     $form.find("input[type=text], textarea").val("");
            //     $model.find("#page-loader").addClass("hide");
            //     if(data.status == true)
            //     {
            //         $body.append('<h4 class="text-success" ><i class="material-icons">&#xE86C;</i> ' + data.message  +'</h4>');
            //         location.reload();
            //     }
            //     else
            //     {
            //         $body.append('<h4 class="text-danger"><i class="material-icons">&#xE86C;</i> ' + data.message  +'</h4>');
            //     }
            // },"json");


          var request= $.ajax({
            method: "POST",
            url: baseurl +"Adjust/createDatabase/",
            data: data,
            dataType: "json",
            timeout : 10000000
            });

            request.done(function( data ) {
                $btnSubmit.prop("disabled",false) ;
                $form.find("input[type=text], textarea").val("");
                $model.find("#page-loader").addClass("hide");
                // console.log(data);
                if(data.status == true)
                {
                    $body.append('<h4 class="text-success" ><i class="material-icons">&#xE86C;</i> ' + data.message  +'</h4>');
                    location.reload();
                }
                else
                {
                    $body.append('<h4 class="text-danger"><i class="material-icons">&#xE86C;</i> ' + data.message  +'</h4>');
                }
            });
            
            request.fail(function( jqXHR, textStatus ) {
                alert( "Request failed: " + textStatus );
                $btnSubmit.prop("disabled",false) ;
                $form.find("input[type=text], textarea").val("");
                $model.find("#page-loader").addClass("hide");
            });

        
    
      return false ;
   });
};
var handDeleteDatabase = function(){
    var btn_delete = $('[data-click="delete_database"]') ;
    var $model_del = $('#modal-delete') ;
    var $alert_danger = $model_del.find(".alert-danger"); 
    var $form_delete =  $("#form-delete");
    var btnft_delete = $('[data-click="modal-delete"]') ;
    var id=0 ;
    var data = {};

	if (btn_delete.length !== 0) {
        btn_delete.click(function(){
            $model_del.modal('show') ;
            id = $(this).data("id");
            console.log(id);
        });
    }
    if (btnft_delete.length !== 0) {
        btnft_delete.click(function(){
            $(this).prop("disabled",true) ;
            data = { "id" : id , password : $form_delete.find("input").val() } ;

            
            $.post(baseurl + "Adjust/delDatabase/" , data ,function(data){
                btnft_delete.prop("disabled",false) ;
                $form_delete.find("input[type=password]").val("");
                // $model.find("#page-loader").addClass("hide");
                
                if(data.status == true)
                {
                    $model_del.modal('hide') ;
                    location.reload();
                }
                else
                {
                    $alert_danger.removeClass("hide");
                    $alert_danger.find(".alert-span").text(data.message);
                }
               
            },"json");
        });
    }

    
};

var handUpload = function() {
    $modal_upload = $("#modal-upload");
    $btnupload = $(".btn-upload");
    $select_year = $(".select-year");
    $select_data = $(".select-data");

    $select_level = $(".select-level");
    $filename = $(".filename");
    // type = $("[name='type']").val();
    type = $select_data.val();
    $progress_bar = $('#progress-upload .progress-bar') ;

    // $("#fileupload").fileupload({
    //     dataType: 'json',
    //     add: function (e, data) {

    //         $.each(data.files, function (index, file) {
    //             // alert('Selected file: ' + file.name);
    //             $filename.html(file.name);
    //         });
    //         $btnupload.unbind().click(function(){

    //             $modal_upload.modal({backdrop: 'static', keyboard: false}) ;
    //             // data.context = $('<p/>').text('Uploading...').replaceAll($(this));
    //             $(this).text("Uploading...");
    //             data.submit();
    //         });
               
    //     },
    //     progressall: function (e, data) {
    //         var progress = parseInt(data.loaded / data.total * 100, 10);
    //         $progress_bar.css(
    //             'width',
    //             progress + '%'
    //         ).text(progress + '%' );
    //         console.log(progress);
    //     },
    //     done: function (e, data) {
    //         $result = data.result.result ;
    //         if(data.result.error){
                

    //             $.gritter.add({
    //                 title: 'แจ้งเตือน',
    //                 text: data.result.error ,
    //                 sticky: true ,
    //             });
    //         }
    //         else
    //         {
    //             update_data(data.result.file_name);
    //             $('[data-click="importUsers"]').data("json", $result);
    //             $(".user-import-table").html(data.result.html);
    //             // data.context.text('Upload finished.');
    //         }

    //     }
    // });

    $btnupload.prop("disabled",true);
    var get_level = function(levelid){
        $.post(baseurl+'Adjust/get_level/' + levelid , function(data){
            var html = '';
            data.forEach(function(element) {
                if($select_data.data("selected")==element.id ){
                    html += "<option selected='selected'  value='" + element.id +"'>" + element.name +"</option>";
                } else
                {
                    html += "<option  value='" + element.id +"'>" + element.name +"</option>";
                }

            }, this);
            $select_data.empty().append(html);


        },'json');
    };

    
    

    //onload
    var levelid = $select_level.val();
    get_level(levelid);
    // onchange
    $select_level.change(function(){
        var levelid = $(this).val();
        $.post(baseurl+'Adjust/get_level/' + levelid , function(data){
            
            // console.log(data[0].id);
            if(data.length){
                location.href =  baseurl+'Adjust/import/' + data[0].id +'/' + $select_year.val();
            }

        },'json');
    });

    

    $("#fileupload").change(function(data){
        // console.log($(this)[0]);
        if($(this)[0].files[0]){
            var filename = $(this)[0].files[0].name;
            $filename.text(filename);
            $btnupload.prop("disabled",false);
        }else{
            $filename.text("");
            $btnupload.prop("disabled",true);
        }
    });



     $select_year.change(function(){
        location.href =  baseurl+'Adjust/import/' + $select_data.val() +'/' + $(this).val();
    });

     $select_data.change(function(){
        type = $(this).val(); 
        location.href =  baseurl+'Adjust/import/' + type +'/' + $select_year.val();
    });


       
}

var  update_data = function(filename) {
    $modal_upload = $("#modal-upload");
    $select_year = $(".select-year");
    type = $("[name='type']").val();
    var data = 'filename='+filename + '&year=' + $select_year.val() + '&type=' + type ;
   

//    console.log(baseurl);
    $.post(baseurl+"Adjust/update_data", data , function(data){
        // console.log(data);
        $.gritter.add({
            title: 'แจ้งเตือน',
            text: data.message
        });

        $modal_upload.modal('hide') ;
        $(".btn-upload").html('<i class="fa fa-floppy-o" aria-hidden="true"></i> อัพโหลด');

        if(data.status == true )
        {
            location.reload();            
        }       
    },"json");


};


var handDataTable  = function(){
    var $table = $('#dataTable') ;

    $table.DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'csv', 'excel', 'pdf', 'print'
        ],
        "processing": true,
        "serverSide": true,
        "pageLength": 50 ,
        "order": [],
        "ajax": {
             "url" : $table.data('url') ,
            "type" : "POST",
        },
        "columnDefs": [
            { 
                "targets": [ 0 ], //first column / numbering column
                "orderable": false, //set not orderable
            },
        ],
    } );
};


/* Application Controller
------------------------------------------------ */
var Adjust = function () {
	"use strict";
	
	return {
		//main function
		init: function () {
		    handAdjustSave();
            handDeleteDatabase();
            handUpload();
            handDataTable();
		}
  };
}();