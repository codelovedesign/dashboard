var handleDashboardGritterNotification = function() {
    // $(window).load(function() {
    //     setTimeout(function() {
    //         $.gritter.add({
    //             title: 'Welcome back, Admin!',
    //             text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempus lacus ut lectus rutrum placerat.',
    //             image: 'assets/img/user.jpg',
    //             sticky: true,
    //             time: '',
    //             class_name: 'my-sticky-class'
    //         });
    //     }, 1000);
    // });
};

var handleEmloyeeSwitchType = function() {
    var option = $(".change-employee");
    var sale = $('.none-type-sale');

    // console.log(option);

    var toggle = function(id){
        if(id==2)
            sale.hide();
        else
            sale.show();
    }

    toggle(option.val());

    option.change(function(){
        // console.log($(this).val());
        toggle($(this).val());
    });

    
};


var employee = function () {
	"use strict";
    return {
        //main function
        init: function () {
            handleEmloyeeSwitchType();
            handleDashboardGritterNotification();
        }
    };
}();