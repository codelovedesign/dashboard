var baseurl = $("body").data("baseurl") ;
var handTrainingSave = function(){

   $('body').on('keydown', 'input.numberic', function(e){-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()});

   var $form =  $("form#year");
   var $btnSubmit =  $(".btn-submit");
   var $model = $('#modal-message') ; 
   var $body = $model.find(".modal-body"); 


   $form.submit(function(){

        $model.find("#page-loader").removeClass("hide");
        $btnSubmit.prop("disabled",true) ;
        $model.modal('show') ;
        $body.empty();
        data = $(this).serialize();



          var request= $.ajax({
            method: "POST",
            url: baseurl +"training/createDatabase/",
            data: data,
            dataType: "json",
            timeout : 10000000
            });

            request.done(function( msg ) {
                $btnSubmit.prop("disabled",false) ;
                $form.find("input[type=text], textarea").val("");
                $model.find("#page-loader").addClass("hide");
                if(data.status == true)
                {
                    $body.append('<h4 class="text-success" ><i class="material-icons">&#xE86C;</i> ' + data.message  +'</h4>');
                    location.reload();
                }
                else
                {
                    $body.append('<h4 class="text-danger"><i class="material-icons">&#xE86C;</i> ' + data.message  +'</h4>');
                }
            });
            
            request.fail(function( jqXHR, textStatus ) {
                alert( "Request failed: " + textStatus );
                $btnSubmit.prop("disabled",false) ;
                $form.find("input[type=text], textarea").val("");
                $model.find("#page-loader").addClass("hide");
            });

        
    
      return false ;
   });
};
var handDeleteDatabase = function(){
    var btn_delete = $('[data-click="delete_database"]') ;
    var $model_del = $('#modal-delete') ;
    var $alert_danger = $model_del.find(".alert-danger"); 
    var $form_delete =  $("#form-delete");
    var btnft_delete = $('[data-click="modal-delete"]') ;
    var id=0 ;
    var data = {};

	if (btn_delete.length !== 0) {
        btn_delete.click(function(){
            $model_del.modal('show') ;
            id = $(this).data("id");
            console.log(id);
        });
    }
    if (btnft_delete.length !== 0) {
        btnft_delete.click(function(){
            $(this).prop("disabled",true) ;
            data = { "id" : id , password : $form_delete.find("input").val() } ;

            
            $.post(baseurl + "training/delDatabase/" , data ,function(data){
                btnft_delete.prop("disabled",false) ;
                $form_delete.find("input[type=password]").val("");
                // $model.find("#page-loader").addClass("hide");
                
                if(data.status == true)
                {
                    $model_del.modal('hide') ;
                    location.reload();
                }
                else
                {
                    $alert_danger.removeClass("hide");
                    $alert_danger.find(".alert-span").text(data.message);
                }
               
            },"json");
        });
    }

    
};

var handUpload = function() {
    $modal_upload = $("#modal-upload");
    $btnupload = $(".btn-upload");
    $select_year = $(".select-year");
    $select_data = $(".select-data");
    $filename = $(".filename");
    // type = $("[name='type']").val();
    type = $select_data.val();
    $progress_bar = $('#progress-upload .progress-bar') ;

    $("#fileupload").fileupload({
        dataType: 'json',
        add: function (e, data) {

            $.each(data.files, function (index, file) {
                // alert('Selected file: ' + file.name);
                $filename.html(file.name);
            });
            $btnupload.unbind().click(function(){

                $modal_upload.modal({backdrop: 'static', keyboard: false}) ;
                // data.context = $('<p/>').text('Uploading...').replaceAll($(this));
                $(this).text("Uploading...");
                data.submit();
            });
               
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $progress_bar.css(
                'width',
                progress + '%'
            ).text(progress + '%' );
            console.log(progress);
        },
        done: function (e, data) {
            $result = data.result.result ;
            if(data.result.error){
                

                $.gritter.add({
                    title: 'แจ้งเตือน',
                    text: data.result.error ,
                    sticky: true ,
                });
            }
            else
            {
                update_data(data.result.file_name);
                $('[data-click="importUsers"]').data("json", $result);
                $(".user-import-table").html(data.result.html);
                // data.context.text('Upload finished.');
            }

        }
    });



     $select_year.change(function(){
        location.href =  baseurl+'training/import/' + type +'/' + $(this).val();
    });

     $select_data.change(function(){
        type = $(this).val(); 
        location.href =  baseurl+'training/import/' + type +'/' + $select_year.val();
    });


       
}

var  update_data = function(filename) {
    $modal_upload = $("#modal-upload");
    $select_year = $(".select-year");
    type = $("[name='type']").val();
    var data = 'filename='+filename + '&year=' + $select_year.val() + '&type=' + type ;
   

//    console.log(baseurl);
    $.post(baseurl+"training/update_data", data , function(data){
        // console.log(data);
        $.gritter.add({
            title: 'แจ้งเตือน',
            text: data.message
        });

        $modal_upload.modal('hide') ;
        $(".btn-upload").html('<i class="fa fa-floppy-o" aria-hidden="true"></i> อัพโหลด');

        if(data.status == true )
        {
            location.reload();            
        }       
    },"json");


};


var handDataTable  = function(){
    var $table = $('#dataTable') ;

    $table.DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'csv', 'excel', 'pdf', 'print'
        ],
        "processing": true,
        "serverSide": true,
        "pageLength": 50 ,
        "order": [],
        "ajax": {
             "url" : $table.data('url') ,
            "type" : "POST",
        },
        "columnDefs": [
            { 
                "targets": [ 0 ], //first column / numbering column
                "orderable": false, //set not orderable
            },
        ],
    } );
};


/* Application Controller
------------------------------------------------ */
var training = function () {
	"use strict";
	
	return {
		//main function
		init: function () {
		    handTrainingSave();
            handDeleteDatabase();
            handUpload();
            handDataTable();
		}
  };
}();