

var handLevelSelect = function(){

   var $target =  $("select[name='award_type']");
   var $target2 =  $("select[name='set_award']");

    $("select[name='employee_level']").change(function (e) {
        $.post("/processor/get_option_award_type/" + $(this).val() ,function(data){
            $target.empty().append(data);
        });

        $.post("/processor/get_option_set/" + $(this).val() ,function(data){
            $target2.empty().append(data);
        });
    });

};


/* Application Controller
------------------------------------------------ */
var report = function () {
	"use strict";
	
	return {
		//main function
		init: function () {
		    handLevelSelect();
		}
  };
}();